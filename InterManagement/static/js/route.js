//navigation bar setting
var airport_flag;
var route_flag=sessionStorage.getItem("para_flag");

function addRoute() {
	update_flag = 0;
	update_display(update_flag);
}

function modifyRoute() {
	update_flag = 1;
	update_display(update_flag);
}

var height_flag=false;
var duration_flag=false;
var distance_flag=false;
var airport_set=[];
var modify_index;
var source_airport=null, dest_airport=null, height=null, duration=null, distance=null, description=null;
var route_set=[];
var has_init=false;

function update_display(route_flag) {
	if (route_flag==0) {
		var box=document.getElementById("content_box");
		while (box.hasChildNodes()) {   
			box.removeChild(box.firstChild);
		}
		$("#content_box").append("<h3> Add Route </h3>\
									<form class=\"form-horizontal\">\
										<div class =\"form-group\">\
											<div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
												<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
											</div>\
										</div>\
										<br />\
										<div class =\"form-group\">\
											<label for=\"s_air\" class=\"col-md-2 control-label\">Source Airport</label>\
											<div class=\"col-md-3\">\
												<div class=\"btn-group\" id=s_air>\
													<a class=\"btn btn-default dropdown-toggle\"  id=\"dropdown_title_source\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Select an airport\
													<span class=\"caret\"></span>\
													</a>\
													<ul class=\"dropdown-menu\" id=\"source_air\"></ul>\
												</div>\
											</div>\
											<label for=\"d_air\" class=\"col-md-2 control-label\">Destination Airport</label>\
											<div class=\"col-md-3\">\
												<div class=\"btn-group\" id=d_air>\
													<a class=\"btn btn-default dropdown-toggle\" id=\"dropdown_title_dest\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Select an airport\
													<span class=\"caret\"></span>\
													</a>\
													<ul class=\"dropdown-menu\" id=\"dest_air\"></ul>\
												</div>\
											</div>\
										</div>\
	    								<br />\
										<div class =\"form-group\" id=\"inofbox1\">\
											<label for=\"height\" class=\"col-md-2 control-label\">Schedule Height(feet)</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"height\" class=\"form-control\" placeholder=\"00000\" oninput=\"validateHeight()\">\
												<span id=\"heightcheck\"></span>\
											</div>\
											<label for=\"duraion\" class=\"col-md-2 control-label\">Duraion(min)</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"duration\" class=\"form-control\" placeholder=\"0000\" oninput=\"validateDuration()\">\
												<span id=\"durationcheck\"></span>\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\" id=\"inofbox2\">\
											<label for=\"distance\" class=\"col-md-2 control-label\">Distance(mile)</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"distance\" class=\"form-control\" placeholder=\"00000\" oninput=\"validateDistance()\">\
												<span id=\"distancecheck\"></span>\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\" id=\"inofbox3\">\
											<label for=\"description\" class=\"col-md-2 control-label\">Description</label>\
											<div class=\"col-md-8\">\
												<input type=\"text\" id=\"description\" class=\"form-control\" placeholder=\"Please input description of the route\">\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\">\
											<div style=\"text-align:center\">\
												<button type=\"button\" class=\"btn btn-primary\" id=\"setroute\" onclick=\"add_route()\">Submit</button>\
										</div>\
									</form>");
		airport_set=[];
		//load airport
		$.get("/load_airport",{}, function( data, status ){
			//console.log(data.coords[0]);
			//store airport
			for (i=0; i<data.coords.length; i++){
				airport_set.push ({iata_code: data.coords[i][0], city: data.coords[i][1]});
			}
			//console.log(airport_set);
			//add every airport to a dropdown
			for (var i = 0; i<airport_set.length; ++i){   
				newline = "<li><a tabindex=\"-1\" onclick=\"set_iata("+i.toString()+", true)\" href=\"#\"> "+airport_set[i].city+" ("+airport_set[i].iata_code+")</a></li>"
				//console.log(newline);
				$("#source_air").append(newline);
				newline = "<li><a tabindex=\"-1\" onclick=\"set_iata("+i.toString()+", false)\" href=\"#\"> "+airport_set[i].city+" ("+airport_set[i].iata_code+")</a></li>"
				//console.log(newline);
				$("#dest_air").append(newline);
			}
		});
	}
	else if (route_flag==1) {
		var box=document.getElementById("content_box");
		while (box.hasChildNodes()) {   
			box.removeChild(box.firstChild);
		}
		$("#content_box").append("<h3> Modify Route </h3>\
									<form class=\"form-horizontal\">\
										<div class =\"form-group\">\
											<div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
												<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
											</div>\
										</div>\
										<br />\
										<div class =\"form-group\">\
											<label for=\"s_air\" class=\"col-md-2 control-label\">Source Airport</label>\
											<div class=\"col-md-3\">\
												<div class=\"btn-group\" id=s_air>\
													<a class=\"btn btn-default dropdown-toggle\"  id=\"dropdown_title_source\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Select an airport\
													<span class=\"caret\"></span>\
													</a>\
													<ul class=\"dropdown-menu\" id=\"source_air\"></ul>\
												</div>\
											</div>\
											<label for=\"d_air\" class=\"col-md-2 control-label\">Destination Airport</label>\
											<div class=\"col-md-3\">\
												<div class=\"btn-group\" id=d_air>\
													<a class=\"btn btn-default dropdown-toggle\" id=\"dropdown_title_dest\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Select an airport\
													<span class=\"caret\"></span>\
													</a>\
													<ul class=\"dropdown-menu\" id=\"dest_air\"></ul>\
												</div>\
											</div>\
										</div>\
	    								<br />\
										<div class =\"form-group\" id=\"inofbox1\">\
											<label for=\"height\" class=\"col-md-2 control-label\">Schedule Height(feet)</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"height\" class=\"form-control\" placeholder=\"00000\" oninput=\"validateHeight()\">\
												<span id=\"heightcheck\"></span>\
											</div>\
											<label for=\"duraion\" class=\"col-md-2 control-label\">Duraion(min)</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"duration\" class=\"form-control\" placeholder=\"0000\" oninput=\"validateDuration()\">\
												<span id=\"durationcheck\"></span>\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\" id=\"inofbox2\">\
											<label for=\"distance\" class=\"col-md-2 control-label\">Distance(mile)</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"distance\" class=\"form-control\" placeholder=\"00000\" oninput=\"validateDistance()\">\
												<span id=\"distancecheck\"></span>\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\" id=\"inofbox3\">\
											<label for=\"description\" class=\"col-md-2 control-label\">Description</label>\
											<div class=\"col-md-8\">\
												<input type=\"text\" id=\"description\" class=\"form-control\" placeholder=\"Please input description of the route\">\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\">\
											<div style=\"text-align:center\">\
												<button type=\"button\" class=\"btn btn-primary\" id=\"setroute\" onclick=\"mod_route()\">Submit</button>\
										</div>\
									</form>\
									<div style=\"height: 300px; overflow-y:auto;\">\
									<label for=\"own_table\">Route</label>\
										<table class=\"table table-bordered table-condensed table-striped table-hover\" id=\"route_table\">\
											<thead>\
												<tr><th>Departure Airport</th><th>Arrival Airport</th><th>Scheduled Height(feet)</th><th>Duration(min)</th><th>Distance(mile)</th><th>Description</th></tr>\
											</thead>\
											<tbody id=\"route_list\">\
											</tbody>\
										</table>\
								</div>");
		refreshtable();
	}
}

if (!has_init) {
	update_display(route_flag);
	has_init = true;
}

function refreshtable() {
	$("#route_list tr").remove();
	route_set=[];
	$.get("/load_route",{}, function( data, status ){
		//console.log(data.coords[0]);
		//store airport
		for (i=0; i<data.coords.length; i++){
			route_set.push ({route_id: data.coords[i][0], dept_airport: data.coords[i][1], arr_airport: data.coords[i][2], scheduled_height: data.coords[i][3], duraion: data.coords[i][4], distance: data.coords[i][5], description: data.coords[i][6]});
		}
		//console.log(route_set);
		//add every airport to a dropdown
		for (var i = 0; i<route_set.length; ++i){   
			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='selectroute("+i.toString()+")'><td>"+route_set[i].dept_airport+"</td><td>"+route_set[i].arr_airport+"</td><td>"+route_set[i].scheduled_height+"</td><td>"+route_set[i].duraion+"</td><td>"+route_set[i].distance+"</td><td>"+route_set[i].description+"</tr>";
	//console.log(newline);
			$("#route_list").append(newline);
		}
	});
}

function selectroute(index) {
	//console.log(route_set[index]);
	document.getElementById("height").value=route_set[index].scheduled_height;
	document.getElementById("duration").value=route_set[index].duraion;
	document.getElementById("distance").value=route_set[index].distance;
	document.getElementById("description").value=route_set[index].description;
	$('#dropdown_title_source').html(route_set[index].dept_airport+"<span class='caret'></span>");
	$('#dropdown_title_dest').html(route_set[index].arr_airport+"<span class='caret'></span>");
	modify_index=index;
	height_flag=true;
	duration_flag=true;
	distance_flag=true;
	source_airport=route_set[index].dept_airport;
	dest_airport=route_set[index].arr_airport;
}

function validateHeight() {
	var heightcheck=document.getElementById("height").value;
	var isnum = /^\d+$/.test(heightcheck);
	//console.log(isnum);
	if (!isnum) {
		document.getElementById("heightcheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("inofbox1").className="form-group has-feedback has-error";
		height_flag=false;
	}
	else {
		document.getElementById("heightcheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("inofbox1").className="form-group has-feedback has-success";
		height_flag=true;
	}
}

function validateDuration() {
	var durationcheck=document.getElementById("duration").value;
	var isnum = /^\d+$/.test(durationcheck);
	//console.log(isnum);
	if (!isnum) {
		document.getElementById("durationcheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("inofbox1").className="form-group has-feedback has-error";
		duration_flag=false;
	}
	else {
		document.getElementById("durationcheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("inofbox1").className="form-group has-feedback has-success";
		duration_flag=true;
	}
}

function validateDistance() {
	var distancecheck=document.getElementById("distance").value;
	var isnum = /^\d+$/.test(distancecheck);
	//console.log(isnum);
	if (!isnum) {
		document.getElementById("distancecheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("inofbox2").className="form-group has-feedback has-error";
		distance_flag=false;
	}
	else {
		document.getElementById("distancecheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("inofbox2").className="form-group has-feedback has-success";
		distance_flag=true;
	}
}

function set_iata(index, flag) {
	if (flag) {
		source_airport=airport_set[index].iata_code;
		line=airport_set[index].city+" ("+airport_set[index].iata_code+")";
		$('#dropdown_title_source').html(line)+"<span class='caret'></span>";
	}
	else {
		dest_airport=airport_set[index].iata_code;
		line=airport_set[index].city+" ("+airport_set[index].iata_code+")";
		$('#dropdown_title_dest').html(line+"<span class='caret'></span>");
	}
	console.log(source_airport);
	console.log(dest_airport);
}

function add_route() {
	height=document.getElementById("height").value;
	duration=document.getElementById("duration").value;
	distance=document.getElementById("distance").value;
	description=document.getElementById("description").value;
	if (height_flag && duration_flag && distance_flag && source_airport!=null && dest_airport!=null) {
		$.get( "/add_route",{source_airport:source_airport, dest_airport:dest_airport, height:height, duration:duration, distance:distance, description:description}, function( data, status ) {
			//console.log(data);
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Airport Added.</span>");
				document.getElementById("error").className="alert alert-success";
				document.getElementById("heightcheck").className="";	
				document.getElementById("inofbox1").className="form-group has-feedback";
				document.getElementById("durationcheck").className="";
				document.getElementById("inofbox2").className="form-group has-feedback";
				document.getElementById("distancecheck").className="";
				$('#dropdown_title_source').html("Select an Airport"+"<span class='caret'></span>");
				$('#dropdown_title_dest').html("Select an Airport"+"<span class='caret'></span>");
				$("#error").show();
				document.getElementById("height").value="";
				document.getElementById("duration").value="";
				document.getElementById("distance").value="";
				document.getElementById("description").value="";
				source_airport=null;
				dest_airport=null;
				height=null;
				duration=null;
				distance=null;
				description=null;
				height_flag=false;
				duration_flag=false;
				distance_flag=false;
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}	
}

function mod_route() {
	height=document.getElementById("height").value;
	duration=document.getElementById("duration").value;
	distance=document.getElementById("distance").value;
	description=document.getElementById("description").value;
	if (height_flag && duration_flag && distance_flag && source_airport!=null && dest_airport!=null) {
		$.get( "/mod_route",{route_id:route_set[modify_index].route_id, source_airport:source_airport, dest_airport:dest_airport, height:height, duration:duration, distance:distance, description:description}, function( data, status ) {
			//console.log(data);
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Airport Modified.</span>");
				document.getElementById("error").className="alert alert-success";
				document.getElementById("heightcheck").className="";	
				document.getElementById("inofbox1").className="form-group has-feedback";
				document.getElementById("durationcheck").className="";
				document.getElementById("inofbox2").className="form-group has-feedback";
				document.getElementById("distancecheck").className="";
				$('#dropdown_title_source').html("Select an Airport"+"<span class='caret'></span>");
				$('#dropdown_title_dest').html("Select an Airport"+"<span class='caret'></span>");
				$("#error").show();
				document.getElementById("height").value="";
				document.getElementById("duration").value="";
				document.getElementById("distance").value="";
				document.getElementById("description").value="";
				source_airport=null;
				dest_airport=null;
				height=null;
				duration=null;
				distance=null;
				description=null;
				height_flag=false;
				duration_flag=false;
				distance_flag=false;
				refreshtable();
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}	
}
