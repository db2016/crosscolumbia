var airport_flag=sessionStorage.getItem("para_flag");
var iatacode=null, type=null, city=null, state=null, name=null, time_zone=null;
var state_flag=false;
var iata_flag=false;
var tz_flag=false;
var airport_set=[];
var modify_index;
var has_init=false;

function addAirport() {
	update_flag = 0;
	update_display(update_flag);
}

function modifyAirport() {
	update_flag = 1;
	update_display(update_flag);
}

function update_display(airport_flag) {
	if (airport_flag==0) {
		var box=document.getElementById("content_box");
		while (box.hasChildNodes()) {   
			box.removeChild(box.firstChild);
		}
		$("#content_box").append("<h3> Add Airport </h3>\
									<form class=\"form-horizontal\">\
										<div class =\"form-group\">\
											<div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
												<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
											</div>\
										</div>\
										<br />\
										<div class =\"form-group\" id=\"iatacheckbox\">\
											<label for=\"iata\" class=\"col-md-2 control-label\">IATA CODE</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"iata\" class=\"form-control\" placeholder=\"XXX\" oninput=\"validateIATA()\">\
												<span id=\"iatacheck\"></span>\
											</div>\
											<label for=\"type\" class=\"col-md-2 control-label\">Type</label>\
											<div class=\"col-md-3\">\
												<div class=\"btn-group\" id=\"type\">\
													<a class=\"btn btn-default dropdown-toggle\"  id=\"dropdown_type\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Select a type\
													<span class=\"caret\"></span>\
													</a>\
													<ul class=\"dropdown-menu\" id=\"type_val\">\
														<li><a tabindex=\"-1\" onclick=\"set_type(0)\" href=\"#\">HUB</a></li>\
														<li><a tabindex=\"-1\" onclick=\"set_type(1)\" href=\"#\">REG</a></li>\
													</ul>\
												</div>\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\" id=\"locationbox\">\
											<label for=\"city\" class=\"col-md-2 control-label\">City</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"city\" class=\"form-control\" placeholder=\"City\">\
											</div>\
											<label for=\"state\" class=\"col-md-2 control-label\">State</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"state\" class=\"form-control\" placeholder=\"State\" oninput=\"validateState()\">\
												<span id=\"statecheck\"></span>\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\" id=\"tzbox\">\
											<label for=\"name\" class=\"col-md-2 control-label\">Name</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"name\" class=\"form-control\" placeholder=\"Name\">\
											</div>\
											<label for=\"timezone\" class=\"col-md-2 control-label\">Time Zone</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"timezone\" class=\"form-control\" placeholder=\"timezone\" oninput=\"validateTZ()\">\
												<span id=\"tzcheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\">\
											<div style=\"text-align:center\">\
												<button type=\"button\" class=\"btn btn-primary\" id=\"setairport\" onclick=\"set_airport()\">Submit</button>\
										</div>\
									</div>\
								</form>");
	} else if (airport_flag == 1) {
		var box=document.getElementById("content_box");
		while (box.hasChildNodes()) {   
			box.removeChild(box.firstChild);
		}
		$("#content_box").append("<h3> Modify Airport </h3>\
									<form class=\"form-horizontal\">\
										<div class =\"form-group\">\
											<div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
												<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
											</div>\
										</div>\
										<br />\
										<div class =\"form-group\" id=\"iatacheckbox\">\
											<label for=\"iata\" class=\"col-md-2 control-label\">IATA CODE</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"iata\" class=\"form-control\" placeholder=\"XXX\" oninput=\"validateIATA()\">\
												<span id=\"iatacheck\"></span>\
											</div>\
											<label for=\"type\" class=\"col-md-2 control-label\">Type</label>\
											<div class=\"col-md-3\">\
												<div class=\"btn-group\" id=\"type\">\
													<a class=\"btn btn-default dropdown-toggle\"  id=\"dropdown_type\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Select a type\
													<span class=\"caret\"></span>\
													</a>\
													<ul class=\"dropdown-menu\" id=\"type_val\">\
														<li><a tabindex=\"-1\" onclick=\"set_type(0)\" href=\"#\">HUB</a></li>\
														<li><a tabindex=\"-1\" onclick=\"set_type(1)\" href=\"#\">REG</a></li>\
													</ul>\
												</div>\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\" id=\"locationbox\">\
											<label for=\"city\" class=\"col-md-2 control-label\">City</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"city\" class=\"form-control\" placeholder=\"City\">\
											</div>\
											<label for=\"state\" class=\"col-md-2 control-label\">State</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"state\" class=\"form-control\" placeholder=\"State\" oninput=\"validateState()\">\
												<span id=\"statecheck\"></span>\
											</div>\
										</div>\
		    							<br />\
										<div class =\"form-group\" id=\"tzbox\">\
											<label for=\"name\" class=\"col-md-2 control-label\">Name</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"name\" class=\"form-control\" placeholder=\"Name\">\
											</div>\
											<label for=\"timezone\" class=\"col-md-2 control-label\">Time Zone</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"timezone\" class=\"form-control\" placeholder=\"timezone\" oninput=\"validateTZ()\">\
												<span id=\"tzcheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\">\
											<div style=\"text-align:center\">\
												<button type=\"button\" class=\"btn btn-primary\" id=\"setairport\" onclick=\"mod_airport()\">Submit</button>\
										</div>\
									</div>\
								</form>\
								<div style=\"height: 300px; overflow-y:auto;\">\
									<label for=\"own_table\">Own Flight</label>\
										<table class=\"table table-bordered table-condensed table-striped table-hover\" id=\"airport_table\">\
											<thead>\
												<tr><th>IATA_CODE</th><th>City</th><th>State</th><th>Name</th><th>Time Zone</th><th>Type</th></tr>\
											</thead>\
											<tbody id=\"airport_list\">\
											</tbody>\
										</table>\
								</div>");
		refreshtable();
	}
}

if (!has_init) {
	update_display(airport_flag);
	has_init = true;
}

function refreshtable() {
	$("#airport_list tr").remove();
	airport_set=[];
	$.get("/load_airport",{}, function( data, status ){
		//console.log(data.coords[0]);
		//store airport
		for (i=0; i<data.coords.length; i++){
			airport_set.push ({iata_code: data.coords[i][0], city: data.coords[i][1], state: data.coords[i][2], timezone: data.coords[i][3], airname: data.coords[i][4], airtype: data.coords[i][5]});
		}
		//console.log(airport_set);
		//add every airport to a dropdown
		for (var i = 0; i<airport_set.length; ++i){   
			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='selectairport("+i.toString()+")'><td>"+airport_set[i].iata_code+"</td><td>"+airport_set[i].city+"</td><td>"+airport_set[i].state+"</td><td>"+airport_set[i].airname+"</td><td>"+airport_set[i].timezone+"</td><td>"+airport_set[i].airtype+"</tr>";
	//console.log(newline);
			$("#airport_list").append(newline);
		}
	});
}

function validateState() {
	var statecheck=document.getElementById("state").value;
	var ischar=/^[A-Z]+$/.test(statecheck);
	var statelen=statecheck.length;
	//console.log(isnum);
	if (statelen!=2 || !ischar) {
		document.getElementById("statecheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("locationbox").className="form-group has-feedback has-error";
		state_flag=false;
	}
	else {
		document.getElementById("statecheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("locationbox").className="form-group has-feedback has-success";
		state_flag=true;
	}
}

function validateIATA() {
	var iatacheck=document.getElementById("iata").value;
	var ischar=/^[A-Z]+$/.test(iatacheck);
	var iatalen=iatacheck.length;
	//console.log(isnum);
	if (iatalen!=3 || !ischar) {
		document.getElementById("iatacheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("iatacheckbox").className="form-group has-feedback has-error";
		iata_flag=false;
	}
	else {
		document.getElementById("iatacheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("iatacheckbox").className="form-group has-feedback has-success";
		iata_flag=true;
	}
}

function validateTZ() {
	var tzcheck=document.getElementById("timezone").value;
	var ischar=/^[A-Z]+$/.test(tzcheck);
	var tzlen=tzcheck.length;
	//console.log(isnum);
	if ((tzlen<3 || tzlen>4) || !ischar) {
		document.getElementById("tzcheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("tzbox").className="form-group has-feedback has-error";
		tz_flag=false;
	}
	else {
		document.getElementById("tzcheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("tzbox").className="form-group has-feedback has-success";
		tz_flag=true;
	}
}

function set_type(t) {
	//console.log(type);
	if (t==0) {
		type="HUB";
		$('#dropdown_type').html(type+"<span class='caret'></span>");
	}
	else {
		type="REG";
		$('#dropdown_type').html(type+"<span class='caret'></span>");
	}
	//console.log(type);
}

function selectairport(index) {
	document.getElementById("iata").value=airport_set[index].iata_code;
	document.getElementById("city").value=airport_set[index].city;
	document.getElementById("state").value=airport_set[index].state;
	document.getElementById("name").value=airport_set[index].airname;
	document.getElementById("timezone").value=airport_set[index].timezone;
	$('#dropdown_type').html(airport_set[index].airtype+"<span class='caret'></span>");
	type=airport_set[index].airtype;
	modify_index=index;
	state_flag=true;
	iata_flag=true;
	tz_flag=true;
}

function set_airport() {
	iatacode=document.getElementById("iata").value;
	city=document.getElementById("city").value;
	state=document.getElementById("state").value;
	name=document.getElementById("name").value;
	time_zone=document.getElementById("timezone").value;
	if (state_flag && iata_flag && tz_flag && city!=null && name!=null && type!=null) {
		$.get( "/add_airport",{iatacode:iatacode, city:city, state:state, name:name, time_zone:time_zone, type:type}, function( data, status ) {
			//console.log(data);
			if (data.coords[0][0]=="Existed") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Airport Existed.</span>");
				document.getElementById("error").className="alert alert-warning";
				document.getElementById("statecheck").className="";	
				document.getElementById("locationbox").className="form-group has-feedback";
				document.getElementById("iatacheck").className="";
				document.getElementById("iatacheckbox").className="form-group has-feedback";
				document.getElementById("tzcheck").className="";
				document.getElementById("tzbox").className="form-group has-feedback";
				$('#dropdown_type').html("Select a type"+"<span class='caret'></span>");
				$("#error").show();
				document.getElementById("iata").value="";
				document.getElementById("city").value="";
				document.getElementById("state").value="";
				document.getElementById("name").value="";
				document.getElementById("timezone").value="";
				iatacode=null;
				type=null;
				city=null;
				state=null;
				name=null;
				time_zone=null;
				state_flag=false;
				iata_flag=false;
				tz_flag=false;
			}
			else {	
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Airport Added.</span>");
				document.getElementById("error").className="alert alert-success";
				document.getElementById("statecheck").className="";	
				document.getElementById("locationbox").className="form-group has-feedback";
				document.getElementById("iatacheck").className="";
				document.getElementById("iatacheckbox").className="form-group has-feedback";
				document.getElementById("tzcheck").className="";
				document.getElementById("tzbox").className="form-group has-feedback";
				$('#dropdown_type').html("Select a type"+"<span class='caret'></span>");
				$("#error").show();
				document.getElementById("iata").value="";
				document.getElementById("city").value="";
				document.getElementById("state").value="";
				document.getElementById("name").value="";
				document.getElementById("timezone").value="";
				iatacode=null;
				type=null;
				city=null;
				state=null;
				name=null;
				time_zone=null;
				state_flag=false;
				iata_flag=false;
				tz_flag=false;
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}	
}

function mod_airport() {
	iatacode=document.getElementById("iata").value;
	city=document.getElementById("city").value;
	state=document.getElementById("state").value;
	name=document.getElementById("name").value;
	time_zone=document.getElementById("timezone").value;
	/*console.log(state_flag);
	console.log(iata_flag);
console.log(tz_flag);
console.log(city);
console.log(name);
console.log(type);*/
	if (state_flag && iata_flag && tz_flag && city!=null && name!=null && type!=null) {
		$.get( "/modify_airport",{iatacode:iatacode, city:city, state:state, name:name, time_zone:time_zone, type:type, old_iata:airport_set[modify_index].iata_code}, function( data, status ) {
			//console.log(data);
			if (data.coords[0][0]!="Success") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Fail to Modify</span>");
				document.getElementById("error").className="alert alert-warning";
				document.getElementById("statecheck").className="";	
				document.getElementById("locationbox").className="form-group has-feedback";
				document.getElementById("iatacheck").className="";
				document.getElementById("iatacheckbox").className="form-group has-feedback";
				document.getElementById("tzcheck").className="";
				document.getElementById("tzbox").className="form-group has-feedback";
				$('#dropdown_type').html("Select a type"+"<span class='caret'></span>");
				$("#error").show();
				selectairport(modify_index);
				iatacode=null;
				type=null;
				city=null;
				state=null;
				name=null;
				time_zone=null;
				state_flag=false;
				iata_flag=false;
				tz_flag=false;
			}
			else {	
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Airport Added.</span>");
				document.getElementById("error").className="alert alert-success";
				document.getElementById("statecheck").className="";	
				document.getElementById("locationbox").className="form-group has-feedback";
				document.getElementById("iatacheck").className="";
				document.getElementById("iatacheckbox").className="form-group has-feedback";
				document.getElementById("tzcheck").className="";
				document.getElementById("tzbox").className="form-group has-feedback";
				$('#dropdown_type').html("Select a type"+"<span class='caret'></span>");
				$("#error").show();
				document.getElementById("iata").value="";
				document.getElementById("city").value="";
				document.getElementById("state").value="";
				document.getElementById("name").value="";
				document.getElementById("timezone").value="";
				iatacode=null;
				type=null;
				city=null;
				state=null;
				name=null;
				time_zone=null;
				state_flag=false;
				iata_flag=false;
				tz_flag=false;
				refreshtable();
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
		selectairport(modify_index);
	}	
}
