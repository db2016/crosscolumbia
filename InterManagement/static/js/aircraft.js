var aircraft_flag=sessionStorage.getItem("para_flag");
// var iatacode=null, type=null, city=null, state=null, name=null, time_zone=null;

var id_flag=false; // 1
var year_flag=false; //2
var seats_flag=false; //3
var location_flag=false; //4
var maintainence_flag=false; //5
var range_flag=false; //6
var type=null;

var aircraft_set=[];
var modify_index=-1;
var has_init=false;
var autofill = [
  [0, 8, 116, 4343],
  [0, 12, 138, 3812],
  [0, 16, 169, 3718],
  [0, 8, 116, 4343],
  [0, 12, 138, 4062],
  [0, 16, 190, 4062]
];

function addAircraft() {
	update_flag = 0;
	update_display(update_flag);
}

function modifyAircraft() {
	update_flag = 1;
	update_display(update_flag);
}

function update_display(airport_flag) {
	var box=document.getElementById("content_box");
	while (box.hasChildNodes()) {   
		box.removeChild(box.firstChild);
	}

	if (airport_flag==0) {
		$("#content_box").append("<h3> Add Aircraft </h3>\
									<form class=\"form-horizontal\">\
										<div class =\"form-group\">\
											<div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
												<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
											</div>\
										</div>\
										<div class =\"form-group\">\
											<div id=\"idcheckbox\">\
												<label for=\"aircraft_id\" class=\"col-md-2 control-label\">AIRCRAFT ID</label>\
												<div class=\"col-md-3\">\
													<input type=\"text\" id=\"aircraft_id\" class=\"form-control\" placeholder=\"NXXXCC\" oninput=\"validateID()\">\
													<span id=\"idcheck\"></span>\
												</div>\
											</div>\
											<div id=\"typebox\">\
												<label for=\"type\" class=\"col-md-2 control-label\">Type</label>\
												<div class=\"col-md-3\">\
													<div class=\"btn-group\" id=\"type\">\
														<a class=\"btn btn-default dropdown-toggle\"  id=\"dropdown_type\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Select a type\
														<span class=\"caret\"></span>\
														</a>\
														<ul class=\"dropdown-menu\" id=\"type_val\">\
															<li><a tabindex=\"-1\" onclick=\"set_type(0)\" href=\"#\">A319-CEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(1)\" href=\"#\">A320-CEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(2)\" href=\"#\">A321-CEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(3)\" href=\"#\">A319-NEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(4)\" href=\"#\">A320-NEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(5)\" href=\"#\">A321-NEO</a></li>\
														</ul>\
													</div>\
												</div>\
											</div>\
										</div>\
										<div class =\"form-group\" id=\"yearcheckbox\">\
											<label for=\"year\" class=\"col-md-2 control-label\">Year</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"year\" class=\"form-control\" placeholder=\"20XX\" oninput=\"validateYear()\">\
												<span id=\"yearcheck\"></span>\
											</div>\
										</div>\
		    							<div class =\"form-group\" id=\"seatcheckbox\">\
											<label for=\"fc_seats\" class=\"col-md-3 control-label\">First Class Seats</label>\
											<div class=\"col-md-2\">\
												<input type=\"text\" id=\"fc_seats\" class=\"form-control\" placeholder=\"XX\" oninput=\"validateSeats()\">\
												<span id=\"fc_seatscheck\"></span>\
											</div>\
											<label for=\"bc_seats\" class=\"col-md-3 control-label\">Business Class Seats</label>\
											<div class=\"col-md-2\">\
												<input type=\"text\" id=\"bc_seats\" class=\"form-control\" placeholder=\"XX\" oninput=\"validateSeats()\">\
												<span id=\"bc_seatscheck\"></span>\
											</div>\
											<label for=\"ec_seats\" class=\"col-md-3 control-label\">Economy Class Seats</label>\
											<div class=\"col-md-2\">\
												<input type=\"text\" id=\"ec_seats\" class=\"form-control\" placeholder=\"XX\" oninput=\"validateSeats()\">\
												<span id=\"ec_seatscheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\" id=\"cur_locationcheckbox\">\
											<label for=\"cur_location\" class=\"col-md-2 control-label\">Current Location</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"cur_location\" class=\"form-control\" placeholder=\"XXXX\" oninput=\"validateLocation()\">\
												<span id=\"cur_locationcheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\" id=\"maintainence_statuscheckbox\">\
											<label for=\"maintainence_status\" class=\"col-md-2 control-label\">Maintainence Status</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"maintainence_status\" class=\"form-control\" placeholder=\"XXXXX\" oninput=\"validateMaintainence()\">\
												<span id=\"maintainence_statuscheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\" id=\"range_typical_payloadcheckbox\">\
											<label for=\"range_typical_payload\" class=\"col-md-2 control-label\">Typical Range Payload (Mile)</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"range_typical_payload\" class=\"form-control\" placeholder=\"9999\" oninput=\"validatePayload()\">\
												<span id=\"range_typical_payloadcheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\">\
											<div style=\"text-align:center\">\
												<button type=\"button\" class=\"btn btn-primary\" id=\"setaircraft\" onclick=\"set_aircraft()\">Submit</button>\
										</div>\
									</div>\
								</form>");
	} else if (airport_flag == 1) {
		$("#content_box").append("<h3> Modify Aircraft </h3>\
									<form class=\"form-horizontal\">\
										<div class =\"form-group\">\
											<div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
												<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
											</div>\
										</div>\
										<div class =\"form-group\">\
											<div id=\"idcheckbox\">\
												<label for=\"aircraft_id\" class=\"col-md-2 control-label\">AIRCRAFT ID</label>\
												<div class=\"col-md-3\">\
													<input type=\"text\" id=\"aircraft_id\" class=\"form-control\" placeholder=\"NXXXCC\" readonly>\
													<span id=\"idcheck\"></span>\
												</div>\
											</div>\
											<div id=\"typebox\">\
												<label for=\"type\" class=\"col-md-2 control-label\">Type</label>\
												<div class=\"col-md-3\">\
													<div class=\"btn-group\" id=\"type\">\
														<a class=\"btn btn-default dropdown-toggle\"  id=\"dropdown_type\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Select a type\
														<span class=\"caret\"></span>\
														</a>\
														<ul class=\"dropdown-menu\" id=\"type_val\">\
															<li><a tabindex=\"-1\" onclick=\"set_type(0)\" href=\"#\">A319-CEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(1)\" href=\"#\">A320-CEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(2)\" href=\"#\">A321-CEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(3)\" href=\"#\">A319-NEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(4)\" href=\"#\">A320-NEO</a></li>\
															<li><a tabindex=\"-1\" onclick=\"set_type(5)\" href=\"#\">A321-NEO</a></li>\
														</ul>\
													</div>\
												</div>\
											</div>\
										</div>\
										<div class =\"form-group\" id=\"yearcheckbox\">\
											<label for=\"year\" class=\"col-md-2 control-label\">Year</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"year\" class=\"form-control\" placeholder=\"20XX\" oninput=\"validateYear()\">\
												<span id=\"yearcheck\"></span>\
											</div>\
										</div>\
		    							<div class =\"form-group\" id=\"seatcheckbox\">\
											<label for=\"fc_seats\" class=\"col-md-3 control-label\">First Class Seats</label>\
											<div class=\"col-md-2\">\
												<input type=\"text\" id=\"fc_seats\" class=\"form-control\" placeholder=\"XX\" oninput=\"validateSeats()\">\
												<span id=\"fc_seatscheck\"></span>\
											</div>\
											<label for=\"bc_seats\" class=\"col-md-3 control-label\">Business Class Seats</label>\
											<div class=\"col-md-2\">\
												<input type=\"text\" id=\"bc_seats\" class=\"form-control\" placeholder=\"XX\" oninput=\"validateSeats()\">\
												<span id=\"bc_seatscheck\"></span>\
											</div>\
											<label for=\"ec_seats\" class=\"col-md-3 control-label\">Economy Class Seats</label>\
											<div class=\"col-md-2\">\
												<input type=\"text\" id=\"ec_seats\" class=\"form-control\" placeholder=\"XX\" oninput=\"validateSeats()\">\
												<span id=\"ec_seatscheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\" id=\"cur_locationcheckbox\">\
											<label for=\"cur_location\" class=\"col-md-2 control-label\">Current Location</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"cur_location\" class=\"form-control\" placeholder=\"XXXX\" oninput=\"validateLocation()\">\
												<span id=\"cur_locationcheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\" id=\"maintainence_statuscheckbox\">\
											<label for=\"maintainence_status\" class=\"col-md-2 control-label\">Maintainence Status</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"maintainence_status\" class=\"form-control\" placeholder=\"XXXXX\" oninput=\"validateMaintainence()\">\
												<span id=\"maintainence_statuscheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\" id=\"range_typical_payloadcheckbox\">\
											<label for=\"range_typical_payload\" class=\"col-md-2 control-label\">Typical Range Payload (Mile)</label>\
											<div class=\"col-md-3\">\
												<input type=\"text\" id=\"range_typical_payload\" class=\"form-control\" placeholder=\"9999\" oninput=\"validatePayload()\">\
												<span id=\"range_typical_payloadcheck\"></span>\
											</div>\
										</div>\
										<div class =\"form-group\">\
											<div style=\"text-align:center\">\
												<button type=\"button\" class=\"btn btn-primary\" id=\"setaircraft\" onclick=\"mod_aircraft()\">Submit</button>\
										</div>\
									</div>\
								</form>\
								<div style=\"height: 300px; overflow-y:auto;\">\
									<label for=\"own_table\">Owned Aircraft</label>\
										<table class=\"table table-bordered table-condensed table-striped table-hover\" id=\"aircraft_table\">\
											<thead>\
												<tr><th>Aircraft ID</th><th>Type</th><th>In Service Year</th><th>First Class</th><th>Business Class</th><th>Economy Class</th><th>Current Location</th><th>Maintainence Status</th><th>Typical Range</th></tr>\
											</thead>\
											<tbody id=\"aircraft_list\">\
											</tbody>\
										</table>\
								</div>");
		refreshtable();
	}
}

if (!has_init) {
	update_display(aircraft_flag);
	has_init = true;
}

function refreshtable() {
	$("#aircraft_list tr").remove();
	aircraft_set=[];
	$.get("/load_aircraft",{}, function( data, status ){
		//console.log(data.coords[0]);
		//store airport
		for (i=0; i<data.coords.length; i++){
			aircraft_set.push ({aircraft_id: data.coords[i][0], type: data.coords[i][1], buy_year: data.coords[i][2],
				fc_seats: data.coords[i][3],bc_seats: data.coords[i][4], ec_seats: data.coords[i][5],
				cur_location: data.coords[i][6],maintainence_status: data.coords[i][7], range_typical_payload: data.coords[i][8]});
		}
		//console.log(aircraft_set);
		//add every airport to a dropdown
		for (var i = 0; i<aircraft_set.length; ++i){   
			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='selectaircraft("+i.toString()+")'><td>"+
			aircraft_set[i].aircraft_id+"</td><td>"+aircraft_set[i].type+"</td><td>"+aircraft_set[i].buy_year+"</td><td>"+aircraft_set[i].fc_seats+"</td><td>"+aircraft_set[i].bc_seats+"</td><td>"+
			aircraft_set[i].ec_seats+"</td><td>"+aircraft_set[i].cur_location+"</td><td>"+aircraft_set[i].maintainence_status+"</td><td>"+aircraft_set[i].range_typical_payload+"</td></tr>";
	//console.log(newline);
			$("#aircraft_list").append(newline);
		}
	});
}

function setCheckAndCheckbox(isValid, checkName, checkBoxName) {
	if (isValid) {
		document.getElementById(checkName).className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById(checkBoxName).className="form-group has-feedback has-success";
	} else {
		document.getElementById(checkName).className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById(checkBoxName).className="form-group has-feedback has-error";
	}
}

function setCheckbox(isValid, checkBoxName) {
	if (isValid) {
		document.getElementById(checkBoxName).className="form-group has-feedback has-success";
	} else {
		document.getElementById(checkBoxName).className="form-group has-feedback has-error";
	}
}

function validateYear() {
	var yearInt = parseInt(document.getElementById("year").value);
	year_flag = yearInt >= 2010;
	setCheckAndCheckbox(year_flag, "yearcheck", "yearcheckbox");
}

function validateSeats() {
	var fc_seatsStr = document.getElementById("fc_seats").value;
	var bc_seatsStr = document.getElementById("bc_seats").value;
	var ec_seatsStr = document.getElementById("ec_seats").value;
	var fc_seatsValid = (fc_seatsStr != null && parseInt(fc_seatsStr) >= 0);
	var bc_seatsValid = (bc_seatsStr != null && parseInt(bc_seatsStr) >= 0);
	var ec_seatsValid = (ec_seatsStr != null && parseInt(ec_seatsStr) >= 0);
	seats_flag = fc_seatsValid && bc_seatsValid && ec_seatsValid;
	setCheckAndCheckbox(fc_seatsValid, "fc_seatscheck", "seatcheckbox");
	setCheckAndCheckbox(bc_seatsValid, "bc_seatscheck", "seatcheckbox");
	setCheckAndCheckbox(ec_seatsValid, "ec_seatscheck", "seatcheckbox");
	setCheckbox(seats_flag, "seatcheckbox");
}

function validateLocation() {
	var cur_location = document.getElementById("cur_location").value;
	location_flag = cur_location.length <=4 && /^\w+$/.test(cur_location);
	setCheckAndCheckbox(location_flag, "cur_locationcheck", "cur_locationcheckbox");
}

function validateMaintainence() {
	var maintainence_status = document.getElementById("maintainence_status").value;
	maintainence_flag = maintainence_status.length <=5 && /^[A-Z0-9]+$/.test(maintainence_status);
	setCheckAndCheckbox(maintainence_flag, "maintainence_statuscheck", "maintainence_statuscheckbox");
}

function validatePayload() {
	var range_typical_payload = document.getElementById("range_typical_payload").value;
	range_flag = /^[0-9]+$/.test(range_typical_payload) && parseInt(range_typical_payload) < 10000;
	setCheckAndCheckbox(range_flag, "range_typical_payloadcheck", "range_typical_payloadcheckbox");
}

function validateID() {
	var idcheck=document.getElementById("aircraft_id").value;
	var iatalen=idcheck.length;
	var charcheck=false;
	if (iatalen == 6) {
	    charcheck=/^[0-9]+$/.test(idcheck.substring(1,4)) && idcheck.charAt(0) == 'N' && idcheck.substring(4,6) == "CC";
    }
	id_flag = (iatalen == 6 && charcheck);
	setCheckAndCheckbox(id_flag, "idcheck", "idcheckbox");
}

function set_type(t) {
	//console.log(type);
	switch(t) {
    case 0:
        type = "A319-CEO";
        break;
    case 1:
        type = "A320-CEO";
        break;
    case 2:
        type = "A321-CEO";
        break;
    case 3:
        type = "A319-NEO";
        break;
    case 4:
        type = "A320-NEO";
        break;
    case 5:
        type = "A321-NEO";
        break;
    default:
        type = "Unknown";
	}
	document.getElementById("typebox").className="has-feedback has-success";
	$('#dropdown_type').html(type+"<span class='caret'></span>");
	$('#fc_seats').val(autofill[t][0]);
	$('#bc_seats').val(autofill[t][1]);
	$('#ec_seats').val(autofill[t][2]);
	$('#range_typical_payload').val(autofill[t][3]);
	validateSeats();
	validatePayload();
}

function selectaircraft(index) {
	document.getElementById("aircraft_id").value=aircraft_set[index].aircraft_id;
	document.getElementById("year").value=aircraft_set[index].buy_year;
	document.getElementById("fc_seats").value=aircraft_set[index].fc_seats;
	document.getElementById("bc_seats").value=aircraft_set[index].bc_seats;
	document.getElementById("ec_seats").value=aircraft_set[index].ec_seats;
	document.getElementById("cur_location").value=aircraft_set[index].cur_location;
	document.getElementById("maintainence_status").value=aircraft_set[index].maintainence_status;
	document.getElementById("range_typical_payload").value=aircraft_set[index].range_typical_payload;
	$('#dropdown_type').html(aircraft_set[index].type+"<span class='caret'></span>");
	type=aircraft_set[index].type;
	modify_index=index;
	state_flag=true;
	id_flag=true;
	tz_flag=true;
}

function runAllCheckWithoutID() {
	validateYear();validateSeats();
	validateLocation();validateMaintainence();validatePayload();
	if (type == null) {
		document.getElementById("typebox").className="has-feedback has-error";
	}
}

function set_aircraft() {
	validateID();
	runAllCheckWithoutID();
	if (id_flag && year_flag && seats_flag && location_flag && maintainence_flag && range_flag && type != null) {
		var aircraft_id=document.getElementById("aircraft_id").value;
		var buy_year = document.getElementById("year").value;
		var fc_seats = document.getElementById("fc_seats").value;
		var bc_seats = document.getElementById("bc_seats").value;
		var ec_seats = document.getElementById("ec_seats").value;
		var cur_location = document.getElementById("cur_location").value;
		var maintainence_status = document.getElementById("maintainence_status").value;
		var range_typical_payload = document.getElementById("range_typical_payload").value;
		$.get( "/add_aircraft",{aircraft_id:aircraft_id, type:type, buy_year:buy_year, fc_seats:fc_seats, bc_seats:bc_seats, 
			ec_seats:ec_seats, cur_location:cur_location, maintainence_status:maintainence_status, 
			range_typical_payload:range_typical_payload}, function( data, status ) {
			if (data.coords[0][0]=="Existed") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Aircraft Existed.</span>");
				document.getElementById("error").className="alert alert-warning";
				setCheckAndCheckbox(false, "idcheck", "idcheckbox");
				$("#error").show();
			}
			else {	
				update_display(0);
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Aircraft Added.</span>");
				document.getElementById("error").className="alert alert-success";
				$("#error").show();
				type=null;
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong information or information is incomplete.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}	
}

function mod_aircraft() {
	runAllCheckWithoutID();
	if (year_flag && seats_flag && location_flag && maintainence_flag && range_flag && type != null && modify_index != -1) {
		var aircraft_id=aircraft_set[modify_index].aircraft_id;
		var buy_year = document.getElementById("year").value;
		var fc_seats = document.getElementById("fc_seats").value;
		var bc_seats = document.getElementById("bc_seats").value;
		var ec_seats = document.getElementById("ec_seats").value;
		var cur_location = document.getElementById("cur_location").value;
		var maintainence_status = document.getElementById("maintainence_status").value;
		var range_typical_payload = document.getElementById("range_typical_payload").value;
		$.get( "/modify_aircraft",{aircraft_id:aircraft_id, type:type, buy_year:buy_year, fc_seats:fc_seats, bc_seats:bc_seats, 
			ec_seats:ec_seats, cur_location:cur_location, maintainence_status:maintainence_status, 
			range_typical_payload:range_typical_payload}, function( data, status ) {
			if (data.coords[0][0]!="Success") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Fail to Modify</span>");
				document.getElementById("error").className="alert alert-success";
				setCheckAndCheckbox(false, "idcheck", "idcheckbox");
				$("#error").show();
			}
			else {	
				update_display(1);
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Aircraft Modified.</span>");
				document.getElementById("error").className="alert alert-warning";
				$("#error").show();
				type=null;
				modify_index=-1;
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong information or information is incomplete.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}	
}
