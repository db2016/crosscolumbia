var flight_flag=sessionStorage.getItem("para_flag");
var cc_share_flag=null;

var id_flag=false; // 1
var dept_time_flag=false; //3
var arri_time_flag=false; //3
var dept_date_flag=false; //3
var arri_date_flag=false; //3
var int1_flag=false; //4
var int2_flag=false; //4
var int3_flag=false; //4
var float1_flag=false; //4
var float2_flag=false; //4
var float3_flag=false; //4
var route_flag=false;
var aircraft_flag=false;

var modify_index=-1;
var has_init=false;
var airportList=[];
var route_set=[];
var route_id=null;
var aircraft_set=[];
var aircraft_id=null;
var flight_set=[];

function resetflags() {
	id_flag=false; // 1
	dept_time_flag=false; //3
	arri_time_flag=false; //3
	dept_date_flag=false; //3
	arri_date_flag=false; //3
	int1_flag=false; //4
	int2_flag=false; //4
	int3_flag=false; //4
	float1_flag=false; //4
	float2_flag=false; //4
	float3_flag=false; //4
	route_flag=false;
	aircraft_flag=false;
}

function addFlight() {
	flight_flag = 0;
	update_display(flight_flag);
	resetflags();
}

function modifyFlight() {
	flight_flag = 1;
	update_display(flight_flag);
	resetflags();
}

function setFlightType(flight_type) {
	if (flight_type == 1 || flight_type == 0) {
		cc_share_flag = flight_type;
	}
	update_display(flight_flag);
	resetflags();
}

function initAirportList() {
	$.get("/load_airport",{}, function( data, status ) {
		for (i=0; i<data.coords.length; i++) {
			airportList.push(data.coords[i][0]);
		}
		//console.log(airportList);
	});
}

function update_display(add_modify_flag) {
	var box=document.getElementById("content_box");
	while (box.hasChildNodes()) {   
		box.removeChild(box.firstChild);
	}
	if (cc_share_flag == null) {
		return;
	}
	if (add_modify_flag== 0 && cc_share_flag == 0) {
		$("#content_box").append(" <h3> Add Flight - Cross Columbia Operated </h3>\
									<form class=\"form-horizontal\">\
									 <div class =\"form-group\">\
									  <div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
									   <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
									  </div>\
									 </div>\
									 <div class =\"form-group\">\
									  <div id=\"idcheckbox\">\
									   <label for=\"flight_num\" class=\"col-md-2 control-label\">Flight Number</label>\
									   <div class=\"col-md-3\">\
									    <input type=\"text\" id=\"flight_num\" class=\"form-control\" placeholder=\"XXX\" oninput=\"validateID()\">\
									    <span id=\"idcheck\"></span>\
									   </div>\
									   <div>\
									      <button type=\"button\" class=\"btn btn-primary\" id=\"search_flight\" onclick=\"search_cc_flight()\">Search</button>\
									   </div>\
									   <label class=\"col-md-4 control-label\" id=\"flight_search_result\"></label>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"airportcheckbox\">\
									  <label for=\"origin_airport\" class=\"col-md-2 control-label\">Origin</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"origin_airport_selector\" class=\"selectpicker\">\
									    <!--<option value=\"volvo\">Volvo</option>-->\
									   </select>\
									  </div>\
									  <label for=\"dest_airport\" class=\"col-md-1 control-label\">Destination</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"dest_airport_selector\" class=\"selectpicker\">\
									    <!--<option value=\"volvo\">Volvo</option>-->\
									   </select>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"routeaircraftcheckbox\">\
									  <label for=\"route_id\" class=\"col-md-2 control-label\">Route</label>\
									  <div class=\"col-md-1\">\
									    <input type=\"text\" id=\"route_id\" class=\"form-control col-md-1\" readonly>\
									  </div>\
									  <label for=\"aircraft_id\" class=\"col-md-1 control-label\">Aircraft</label>\
									  <div class=\"col-md-2\">\
									    <input type=\"text\" id=\"aircraft_id\" class=\"form-control  col-md-1\" readonly>\
									  </div>\
									  <div>\
									    <button type=\"button\" class=\"btn btn-primary\" id=\"select_route\" onclick=\"selected_route()\">Select Route & Aircraft</button>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"tablebox\">\
									 </div>\
									 <div class =\"form-group\" id=\"deptcheckbox\">\
									  <label for=\"dept_date_selector\" class=\"col-md-2 control-label\">Departure Date</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"dept_date_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd\" oninput=\"validDate(0)\">\
                                       <span id=\"dept_date_check\"></span>\
									  </div>\
									  <label for=\"dept_time_selector\" class=\"col-md-2 control-label\">Departure Time</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"dept_time_selector\" class=\"form-control\" placeholder=\"hh:mm:ss\" oninput=\"validTime(0)\">\
                                       <span id=\"dept_time_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"arricheckbox\">\
									  <label for=\"arrival_date_selector\" class=\"col-md-2 control-label\">Arrival Date</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"arrival_date_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd\" oninput=\"validDate(1)\">\
                                       <span id=\"arrival_date_check\"></span>\
									  </div>\
									  <label for=\"arrival_time_selector\" class=\"col-md-2 control-label\">Arrival Time</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"arrival_time_selector\" class=\"form-control\" placeholder=\"hh:mm:ss\" oninput=\"validTime(1)\">\
                                       <span id=\"arrival_time_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"statusbox\">\
									  <label for=\"status_selector\" class=\"col-md-2 control-label\">Status</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"status_selector\" class=\"selectpicker\">\
									    <option value=\"ARR\">ARRIVED</option>\
									    <option value=\"BOA\">BOARDING</option>\
									    <option value=\"CCL\">CANCELLED</option>\
									    <option value=\"DLY\">DELAYED</option>\
									    <option value=\"EME\">EMERGENCY</option>\
									    <option value=\"RDL\">EN ROUTE DELAYED</option>\
									    <option value=\"RER\">EN ROUTE EARILER</option>\
									    <option value=\"RES\">RESCHEDULED</option>\
									    <option value=\"ROU\">EN ROUTE ON TIME</option>\
									    <option value=\"SCH\" selected=\"selected\">SCHEDULED</option>\
									    <option value=\"UNK\">UNKOWN</option>\
									   </select>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"ticketbox\">\
									  <label class=\"col-md-2 control-label\">Ticket Type</label>\
									  <label class=\"col-md-2 control-label\">First</label>\
									  <label class=\"col-md-2 control-label\">Business</label>\
									  <label class=\"col-md-2 control-label\">Economic</label>\
									 </div>\
									 <div class =\"form-group\" id=\"tickettotalbox\">\
									  <label class=\"col-md-2 control-label\">Total Ticket</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_total\" class=\"col-md-2 form-control\" readonly>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_total\" class=\"col-md-2 form-control\" readonly>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_total\" class=\"col-md-2 form-control\" readonly>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"ticketfullpricebox\">\
									  <label class=\"col-md-2 control-label\">Full Price</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_full_price\" class=\"col-md-2 form-control\" oninput=\"validInt(3)\">\
                                       <span id=\"first_full_price_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_full_price\" class=\"col-md-2 form-control\" oninput=\"validInt(4)\">\
                                       <span id=\"business_full_price_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_full_price\" class=\"col-md-2 form-control\" oninput=\"validInt(5)\">\
                                       <span id=\"economic_full_price_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"pricepercentbox\">\
									  <label class=\"col-md-2 control-label\">Price Percent</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_price_percent\" class=\"col-md-2 form-control\" oninput=\"validFloat(3)\">\
                                       <span id=\"first_price_percent_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_price_percent\" class=\"col-md-2 form-control\" oninput=\"validFloat(4)\">\
                                       <span id=\"business_price_percent_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_price_percent\" class=\"col-md-2 form-control\" oninput=\"validFloat(5)\">\
                                       <span id=\"economic_price_percent_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\">\
									  <div style=\"text-align:center\">\
									   <button type=\"button\" class=\"btn btn-primary\" id=\"setflight\" onclick=\"set_flight_cc()\">Submit</button>\
									  </div>\
									 </div>\
									</form>");
	} if (add_modify_flag== 0 && cc_share_flag == 1) {
		$("#content_box").append(" <h3> Add Flight - Code Share </h3>\
									<form class=\"form-horizontal\">\
									 <div class =\"form-group\">\
									  <div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
									   <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
									  </div>\
									 </div>\
									 <div class =\"form-group\">\
									  <div id=\"idcheckbox\">\
									   <label for=\"flight_num\" class=\"col-md-2 control-label\">Flight Number</label>\
									   <div class=\"col-md-3\">\
									    <input type=\"text\" id=\"flight_num\" class=\"form-control\" placeholder=\"XXXX\" oninput=\"validateID()\">\
									    <span id=\"idcheck\"></span>\
									   </div>\
									   <div>\
									    <button type=\"button\" class=\"btn btn-primary\" id=\"search_flight\" onclick=\"search_shared_flight()\">Search</button>\
									   </div>\
									   <label class=\"col-md-4 control-label\" id=\"flight_search_result\"></label>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"airportcheckbox\">\
									  <label for=\"origin_airport\" class=\"col-md-2 control-label\">Origin</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"origin_airport_selector\" class=\"selectpicker\">\
									   </select>\
									  </div>\
									  <label for=\"dest_airport\" class=\"col-md-1 control-label\">Destination</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"dest_airport_selector\" class=\"selectpicker\">\
									   </select>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"sharedairlineinfocheckbox\">\
									  <label for=\"host_airline\" class=\"col-md-2 control-label\">Host Airline</label>\
									  <div class=\"col-md-1\">\
									    <input type=\"text\" id=\"host_airline\" placeholder=\"XX\" class=\"form-control col-md-1\">\
									  </div>\
									  <label for=\"host_flight_number\" class=\"col-md-3 control-label\">Host Flight Number</label>\
									  <div class=\"col-md-2\">\
									    <input type=\"text\" id=\"host_flight_number\" class=\"form-control  col-md-1\">\
									  </div>\
									  <label for=\"aircraft_type\" class=\"col-md-2 control-label\">Aircraft Type</label>\
									  <div class=\"col-md-1\">\
									    <input type=\"text\" id=\"aircraft_type\" class=\"form-control  col-md-1\">\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"deptcheckbox\">\
									  <label for=\"dept_date_selector\" class=\"col-md-2 control-label\">Departure Date</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"dept_date_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd\" oninput=\"validDate(0)\">\
                                       <span id=\"dept_date_check\"></span>\
									  </div>\
									  <label for=\"dept_time_selector\" class=\"col-md-2 control-label\">Departure Time</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"dept_time_selector\" class=\"form-control\" placeholder=\"hh:mm:ss\" oninput=\"validTime(0)\">\
                                       <span id=\"dept_time_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"arricheckbox\">\
									  <label for=\"arrival_date_selector\" class=\"col-md-2 control-label\">Arrival Date</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"arrival_date_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd\" oninput=\"validDate(1)\">\
                                       <span id=\"arrival_date_check\"></span>\
									  </div>\
									  <label for=\"arrival_time_selector\" class=\"col-md-2 control-label\">Arrival Time</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"arrival_time_selector\" class=\"form-control\" placeholder=\"hh:mm:ss\" oninput=\"validTime(1)\">\
                                       <span id=\"arrival_time_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"statusbox\">\
									  <label for=\"status_selector\" class=\"col-md-2 control-label\">Status</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"status_selector\" class=\"selectpicker\">\
									    <option value=\"ARR\">ARRIVED</option>\
									    <option value=\"BOA\">BOARDING</option>\
									    <option value=\"CCL\">CANCELLED</option>\
									    <option value=\"DLY\">DELAYED</option>\
									    <option value=\"EME\">EMERGENCY</option>\
									    <option value=\"RDL\">EN ROUTE DELAYED</option>\
									    <option value=\"RER\">EN ROUTE EARILER</option>\
									    <option value=\"RES\">RESCHEDULED</option>\
									    <option value=\"ROU\">EN ROUTE ON TIME</option>\
									    <option value=\"SCH\" selected=\"selected\">SCHEDULED</option>\
									    <option value=\"UNK\">UNKOWN</option>\
									   </select>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"ticketbox\">\
									  <label class=\"col-md-2 control-label\">Ticket Type</label>\
									  <label class=\"col-md-2 control-label\">First</label>\
									  <label class=\"col-md-2 control-label\">Business</label>\
									  <label class=\"col-md-2 control-label\">Economic</label>\
									 </div>\
									 <div class =\"form-group\" id=\"tickettotalbox\">\
									  <label class=\"col-md-2 control-label\">Total Ticket</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_total\" class=\"col-md-2 form-control\" oninput=\"validInt(0)\">\
                                        <span id=\"first_total_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_total\" class=\"col-md-2 form-control\" oninput=\"validInt(1)\">\
                                        <span id=\"business_total_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_total\" class=\"col-md-2 form-control\" oninput=\"validInt(2)\">\
                                        <span id=\"economic_total_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"ticketfullpricebox\">\
									  <label class=\"col-md-2 control-label\">Price</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_full_price\" class=\"col-md-2 form-control\" oninput=\"validFloat(0)\">\
                                        <span id=\"first_full_price_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_full_price\" class=\"col-md-2 form-control\" oninput=\"validFloat(1)\">\
                                        <span id=\"business_full_price_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_full_price\" class=\"col-md-2 form-control\" oninput=\"validFloat(2)\">\
                                        <span id=\"economic_full_price_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\">\
									  <div style=\"text-align:center\">\
									   <button type=\"button\" class=\"btn btn-primary\" id=\"setflight\" onclick=\"set_flight_share()\">Submit</button>\
									  </div>\
									 </div>\
									</form>");
	} else if (add_modify_flag == 1 && cc_share_flag == 0) {
		$("#content_box").append(" <h3> Modify Flight - Cross Columbia Operated </h3>\
                                    <form class=\"form-horizontal\">\
									 <div class =\"form-group\">\
									  <div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
									   <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
									  </div>\
									 </div>\
									 <div class =\"form-group\">\
									  <div id=\"idcheckbox\">\
									   <label for=\"flight_num\" class=\"col-md-2 control-label\">Flight Number</label>\
									   <div class=\"col-md-3\">\
									    <input type=\"text\" id=\"flight_num\" class=\"form-control\" placeholder=\"XXX\" readonly>\
									    <span id=\"idcheck\"></span>\
									   </div>\
									   <label class=\"col-md-4 control-label\" id=\"flight_search_result\"></label>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"airportcheckbox\">\
									  <label for=\"origin_airport\" class=\"col-md-2 control-label\">Origin</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"origin_airport_selector\" class=\"selectpicker\" disabled>\
									    <!--<option value=\"volvo\">Volvo</option>-->\
									   </select>\
									  </div>\
									  <label for=\"dest_airport\" class=\"col-md-1 control-label\">Destination</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"dest_airport_selector\" class=\"selectpicker\" disabled>\
									    <!--<option value=\"volvo\">Volvo</option>-->\
									   </select>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"routeaircraftcheckbox\">\
									  <label for=\"route_id\" class=\"col-md-2 control-label\">Route</label>\
									  <div class=\"col-md-1\">\
									    <input type=\"text\" id=\"route_id\" class=\"form-control col-md-1\" readonly>\
									  </div>\
									  <label for=\"aircraft_id\" class=\"col-md-1 control-label\">Aircraft</label>\
									  <div class=\"col-md-2\">\
									    <input type=\"text\" id=\"aircraft_id\" class=\"form-control  col-md-1\" readonly>\
									  </div>\
									  <div>\
									    <button type=\"button\" class=\"btn btn-primary\" id=\"select_route\" onclick=\"selected_route_modify()\">Select Route & Aircraft</button>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"tablebox\">\
									 </div>\
									 <div class =\"form-group\" id=\"deptcheckbox\">\
									  <label for=\"dept_date_selector\" class=\"col-md-2 control-label\">Schedule Departure</label>\
									  <div class=\"col-md-3\">\
									   <input type=\"text\" id=\"dept_date_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd hh:mm:ss\" oninput=\"validDateTime(0)\">\
                                       <span id=\"dept_date_check\"></span>\
									  </div>\
									  <label for=\"dept_time_selector\" class=\"col-md-2 control-label\">Actual Departure</label>\
									  <div class=\"col-md-3\">\
									   <input type=\"text\" id=\"dept_time_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd hh:mm:ss\" oninput=\"validDateTime(1)\">\
                                       <span id=\"dept_time_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"arricheckbox\">\
									  <label for=\"arrival_date_selector\" class=\"col-md-2 control-label\">Schedule Arrival</label>\
									  <div class=\"col-md-3\">\
									   <input type=\"text\" id=\"arrival_date_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd hh:mm:ss\" oninput=\"validDateTime(2)\">\
                                       <span id=\"arrival_date_check\"></span>\
									  </div>\
									  <label for=\"arrival_time_selector\" class=\"col-md-2 control-label\">Actual Arrival</label>\
									  <div class=\"col-md-3\">\
									   <input type=\"text\" id=\"arrival_time_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd hh:mm:ss\" oninput=\"validDateTime(3)\">\
                                       <span id=\"arrival_time_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"statusbox\">\
									  <label for=\"status_selector\" class=\"col-md-2 control-label\">Status</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"status_selector\" class=\"selectpicker\">\
									    <option value=\"ARR\">ARRIVED</option>\
									    <option value=\"BOA\">BOARDING</option>\
									    <option value=\"CCL\">CANCELLED</option>\
									    <option value=\"DLY\">DELAYED</option>\
									    <option value=\"EME\">EMERGENCY</option>\
									    <option value=\"RDL\">EN ROUTE DELAYED</option>\
									    <option value=\"RER\">EN ROUTE EARILER</option>\
									    <option value=\"RES\">RESCHEDULED</option>\
									    <option value=\"ROU\">EN ROUTE ON TIME</option>\
									    <option value=\"SCH\" selected=\"selected\">SCHEDULED</option>\
									    <option value=\"UNK\">UNKOWN</option>\
									   </select>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"ticketbox\">\
									  <label class=\"col-md-2 control-label\">Ticket Type</label>\
									  <label class=\"col-md-2 control-label\">First</label>\
									  <label class=\"col-md-2 control-label\">Business</label>\
									  <label class=\"col-md-2 control-label\">Economic</label>\
									 </div>\
									 <div class =\"form-group\" id=\"tickettotalbox\">\
									  <label class=\"col-md-2 control-label\">Total Ticket</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_total\" class=\"col-md-2 form-control\" readonly>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_total\" class=\"col-md-2 form-control\" readonly>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_total\" class=\"col-md-2 form-control\" readonly>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"ticketfullpricebox\">\
									  <label class=\"col-md-2 control-label\">Full Price</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_full_price\" class=\"col-md-2 form-control\" oninput=\"validInt(3)\">\
                                       <span id=\"first_full_price_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_full_price\" class=\"col-md-2 form-control\" oninput=\"validInt(4)\">\
                                       <span id=\"business_full_price_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_full_price\" class=\"col-md-2 form-control\" oninput=\"validInt(5)\">\
                                       <span id=\"economic_full_price_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"pricepercentbox\">\
									  <label class=\"col-md-2 control-label\">Price Percent</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_price_percent\" class=\"col-md-2 form-control\" oninput=\"validFloat(3)\">\
                                       <span id=\"first_price_percent_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_price_percent\" class=\"col-md-2 form-control\" oninput=\"validFloat(4)\">\
                                       <span id=\"business_price_percent_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_price_percent\" class=\"col-md-2 form-control\" oninput=\"validFloat(5)\">\
                                       <span id=\"economic_price_percent_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\">\
									  <div style=\"text-align:center\">\
									   <button type=\"button\" class=\"btn btn-primary\" id=\"setflight\" onclick=\"modify_flight_cc()\">Submit</button>\
									  </div>\
									 </div>\
									</form>\
                                    <div style=\"height: 300px; overflow-y:auto;\">\
									<label for=\"flight_table\">Flight Cross Columbia</label>\
										<table class=\"table table-bordered table-condensed table-striped table-hover\" id=\"flight_table\">\
											<thead>\
												<tr><th>Flight Number</th><th>Departure Airport</th><th>Arrival Airport</th><th>Flight Date</th></tr>\
											</thead>\
											<tbody id=\"flight_list\">\
											</tbody>\
										</table>\
								    </div>");
		refreshtable_cc();
	}
	else if (add_modify_flag == 1 && cc_share_flag == 1) {
		$("#content_box").append(" <h3> Modify Flight - Code Share </h3>\
                                    <form class=\"form-horizontal\">\
									 <div class =\"form-group\">\
									  <div class=\"alert alert-warning\" style=\"display:none;\" id=\"error\">\
									   <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\
									  </div>\
									 </div>\
									 <div class =\"form-group\">\
									  <div id=\"idcheckbox\">\
									   <label for=\"flight_num\" class=\"col-md-2 control-label\">Flight Number</label>\
									   <div class=\"col-md-3\">\
									    <input type=\"text\" id=\"flight_num\" class=\"form-control\" placeholder=\"XXXX\" readonly>\
									    <span id=\"idcheck\"></span>\
									   </div>\
									   <label class=\"col-md-4 control-label\" id=\"flight_search_result\"></label>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"airportcheckbox\">\
									  <label for=\"origin_airport\" class=\"col-md-2 control-label\">Origin</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"origin_airport_selector\" class=\"selectpicker\" disabled>\
									   </select>\
									  </div>\
									  <label for=\"dest_airport\" class=\"col-md-1 control-label\">Destination</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"dest_airport_selector\" class=\"selectpicker\" disabled>\
									   </select>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"sharedairlineinfocheckbox\">\
									  <label for=\"host_airline\" class=\"col-md-2 control-label\">Host Airline</label>\
									  <div class=\"col-md-1\">\
									    <input type=\"text\" id=\"host_airline\" placeholder=\"XX\" class=\"form-control col-md-1\">\
									  </div>\
									  <label for=\"host_flight_number\" class=\"col-md-3 control-label\">Host Flight Number</label>\
									  <div class=\"col-md-2\">\
									    <input type=\"text\" id=\"host_flight_number\" class=\"form-control  col-md-1\">\
									  </div>\
									  <label for=\"aircraft_type\" class=\"col-md-2 control-label\">Aircraft Type</label>\
									  <div class=\"col-md-1\">\
									    <input type=\"text\" id=\"aircraft_type\" class=\"form-control  col-md-1\">\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"deptcheckbox\">\
									  <label for=\"dept_date_selector\" class=\"col-md-2 control-label\">Schedule Departure</label>\
									  <div class=\"col-md-3\">\
									   <input type=\"text\" id=\"dept_date_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd hh:mm:ss\" oninput=\"validDateTime(0)\">\
                                       <span id=\"dept_date_check\"></span>\
									  </div>\
									  <label for=\"dept_time_selector\" class=\"col-md-2 control-label\">Actual Departure</label>\
									  <div class=\"col-md-3\">\
									   <input type=\"text\" id=\"dept_time_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd hh:mm:ss\" oninput=\"validDateTime(1)\">\
                                       <span id=\"dept_time_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"arricheckbox\">\
									  <label for=\"arrival_date_selector\" class=\"col-md-2 control-label\">Schedule Arrival</label>\
									  <div class=\"col-md-3\">\
									   <input type=\"text\" id=\"arrival_date_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd hh:mm:ss\" oninput=\"validDateTime(2)\">\
                                       <span id=\"arrival_date_check\"></span>\
									  </div>\
									  <label for=\"arrival_time_selector\" class=\"col-md-2 control-label\">Actual Arrival</label>\
									  <div class=\"col-md-3\">\
									   <input type=\"text\" id=\"arrival_time_selector\" class=\"form-control\" placeholder=\"yyyy-mm-dd hh:mm:ss\" oninput=\"validDateTime(3)\">\
                                       <span id=\"arrival_time_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"statusbox\">\
									  <label for=\"status_selector\" class=\"col-md-2 control-label\">Status</label>\
									  <div class=\"col-md-2\">\
									   <select id=\"status_selector\" class=\"selectpicker\">\
									    <option value=\"ARR\">ARRIVED</option>\
									    <option value=\"BOA\">BOARDING</option>\
									    <option value=\"CCL\">CANCELLED</option>\
									    <option value=\"DLY\">DELAYED</option>\
									    <option value=\"EME\">EMERGENCY</option>\
									    <option value=\"RDL\">EN ROUTE DELAYED</option>\
									    <option value=\"RER\">EN ROUTE EARILER</option>\
									    <option value=\"RES\">RESCHEDULED</option>\
									    <option value=\"ROU\">EN ROUTE ON TIME</option>\
									    <option value=\"SCH\" selected=\"selected\">SCHEDULED</option>\
									    <option value=\"UNK\">UNKOWN</option>\
									   </select>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"ticketbox\">\
									  <label class=\"col-md-2 control-label\">Ticket Type</label>\
									  <label class=\"col-md-2 control-label\">First</label>\
									  <label class=\"col-md-2 control-label\">Business</label>\
									  <label class=\"col-md-2 control-label\">Economic</label>\
									 </div>\
									 <div class =\"form-group\" id=\"tickettotalbox\">\
									  <label class=\"col-md-2 control-label\">Total Ticket</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_total\" class=\"col-md-2 form-control\" oninput=\"validInt(0)\">\
                                        <span id=\"first_total_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_total\" class=\"col-md-2 form-control\" oninput=\"validInt(1)\">\
                                        <span id=\"business_total_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_total\" class=\"col-md-2 form-control\" oninput=\"validInt(2)\">\
                                        <span id=\"economic_total_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\" id=\"ticketfullpricebox\">\
									  <label class=\"col-md-2 control-label\">Price</label>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"first_full_price\" class=\"col-md-2 form-control\" oninput=\"validFloat(0)\">\
                                        <span id=\"first_full_price_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"business_full_price\" class=\"col-md-2 form-control\" oninput=\"validFloat(1)\">\
                                        <span id=\"business_full_price_check\"></span>\
									  </div>\
									  <div class=\"col-md-2\">\
									   <input type=\"text\" id=\"economic_full_price\" class=\"col-md-2 form-control\" oninput=\"validFloat(2)\">\
                                        <span id=\"economic_full_price_check\"></span>\
									  </div>\
									 </div>\
									 <div class =\"form-group\">\
									  <div style=\"text-align:center\">\
									   <button type=\"button\" class=\"btn btn-primary\" id=\"setflight\" onclick=\"modify_flight_share()\">Submit</button>\
									  </div>\
									 </div>\
									</form>\
                                    <div style=\"height: 300px; overflow-y:auto;\">\
									<label for=\"flight_table\">Flight Cross Columbia</label>\
										<table class=\"table table-bordered table-condensed table-striped table-hover\" id=\"flight_table\">\
											<thead>\
												<tr><th>Flight Number</th><th>Departure Airport</th><th>Arrival Airport</th><th>Flight Date</th></tr>\
											</thead>\
											<tbody id=\"flight_list\">\
											</tbody>\
										</table>\
								    </div>");
		refreshtable_share();
	}
	$('#origin_airport_selector').append($('<option>', {value: "Select an airport",text: "Select an airport"}));
	$('#dest_airport_selector').append($('<option>', {value: "Select an airport",text: "Select an airport"}));
	for (i=0; i<airportList.length; i++){
		$('#origin_airport_selector').append($('<option>', {value: airportList[i],text: airportList[i]}));
		$('#dest_airport_selector').append($('<option>', {value: airportList[i],text: airportList[i]}));
	}
}

if (!has_init) {
	initAirportList();
	update_display(flight_flag);
	has_init = true;
}

function refreshtable_cc() {
	$("#flight_list tr").remove();
	flight_set=[];
	currentDate=new Date();
	currentDatestr=currentDate.getFullYear().toString()+"-"+(currentDate.getMonth()+1).toString()+"-"+currentDate.getDate().toString();
	//console.log(currentDatestr);
	$.get("/load_flight_cc",{current:currentDatestr}, function( data, status ){
		//console.log(data.coords);
		for (i=0; i<data.coords.length; i++){
			flight_set.push ({flight_id: data.coords[i][0], flight_no: data.coords[i][1], flight_date: data.coords[i][2], flight_status: data.coords[i][3], sched_dept_t: data.coords[i][4], act_dept_t: data.coords[i][5], sched_arr_t: data.coords[i][6], act_arr_t: data.coords[i][7], route_id: data.coords[i][8], dept_airport: data.coords[i][9], arr_airport: data.coords[i][10], aircraft_id: data.coords[i][11], fc_seats: data.coords[i][12], bc_seats: data.coords[i][13], ec_seats: data.coords[i][14], fc_full_price: data.coords[i][15], fc_sold_seats: data.coords[i][16], fc_price_rate: data.coords[i][17], bc_full_price: data.coords[i][18], bc_sold_seats: data.coords[i][19], bc_price_rate: data.coords[i][20], ec_full_price: data.coords[i][21], ec_sold_seats: data.coords[i][22], ec_price_rate: data.coords[i][23]});
		}
		//console.log(flight_set);
		//add every airport to a dropdown
		for (var i = 0; i<flight_set.length; ++i){   
			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='selectflight_cc("+i.toString()+")'><td>"+
			flight_set[i].flight_no+"</td><td>"+flight_set[i].dept_airport+"</td><td>"+flight_set[i].arr_airport+"</td><td>"+flight_set[i].flight_date+"</td></tr>";
	//console.log(newline);
			$("#flight_list").append(newline);
		}
	});
}

function refreshtable_share() {
	$("#flight_list tr").remove();
	flight_set=[];
	currentDate=new Date();
	currentDatestr=currentDate.getFullYear().toString()+"-"+(currentDate.getMonth()+1).toString()+"-"+currentDate.getDate().toString();
	//console.log(currentDatestr);
	$.get("/load_flight_share",{current:currentDatestr}, function( data, status ){
		//console.log(data.coords);
		for (i=0; i<data.coords.length; i++){
			flight_set.push ({flight_id: data.coords[i][0], flight_no: data.coords[i][1], flight_date: data.coords[i][2], flight_status: data.coords[i][3], sched_dept_t: data.coords[i][4], act_dept_t: data.coords[i][5], sched_arr_t: data.coords[i][6], act_arr_t: data.coords[i][7], host_airline: data.coords[i][8],host_flight_no: data.coords[i][9],dept_airport: data.coords[i][10], arr_airport: data.coords[i][11], aircraft_type: data.coords[i][12], fc_price: data.coords[i][13], fc_remain_seats: data.coords[i][14], bc_price: data.coords[i][15], bc_remain_seats: data.coords[i][16], ec_price: data.coords[i][17], ec_remain_seats: data.coords[i][18]});
		}
		//console.log(flight_set);
		//add every airport to a dropdown
		for (var i = 0; i<flight_set.length; ++i){   
			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='selectflight_share("+i.toString()+")'><td>"+
			flight_set[i].flight_no+"</td><td>"+flight_set[i].dept_airport+"</td><td>"+flight_set[i].arr_airport+"</td><td>"+flight_set[i].flight_date+"</td></tr>";
	//console.log(newline);
			$("#flight_list").append(newline);
		}
	});
}

function selectflight_cc(index) {
	reset_cc();
	id_flag=true;
	dept_time_flag=true; //3
	arri_time_flag=true; //3
	dept_date_flag=true; //3
	arri_date_flag=true; //3
	int1_flag=true; //4
	int2_flag=true; //4
	int3_flag=true; //4
	float1_flag=true; //4
	float2_flag=true; //4
	float3_flag=true; //4
	route_flag=true;
	aircraft_flag=true;
	document.getElementById("flight_num").value=flight_set[index].flight_no;
	document.getElementById("origin_airport_selector").value=flight_set[index].dept_airport;
	document.getElementById("dest_airport_selector").value=flight_set[index].arr_airport;
	document.getElementById("dept_date_selector").value=flight_set[index].sched_dept_t;
	document.getElementById("dept_time_selector").value=flight_set[index].act_dept_t;
	document.getElementById("arrival_date_selector").value=flight_set[index].sched_arr_t;
	document.getElementById("arrival_time_selector").value=flight_set[index].act_arr_t;
	document.getElementById("first_total").value=flight_set[index].fc_seats;
	document.getElementById("business_total").value=flight_set[index].bc_seats;
	document.getElementById("economic_total").value=flight_set[index].ec_seats;
	document.getElementById("first_price_percent").value=flight_set[index].fc_price_rate;
	document.getElementById("business_price_percent").value=flight_set[index].bc_price_rate;
	document.getElementById("economic_price_percent").value=flight_set[index].ec_price_rate;
	document.getElementById("first_full_price").value=flight_set[index].fc_full_price;
	document.getElementById("business_full_price").value=flight_set[index].bc_full_price;
	document.getElementById("economic_full_price").value=flight_set[index].ec_full_price;
	document.getElementById("status_selector").value=flight_set[index].flight_status;
	document.getElementById("route_id").value=flight_set[index].route_id;
	document.getElementById("aircraft_id").value=flight_set[index].aircraft_id;
	modify_index=index;
}

function selectflight_share(index) {
	reset_shared();
	id_flag=true;
	dept_time_flag=true; //3
	arri_time_flag=true; //3
	dept_date_flag=true; //3
	arri_date_flag=true; //3
	int1_flag=true; //4
	int2_flag=true; //4
	int3_flag=true; //4
	float1_flag=true; //4
	float2_flag=true; //4
	float3_flag=true; //4
	route_flag=true;
	aircraft_flag=true;
	document.getElementById("flight_num").value=flight_set[index].flight_no;
	document.getElementById("origin_airport_selector").value=flight_set[index].dept_airport;
	document.getElementById("dest_airport_selector").value=flight_set[index].arr_airport;
	document.getElementById("dept_date_selector").value=flight_set[index].sched_dept_t;
	document.getElementById("dept_time_selector").value=flight_set[index].act_dept_t;
	document.getElementById("arrival_date_selector").value=flight_set[index].sched_arr_t;
	document.getElementById("arrival_time_selector").value=flight_set[index].act_arr_t;
	document.getElementById("first_total").value=flight_set[index].fc_remain_seats;
	document.getElementById("business_total").value=flight_set[index].bc_remain_seats;
	document.getElementById("economic_total").value=flight_set[index].ec_remain_seats;
	document.getElementById("first_full_price").value=flight_set[index].fc_price;
	document.getElementById("business_full_price").value=flight_set[index].bc_price;
	document.getElementById("economic_full_price").value=flight_set[index].ec_price;
	document.getElementById("status_selector").value=flight_set[index].flight_status;
	document.getElementById("host_airline").value=flight_set[index].host_airline;
	document.getElementById("host_flight_number").value=flight_set[index].host_flight_no;
	document.getElementById("aircraft_type").value=flight_set[index].aircraft_type;
	modify_index=index;
}

function setCheckAndCheckbox(isValid, checkName, checkBoxName) {
	if (isValid) {
		document.getElementById(checkName).className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById(checkBoxName).className="form-group has-feedback has-success";
	} else {
		document.getElementById(checkName).className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById(checkBoxName).className="form-group has-feedback has-error";
	}
}

function resetCheckAndCheckbox(checkName, checkBoxName) {
	document.getElementById(checkName).className="";
	document.getElementById(checkBoxName).className="form-group has-feedback";
}

function validateID() {
	var idcheck=document.getElementById("flight_num").value;
	var idInt=parseInt(idcheck);
	if (cc_share_flag == '0') {
		id_flag = (idInt > 0 && idInt < 1000);
	} else {
		id_flag = (idInt > 1000 && idInt < 9999);
	}
	setCheckAndCheckbox(id_flag, "idcheck", "idcheckbox");
}

function validDate(type) {
	if (type===0) {
		var time=document.getElementById("dept_date_selector").value;
		var timelen=time.length;
		if (timelen!=10) {
			dept_date_flag=false;
		}
		else {
			timearray=time.split('-');
			if ((parseInt(timearray[1])>0) && (parseInt(timearray[1])<=12) && (parseInt(timearray[2])>0) && (parseInt(timearray[2])<=31)) {
				dept_date_flag=true;
			}
			else {
				dept_date_flag=false;
			}
		}
		setCheckAndCheckbox(dept_date_flag, "dept_date_check", "deptcheckbox");
	}
	else if (type===1) {
		var time=document.getElementById("arrival_date_selector").value;
		var timelen=time.length;
		if (timelen!=10) {
			arri_date_flag=false;
		}
		else {
			timearray=time.split('-');
			if ((parseInt(timearray[1])>0) && (parseInt(timearray[1])<=12) && (parseInt(timearray[2])>0) && (parseInt(timearray[2])<=31)) {
				arri_date_flag=true;
			}
			else {
				arri_date_flag=false;
			}
		}
		setCheckAndCheckbox(arri_date_flag, "arrival_date_check", "arricheckbox");
	}
}

function validTime(type) {
	if (type===0) {
		var time=document.getElementById("dept_time_selector").value;
		var timelen=time.length;
		if (timelen!=8) {
			dept_time_flag=false;
		}
		else {
			timearray=time.split(':');
			if ((parseInt(timearray[0])>=0) && (parseInt(timearray[0])<=23) && (parseInt(timearray[1])>=0) && (parseInt(timearray[1])<=59) && (parseInt(timearray[2])>=0) && (parseInt(timearray[2])<=59)) {
				dept_time_flag=true;
			}
			else {
				dept_time_flag=false;
			}
		}
		setCheckAndCheckbox(dept_time_flag, "dept_time_check", "deptcheckbox");
	}
	else if (type===1) {
		var time=document.getElementById("arrival_time_selector").value;
		var timelen=time.length;
		if (timelen!=8) {
			arri_time_flag=false;
		}
		else {
			timearray=time.split(':');
			if ((parseInt(timearray[0])>=0) && (parseInt(timearray[0])<=23) && (parseInt(timearray[1])>=0) && (parseInt(timearray[1])<=59) && (parseInt(timearray[2])>=0) && (parseInt(timearray[2])<=59)) {
				arri_time_flag=true;
			}
			else {
				arri_time_flag=false;
			}
		}
		setCheckAndCheckbox(arri_time_flag, "arrival_time_check", "arricheckbox");
	}
}

function validDateTime(type) {
	if (type===0) {
		var time=document.getElementById("dept_date_selector").value;
		var timelen=time.length;
		if (timelen!=19) {
			dept_date_flag=false;
		}
		else {
			timearray=[];
			temp=time.split(' ');
			//console.log(temp);
			timearray.push.apply(timearray,temp[0].split('-'));
			//console.log(timearray);
			timearray.push.apply(timearray,temp[1].split(':'));
			//console.log(timearray);
			if ((parseInt(timearray[1])>0) && (parseInt(timearray[1])<=12) && (parseInt(timearray[2])>0) && (parseInt(timearray[2])<=31) && (parseInt(timearray[3])>=0) && (parseInt(timearray[3])<=23) && (parseInt(timearray[4])>=0) && (parseInt(timearray[4])<=59) && (parseInt(timearray[5])>=0) && (parseInt(timearray[5])<=59)) {
				dept_date_flag=true;
			}
			else {
				dept_date_flag=false;
			}
		}
		setCheckAndCheckbox(dept_date_flag, "dept_date_check", "deptcheckbox");
	}
	else if (type===1) {
		var time=document.getElementById("dept_time_selector").value;
		var timelen=time.length;
		if (timelen!=19) {
			dept_time_flag=false;
		}
		else {
			timearray=[];
			temp=time.split(' ');
			timearray.push.apply(timearray,temp[0].split('-'));
			timearray.push.apply(timearray,temp[1].split(':'));
			if ((parseInt(timearray[1])>0) && (parseInt(timearray[1])<=12) && (parseInt(timearray[2])>0) && (parseInt(timearray[2])<=31) && (parseInt(timearray[3])>=0) && (parseInt(timearray[3])<=23) && (parseInt(timearray[4])>=0) && (parseInt(timearray[4])<=59) && (parseInt(timearray[5])>=0) && (parseInt(timearray[5])<=59)) {
				dept_time_flag=true;
			}
			else {
				dept_time_flag=false;
			}
		}
		setCheckAndCheckbox(dept_time_flag, "dept_time_check", "deptcheckbox");
	}
	else if (type===2) {
		var time=document.getElementById("arrival_date_selector").value;
		var timelen=time.length;
		if (timelen!=19) {
			arri_date_flag=false;
		}
		else {
			timearray=[];
			temp=time.split(' ');
			timearray.push.apply(timearray,temp[0].split('-'));
			timearray.push.apply(timearray,temp[1].split(':'));
			if ((parseInt(timearray[1])>0) && (parseInt(timearray[1])<=12) && (parseInt(timearray[2])>0) && (parseInt(timearray[2])<=31) && (parseInt(timearray[3])>=0) && (parseInt(timearray[3])<=23) && (parseInt(timearray[4])>=0) && (parseInt(timearray[4])<=59) && (parseInt(timearray[5])>=0) && (parseInt(timearray[5])<=59)) {
				arri_date_flag=true;
			}
			else {
				arri_date_flag=false;
			}
		}
		setCheckAndCheckbox(arri_date_flag, "arrival_date_check", "arricheckbox");
	}
	else if (type===3) {
		var time=document.getElementById("arrival_time_selector").value;
		var timelen=time.length;
		if (timelen!=19) {
			arri_time_flag=false;
		}
		else {
			timearray=[];
			temp=time.split(' ');
			timearray.push.apply(timearray,temp[0].split('-'));
			timearray.push.apply(timearray,temp[1].split(':'));
			if ((parseInt(timearray[1])>0) && (parseInt(timearray[1])<=12) && (parseInt(timearray[2])>0) && (parseInt(timearray[2])<=31) && (parseInt(timearray[3])>=0) && (parseInt(timearray[3])<=23) && (parseInt(timearray[4])>=0) && (parseInt(timearray[4])<=59) && (parseInt(timearray[5])>=0) && (parseInt(timearray[5])<=59)) {
				arri_time_flag=true;
			}
			else {
				arri_time_flag=false;
			}
		}
		setCheckAndCheckbox(arri_time_flag, "arrival_time_check", "arricheckbox");
	}
}

function validInt(type) {
	if (type===0) {
		var num=document.getElementById("first_total").value;
		if (num.match(/^\d+$/)){
			int1_flag=true;
		}
		else {
			int1_flag=false;
		}
		setCheckAndCheckbox(int1_flag, "first_total_check", "tickettotalbox");
	}
	else if (type===1) {
		var num=document.getElementById("business_total").value;
		if (num.match(/^\d+$/)){
			int2_flag=true;
		}
		else {
			int2_flag=false;
		}
		setCheckAndCheckbox(int2_flag, "business_total_check", "tickettotalbox");
	}
	else if (type===2) {
		var num=document.getElementById("economic_total").value;
		if (num.match(/^\d+$/)){
			int3_flag=true;
		}
		else {
			int3_flag=false;
		}
		setCheckAndCheckbox(int3_flag, "economic_total_check", "tickettotalbox");
	}
	else if (type===3) {
		var num=document.getElementById("first_full_price").value;
		if (num.match(/^\d+$/)){
			int1_flag=true;
		}
		else {
			int1_flag=false;
		}
		setCheckAndCheckbox(int1_flag, "first_full_price_check", "ticketfullpricebox");
	}
	else if (type===4) {
		var num=document.getElementById("business_full_price").value;
		if (num.match(/^\d+$/)){
			int2_flag=true;
		}
		else {
			int2_flag=false;
		}
		setCheckAndCheckbox(int2_flag, "business_full_price_check", "ticketfullpricebox");
	}
	else if (type===5) {
		var num=document.getElementById("economic_full_price").value;
		if (num.match(/^\d+$/)){
			int3_flag=true;
		}
		else {
			int3_flag=false;
		}
		setCheckAndCheckbox(int3_flag, "economic_full_price_check", "ticketfullpricebox");
	}
}

function validFloat(type) {
	if (type===0) {
		var num=document.getElementById("first_full_price").value;
		if (num.match(/^\d+\.\d+$/)){
			float1_flag=true;
		}
		else {
			float1_flag=false;
		}
		setCheckAndCheckbox(float1_flag, "first_full_price_check", "ticketfullpricebox");
	}
	else if (type===1) {
		var num=document.getElementById("business_full_price").value;
		if (num.match(/^\d+\.\d+$/)){
			float2_flag=true;
		}
		else {
			float2_flag=false;
		}
		setCheckAndCheckbox(float2_flag, "business_full_price_check", "ticketfullpricebox");
	}
	else if (type===2) {
		var num=document.getElementById("economic_full_price").value;
		if (num.match(/^\d+\.\d+$/)){
			float3_flag=true;
		}
		else {
			float3_flag=false;
		}
		setCheckAndCheckbox(float3_flag, "economic_full_price_check", "ticketfullpricebox");
	}
	else if (type===3) {
		var num=document.getElementById("first_price_percent").value;
		if (num.match(/^\d+\.\d+$/)){
			float1_flag=true;
		}
		else {
			float1_flag=false;
		}
		setCheckAndCheckbox(float1_flag, "first_price_percent_check", "pricepercentbox");
	}
	else if (type===4) {
		var num=document.getElementById("business_price_percent").value;
		if (num.match(/^\d+\.\d+$/)){
			float2_flag=true;
		}
		else {
			float2_flag=false;
		}
		setCheckAndCheckbox(float2_flag, "business_price_percent_check", "pricepercentbox");
	}
	else if (type===5) {
		var num=document.getElementById("economic_price_percent").value;
		if (num.match(/^\d+\.\d+$/)){
			float3_flag=true;
		}
		else {
			float3_flag=false;
		}
		setCheckAndCheckbox(float3_flag, "economic_price_percent_check", "pricepercentbox");
	}
}

function reset_shared() {
	$('#flight_search_result').html("");
	$('flight_num').val("");
	$('#origin_airport_selector').val("Select an airport");
	$('#dest_airport_selector').val("Select an airport");
	document.getElementById("status_selector").value="SCH";
	$('#host_airline').val("");
	$('#host_flight_number').val("");
	$('#aircraft_type').val("");
	$('#dept_date_selector').val("");
	$('#arrival_date_selector').val("");
	$('#dept_time_selector').val("");
	$('#arrival_time_selector').val("");
	$('#first_total').val("");
	$('#business_total').val("");
	$('#economic_total').val("");
	$('#first_full_price').val("");
	$('#business_full_price').val("");
	$('#economic_full_price').val("");
	resetflags();
	resetCheckAndCheckbox("dept_time_check", "deptcheckbox");
	resetCheckAndCheckbox("dept_date_check", "deptcheckbox");
	resetCheckAndCheckbox("arrival_time_check", "arricheckbox");
	resetCheckAndCheckbox("arrival_date_check", "arricheckbox");
	resetCheckAndCheckbox("first_total_check", "tickettotalbox");
	resetCheckAndCheckbox("business_total_check", "tickettotalbox");
	resetCheckAndCheckbox("economic_total_check", "tickettotalbox");
	resetCheckAndCheckbox("first_full_price_check", "ticketfullpricebox");
	resetCheckAndCheckbox("business_full_price_check", "ticketfullpricebox");
	resetCheckAndCheckbox("economic_full_price_check", "ticketfullpricebox");
}

function search_shared_flight() {
	reset_shared();
	validateID();
	route_flag=true;
	aircraft_flag=true;
	if (id_flag) {
		var flight_num = document.getElementById("flight_num").value;
		$.get( "/get_shared_flight_by_flight_number",{flight_num:flight_num}, function( data, status ) {
			if (data.coords[0]=="YES") {
				$('#flight_search_result').html("<h style=\"color:green;\">Load Info of flight " + flight_num + "</h>");
				$('#origin_airport_selector').val(data.coords[1]);
				$('#dest_airport_selector').val(data.coords[2]);
				$('#host_airline').val(data.coords[5]);
				$('#host_flight_number').val(data.coords[6]);
				$('#aircraft_type').val(data.coords[7]);
				$('#dept_time_selector').val(data.coords[3]);
				$('#arrival_time_selector').val(data.coords[4]);
				dept_time_flag=true;
				arri_time_flag=true;
			}
			else {
				$('#flight_search_result').html("<h style=\"color:red;\">Flight " + flight_num + " does not exist yet.</h>");
			}
		});
	}
	else {
		$('#flight_search_result').html("<h style=\"color:red;\">Flight number illegal.</h>");
	}	
}

function set_flight_share() {
	var flight_num = document.getElementById("flight_num").value;
	var dept_airport=document.getElementById("origin_airport_selector").value;
	var arri_airport=document.getElementById("dest_airport_selector").value;
	var host_airline=document.getElementById("host_airline").value;
	var host_flight_num=document.getElementById("host_flight_number").value;
	var aircraft=document.getElementById("aircraft_type").value;
	var dept_date=document.getElementById("dept_date_selector").value;
	var dept_time=document.getElementById("dept_time_selector").value;
	var arri_date=document.getElementById("arrival_date_selector").value;
	var arri_time=document.getElementById("arrival_time_selector").value;
	var status=document.getElementById("status_selector").value;
	var first_total=document.getElementById("first_total").value;
	var business_total=document.getElementById("business_total").value;
	var economic_total=document.getElementById("economic_total").value;
	var first_full_price=document.getElementById("first_full_price").value;
	var business_full_price=document.getElementById("business_full_price").value;
	var economic_full_price=document.getElementById("economic_full_price").value;
	if (id_flag && dept_date_flag && dept_time_flag && arri_date_flag && arri_time_flag && int1_flag && int2_flag && int3_flag && float1_flag && float2_flag && float3_flag && (dept_airport!="Select an airport") && (arri_airport!="Select an airport")) {
		$.get( "/set_share_flight",{flight_num:flight_num, dept_airport:dept_airport, arri_airport:arri_airport, host_airline:host_airline, host_flight_num:host_flight_num, aircraft:aircraft, dept_date:dept_date, dept_time:dept_time, arri_date:arri_date, arri_time:arri_time, status:status, first_total:first_total, business_total:business_total, economic_total:economic_total, first_full_price:first_full_price, business_full_price:business_full_price, economic_full_price:economic_full_price}, function( data, status ) {
			//console.log(data);
			if (data.coords[0]=="Existed") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Flight Existed.</span>");
				document.getElementById("error").className="alert alert-warning";
				$("#error").show();
				//reset_shared();
			}
			else {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Flight Added.</span>");
				document.getElementById("error").className="alert alert-success";
				$("#error").show();
				reset_shared();
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong Information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}
}

function reset_cc() {
	$('#flight_search_result').html("");
	$('flight_num').val("");
	$('#origin_airport_selector').val("Select an airport");
	$('#dest_airport_selector').val("Select an airport");
	document.getElementById("status_selector").value="SCH";
	$('#dept_date_selector').val("");
	$('#arrival_date_selector').val("");
	$('#dept_time_selector').val("");
	$('#arrival_time_selector').val("");
	$('#first_price_percent').val("");
	$('#business_price_percent').val("");
	$('#economic_price_percent').val("");
	$('#first_full_price').val("");
	$('#business_full_price').val("");
	$('#economic_full_price').val("");
	$('#route_id').val("");
	$('#aircraft_id').val("");
	resetflags();
	resetCheckAndCheckbox("dept_time_check", "deptcheckbox");
	resetCheckAndCheckbox("dept_date_check", "deptcheckbox");
	resetCheckAndCheckbox("arrival_time_check", "arricheckbox");
	resetCheckAndCheckbox("arrival_date_check", "arricheckbox");
	resetCheckAndCheckbox("first_price_percent_check", "pricepercentbox");
	resetCheckAndCheckbox("business_price_percent_check", "pricepercentbox");
	resetCheckAndCheckbox("economic_price_percent_check", "pricepercentbox");
	resetCheckAndCheckbox("first_full_price_check", "ticketfullpricebox");
	resetCheckAndCheckbox("business_full_price_check", "ticketfullpricebox");
	resetCheckAndCheckbox("economic_full_price_check", "ticketfullpricebox");
}

function search_cc_flight() {
	reset_cc();
	validateID();
	if (id_flag) {
		var flight_num = document.getElementById("flight_num").value;
		$.get( "/get_cc_flight_by_flight_number",{flight_num:flight_num}, function( data, status ) {
			if (data.coords[0]=="YES") {
				$('#flight_search_result').html("<h style=\"color:green;\">Load Info of flight " + flight_num + "</h>");
				$('#origin_airport_selector').val(data.coords[1]);
				$('#dest_airport_selector').val(data.coords[2]);
				$('#dept_time_selector').val(data.coords[3]);
				$('#arrival_time_selector').val(data.coords[4]);
				dept_time_flag=true;
				arri_time_flag=true;
			}
			else {
				$('#flight_search_result').html("<h style=\"color:red;\">Flight " + flight_num + " does not exist yet.</h>");
			}
		});
	}
	else {
		$('#flight_search_result').html("<h style=\"color:red;\">Flight number illegal.</h>");
	}	
}

function selected_route() {
	var dept_airport=document.getElementById("origin_airport_selector").value;
	var arri_airport=document.getElementById("dest_airport_selector").value;
	route_set=[];
	if ((dept_airport!="Select an airport") && (arri_airport!="Select an airport")) {
		$.get("/get_route",{dept_airport:dept_airport, arri_airport:arri_airport}, function (data,status){
			$("#table").remove();
			$("#tablebox").append("<div style=\"height: 300px; overflow-y:auto;\" id=\"table\">\
									<label for=\"route_table\">Route</label>\
									<table class=\"table table-bordered table-condensed table-striped table-hover\" id=\"route_table\">\
										<thead>\
											<tr><th>Departure Airport</th><th>Arrival Airport</th><th>Scheduled Height</th><th>Duration</th><th>Distance</th><th>Description</th></tr>\
										</thead>\
										<tbody id=\"route_list\">\
										</tbody>\
									</table>\
								</div>");
			//console.log(data);
			for (i=0; i<data.coords.length; i++){
				route_set.push ({route_id: data.coords[i][0], dept_airport:data.coords[i][1], arri_airport:data.coords[i][2], height:data.coords[i][3], duration:data.coords[i][4], distance:data.coords[i][5], description:data.coords[i][6]});
			}
			for (i=0;i<route_set.length;i++) {
				newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='selectAircraft("+i.toString()+")'><td>"+route_set[i].dept_airport+"</td><td>"+route_set[i].arri_airport+"</td><td>"+route_set[i].height+"</td><td>"+route_set[i].duration+"</td><td>"+route_set[i].distance+"</td><td>"+route_set[i].description+"</tr>"
    			$("#route_list").append(newline);
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong Information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}
}

function selectAircraft(index) {
	route_flag=true;
	route_id=route_set[index].route_id;
	document.getElementById("route_id").value=route_id;
	aircraft_set=[];
	$.get("/get_aircraft",{route_id:route_id}, function (data,status){
		$("#table").remove();
		$("#tablebox").append("<div style=\"height: 300px; overflow-y:auto;\" id=\"table\">\
									<label for=\"aircraft_table\">Route</label>\
									<table class=\"table table-bordered table-condensed table-striped table-hover\" id=\"route_table\">\
										<thead>\
											<tr><th>Type</th><th>Buy Year</th><th>First Class</th><th>Business Class</th><th>Economic Class</th><th>Current Location</th><th>Maintainence Status</th><th>Range Typical Payload</th></tr>\
										</thead>\
										<tbody id=\"aircraft_list\">\
										</tbody>\
									</table>\
								</div>");
		//console.log(data);
		for (i=0; i<data.coords.length; i++){
			aircraft_set.push ({aircraft_id: data.coords[i][0], type:data.coords[i][1], buy_year:data.coords[i][2], fc_seats:data.coords[i][3], bc_seats:data.coords[i][4], ec_seats:data.coords[i][5], cur_location:data.coords[i][6], maintainence_status:data.coords[i][7], range_typical_payload:data.coords[i][8]});
			}
		for (i=0;i<aircraft_set.length;i++) {
			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='setAircraft("+i.toString()+")'><td>"+aircraft_set[i].type+"</td><td>"+aircraft_set[i].buy_year+"</td><td>"+aircraft_set[i].fc_seats+"</td><td>"+aircraft_set[i].bc_seats+"</td><td>"+aircraft_set[i].ec_seats+"</td><td>"+aircraft_set[i].cur_location+"</td><td>"+aircraft_set[i].maintainence_status+"</td><td>"+aircraft_set[i].range_typical_payload+"</tr>"
    		$("#aircraft_list").append(newline);
		}
	});
}

function setAircraft(index) {
	aircraft_flag=true;
	aircraft_id=aircraft_set[index].aircraft_id;
	document.getElementById("aircraft_id").value=aircraft_id;
	$("#table").remove();
	document.getElementById("first_total").value=aircraft_set[index].fc_seats;
	document.getElementById("business_total").value=aircraft_set[index].bc_seats;
	document.getElementById("economic_total").value=aircraft_set[index].ec_seats;
}

function set_flight_cc() {
	var flight_num = document.getElementById("flight_num").value;
	var dept_airport=document.getElementById("origin_airport_selector").value;
	var arri_airport=document.getElementById("dest_airport_selector").value;
	var dept_date=document.getElementById("dept_date_selector").value;
	var dept_time=document.getElementById("dept_time_selector").value;
	var arri_date=document.getElementById("arrival_date_selector").value;
	var arri_time=document.getElementById("arrival_time_selector").value;
	var first_total=document.getElementById("first_total").value;
	var business_total=document.getElementById("business_total").value;
	var economic_total=document.getElementById("economic_total").value;
	var first_price_percent=document.getElementById("first_price_percent").value;
	var business_price_percent=document.getElementById("business_price_percent").value;
	var economic_price_percent=document.getElementById("economic_price_percent").value;
	var first_full_price=document.getElementById("first_full_price").value;
	var business_full_price=document.getElementById("business_full_price").value;
	var economic_full_price=document.getElementById("economic_full_price").value;
	var status=document.getElementById("status_selector").value;
	if (id_flag && dept_date_flag && dept_time_flag && arri_date_flag && arri_time_flag && int1_flag && int2_flag && int3_flag && float1_flag && float2_flag && float3_flag && route_flag && aircraft_flag) {
		$.get( "/set_cc_flight",{flight_num:flight_num, dept_airport:dept_airport, arri_airport:arri_airport, route_id:route_id, aircraft_id:aircraft_id, dept_date:dept_date, dept_time:dept_time, arri_date:arri_date, arri_time:arri_time, status:status, first_total:first_total, business_total:business_total, economic_total:economic_total, first_full_price:first_full_price, business_full_price:business_full_price, economic_full_price:economic_full_price, first_price_percent:first_price_percent, business_price_percent:business_price_percent, economic_price_percent:economic_price_percent}, function( data, status ) {
			//console.log(data);
			if (data.coords[0]=="Existed") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Flight Existed.</span>");
				document.getElementById("error").className="alert alert-warning";
				$("#error").show();
				//reset_shared();
			}
			else {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Flight Added.</span>");
				document.getElementById("error").className="alert alert-success";
				$("#error").show();
				reset_cc();
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong Information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}
}


function selected_route_modify() {
	route_flag=false;
	var dept_airport=document.getElementById("origin_airport_selector").value;
	var arri_airport=document.getElementById("dest_airport_selector").value;
	route_set=[];
	if ((dept_airport!="Select an airport") && (arri_airport!="Select an airport")) {
		$.get("/get_route",{dept_airport:dept_airport, arri_airport:arri_airport}, function (data,status){
			$("#table").remove();
			$("#tablebox").append("<div style=\"height: 300px; overflow-y:auto;\" id=\"table\">\
									<label for=\"route_table\">Route</label>\
									<table class=\"table table-bordered table-condensed table-striped table-hover\" id=\"route_table\">\
										<thead>\
											<tr><th>Departure Airport</th><th>Arrival Airport</th><th>Scheduled Height</th><th>Duration</th><th>Distance</th><th>Description</th></tr>\
										</thead>\
										<tbody id=\"route_list\">\
										</tbody>\
									</table>\
								</div>");
			//console.log(data);
			for (i=0; i<data.coords.length; i++){
				route_set.push ({route_id: data.coords[i][0], dept_airport:data.coords[i][1], arri_airport:data.coords[i][2], height:data.coords[i][3], duration:data.coords[i][4], distance:data.coords[i][5], description:data.coords[i][6]});
			}
			for (i=0;i<route_set.length;i++) {
				newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='selectAircraft_modify("+i.toString()+")'><td>"+route_set[i].dept_airport+"</td><td>"+route_set[i].arri_airport+"</td><td>"+route_set[i].height+"</td><td>"+route_set[i].duration+"</td><td>"+route_set[i].distance+"</td><td>"+route_set[i].description+"</tr>"
    			$("#route_list").append(newline);
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong Information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}
}


function selectAircraft_modify(index) {
	route_flag=true;
	aircraft_flag=false;
	route_id=route_set[index].route_id;
	document.getElementById("route_id").value=route_id;
	aircraft_set=[];
	$.get("/get_aircraft_modify",{route_id:route_id, fc_sold_seat:flight_set[modify_index].fc_sold_seats, bc_sold_seat:flight_set[modify_index].bc_sold_seats,ec_sold_seat:flight_set[modify_index].ec_sold_seats}, function (data,status){
		$("#table").remove();
		$("#tablebox").append("<div style=\"height: 300px; overflow-y:auto;\" id=\"table\">\
									<label for=\"aircraft_table\">Route</label>\
									<table class=\"table table-bordered table-condensed table-striped table-hover\" id=\"route_table\">\
										<thead>\
											<tr><th>Type</th><th>Buy Year</th><th>First Class</th><th>Business Class</th><th>Economic Class</th><th>Current Location</th><th>Maintainence Status</th><th>Range Typical Payload</th></tr>\
										</thead>\
										<tbody id=\"aircraft_list\">\
										</tbody>\
									</table>\
								</div>");
		//console.log(data);
		for (i=0; i<data.coords.length; i++){
			aircraft_set.push ({aircraft_id: data.coords[i][0], type:data.coords[i][1], buy_year:data.coords[i][2], fc_seats:data.coords[i][3], bc_seats:data.coords[i][4], ec_seats:data.coords[i][5], cur_location:data.coords[i][6], maintainence_status:data.coords[i][7], range_typical_payload:data.coords[i][8]});
			}
		for (i=0;i<aircraft_set.length;i++) {
			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='setAircraft("+i.toString()+")'><td>"+aircraft_set[i].type+"</td><td>"+aircraft_set[i].buy_year+"</td><td>"+aircraft_set[i].fc_seats+"</td><td>"+aircraft_set[i].bc_seats+"</td><td>"+aircraft_set[i].ec_seats+"</td><td>"+aircraft_set[i].cur_location+"</td><td>"+aircraft_set[i].maintainence_status+"</td><td>"+aircraft_set[i].range_typical_payload+"</tr>"
    		$("#aircraft_list").append(newline);
		}
	});
}

function modify_flight_cc() {
	var sched_dept_t=document.getElementById("dept_date_selector").value;
	var act_dept_t=document.getElementById("dept_time_selector").value;
	var sched_arr_t=document.getElementById("arrival_date_selector").value;
	var act_arr_t=document.getElementById("arrival_time_selector").value;
	var first_total=document.getElementById("first_total").value;
	var business_total=document.getElementById("business_total").value;
	var economic_total=document.getElementById("economic_total").value;
	var first_price_percent=document.getElementById("first_price_percent").value;
	var business_price_percent=document.getElementById("business_price_percent").value;
	var economic_price_percent=document.getElementById("economic_price_percent").value;
	var first_full_price=document.getElementById("first_full_price").value;
	var business_full_price=document.getElementById("business_full_price").value;
	var economic_full_price=document.getElementById("economic_full_price").value;
	var status=document.getElementById("status_selector").value;
	if (id_flag && dept_date_flag && dept_time_flag && arri_date_flag && arri_time_flag && int1_flag && int2_flag && int3_flag && float1_flag && float2_flag && float3_flag && route_flag && aircraft_flag) {
		$.get("/modify_flight_cc",{flight_id:flight_set[modify_index].flight_id, sched_dept_t:sched_dept_t, act_dept_t:act_dept_t, route_id:route_id, aircraft_id:aircraft_id, sched_arr_t:sched_arr_t, act_arr_t:act_arr_t, status:status, first_total:first_total, business_total:business_total, economic_total:economic_total, first_full_price:first_full_price, business_full_price:business_full_price, economic_full_price:economic_full_price, first_price_percent:first_price_percent, business_price_percent:business_price_percent, economic_price_percent:economic_price_percent, fc_sold_seats: flight_set[modify_index].fc_sold_seats, bc_sold_seats: flight_set[modify_index].bc_sold_seats, ec_sold_seats: flight_set[modify_index].ec_sold_seats}, function (data,status){
			if (data.coords[0][0]!="Success") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Fail to Modify</span>");
				document.getElementById("error").className="alert alert-warning";
				$("#error").show();
			}
			else {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Flight Modified.</span>");
				document.getElementById("error").className="alert alert-success";
				$("#error").show();
				modify_index=-1;
				refreshtable_cc();
				reset_cc();
				resetflags();
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong Information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}
}

function modify_flight_share() {
	var host_airline=document.getElementById("host_airline").value;
	var host_flight_num=document.getElementById("host_flight_number").value;
	var aircraft=document.getElementById("aircraft_type").value;
	var sched_dept_t=document.getElementById("dept_date_selector").value;
	var act_dept_t=document.getElementById("dept_time_selector").value;
	var sched_arr_t=document.getElementById("arrival_date_selector").value;
	var act_arr_t=document.getElementById("arrival_time_selector").value;
	var status=document.getElementById("status_selector").value;
	var first_total=document.getElementById("first_total").value;
	var business_total=document.getElementById("business_total").value;
	var economic_total=document.getElementById("economic_total").value;
	var first_full_price=document.getElementById("first_full_price").value;
	var business_full_price=document.getElementById("business_full_price").value;
	var economic_full_price=document.getElementById("economic_full_price").value;
	if (id_flag && dept_date_flag && dept_time_flag && arri_date_flag && arri_time_flag && int1_flag && int2_flag && int3_flag && float1_flag && float2_flag && float3_flag) {
		$.get( "/modify_share_flight",{flight_id:flight_set[modify_index].flight_id, host_airline:host_airline, host_flight_num:host_flight_num, aircraft:aircraft, sched_dept_t:sched_dept_t, act_dept_t:act_dept_t, sched_arr_t:sched_arr_t, act_arr_t:act_arr_t, status:status, first_total:first_total, business_total:business_total, economic_total:economic_total, first_full_price:first_full_price, business_full_price:business_full_price, economic_full_price:economic_full_price}, function( data, status ) {
			//console.log(data);
			if (data.coords[0][0]!="Success") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Fail to Modify</span>");
				document.getElementById("error").className="alert alert-warning";
				$("#error").show();
			}
			else {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Success!</strong> Flight Modified.</span>");
				document.getElementById("error").className="alert alert-success";
				$("#error").show();
				modify_index=-1;
				refreshtable_share();
				reset_shared();
				resetflags();
			}
		});
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> Wrong Information.</span>");
		document.getElementById("error").className="alert alert-warning";
		$("#error").show();
	}
}
