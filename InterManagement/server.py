#!/usr/bin/env python2.7

"""
Columbia W4111 Intro to databases
Example webserver

To run locally

    python server.py

Go to http://localhost:8111 in your browser


A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""

import os
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from sqlalchemy.exc import DBAPIError
from flask import Flask, render_template, request, url_for, jsonify, Response, redirect, flash, g
import datetime
import json
import random
import string
from json import dumps

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)


#
# The following uses the postgresql test.db -- you can use this for debugging purposes
# However for the project you will need to connect to your Part 2 database in order to use the
# data
#
# XXX: The URI should be in the format of: 
#
#     postgresql://USER:PASSWORD@<IP_OF_POSTGRE_SQL_SERVER>/postgres
#
# For example, if you had username ewu2493, password foobar, then the following line would be:
#
#     DATABASEURI = "postgresql://ewu2493:foobar@<IP_OF_POSTGRE_SQL_SERVER>/postgres"
#
# Swap out the URI below with the URI for the database created in part 2
DATABASEURI = "postgresql://zl2471:d462v@104.196.175.120/postgres"


#
# This line creates a database engine that knows how to connect to the URI above
#
engine = create_engine(DATABASEURI)

@app.before_request
def before_request():
  	"""
  	This function is run at the beginning of every web request 
  	(every time you enter an address in the web browser).
  	We use it to setup a database connection that can be used throughout the request

  	The variable g is globally accessible
  	"""
	try:
		g.conn = engine.connect()
	except:
		print "uh oh, problem connecting to database"
		import traceback; traceback.print_exc()
		g.conn = None

@app.after_request
def add_header(response):
	#set the max_age of webpage to 0 to force browser check page to server
	response.cache_control.max_age=0
	return response

@app.teardown_request
def teardown_request(exception):
	"""
	At the end of the web request, this makes sure to close the database connection.
	If you don't the database could run out of memory!
	"""
	try:
		g.conn.close()
	except Exception as e:
		pass

@app.route('/',methods=["GET","POST"])
def getindex():
  # DEBUG: this is debugging code to see what request looks like
	return render_template("index.html")

@app.route("/airport.html")
def getairport():
	return render_template("airport.html")

@app.route("/aircraft.html")
def getaircraft():
	return render_template("aircraft.html")

@app.route("/flight.html")
def getflight():
	return render_template("flight.html")

@app.route("/route.html")
def getroute():
	return render_template("route.html")

@app.route("/check.html")
def ticketcheck():
	return render_template("check.html")

#Add airport function
#Input: none
#Output: iata_code, city from 'Airport' table
@app.route("/add_airport")
def addairport():
	args = request.args
	iatacode = args.get('iatacode')
	city = args.get('city')
	state = args.get('state')
	name = args.get('name')
	time_zone = args.get('time_zone')
	type_air = args.get('type')
	icao = "K"+iatacode
	cmd = "SELECT * FROM Airport WHERE iata_code= :iata_code;"
	try:
		cur = g.conn.execute(text(cmd), iata_code=iatacode)
	except DBAPIError,e:
		print e
	data = cur.fetchall()
	if (len(data)!=0):
		data=[('Existed',)]
		return jsonify(coords = data)
	cmd = "INSERT INTO Airport (iata_code, icao, city, state, timezone, name, airport_cat) VALUES (:iata_code, :icao, :city, :state, :timezone, :name, :airport_cat);"
	try:
		cur = g.conn.execute(text(cmd), iata_code=iatacode, icao=icao, city=city, state=state, timezone=time_zone, name=name, airport_cat=type_air)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), iata_code=iatacode, icao=icao, city=city, state=state, timezone=time_zone, name=name, airport_cat=type_air)
	data=[('Success',)]
	return jsonify(coords = data)

#Modify airport function
#Input: none
#Output: iata_code, city from 'Airport' table
@app.route("/modify_airport")
def modifyairport():
	#print "mod"
	args = request.args
	iatacode = args.get('iatacode')
	city = args.get('city')
	state = args.get('state')
	name = args.get('name')
	time_zone = args.get('time_zone')
	type_air = args.get('type')
	old_iata = args.get('old_iata')
	icao = "K"+iatacode
	cmd = "UPDATE Airport SET iata_code= :iata_code, icao= :icao, city= :city, state= :state, timezone= :timezone, name= :name, airport_cat= :airport_cat WHERE iata_code= :old_iata;"
	try:
		cur = g.conn.execute(text(cmd), iata_code=iatacode, icao=icao, city=city, state=state, timezone=time_zone, name=name, airport_cat=type_air, old_iata=old_iata)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			try:
				cur = g.conn.execute(text(cmd), iata_code=iatacode, icao=icao, city=city, state=state, timezone=time_zone, name=name, airport_cat=type_air, old_iata=old_iata)
			except DBAPIError,e:
				print e
				data=[('Wrong',)]
				return jsonify(coords = data)
		else:
			data=[('Wrong',)]
			#print data
			return jsonify(coords = data)
	data=[('Success',)]
	#print data
	return jsonify(coords = data)

#load existing airport function
#Input: none
#Output: iata_code, city from 'Airport' table
@app.route("/load_airport")
def loadairport():
	cmd = "SELECT iata_code, city, state, timezone, name, airport_cat FROM Airport ORDER BY iata_code;"
	try:
		cur = g.conn.execute(text(cmd))
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd))
	data = cur.fetchall()
	for i in range(0,len(data)):
		data[i]=list(data[i])
		for j in range(0,len(data[i])):
			data[i][j]=str(data[i][j])
			data[i][j]=str(data[i][j])
		data[i]=tuple(data[i])
	return jsonify(coords = data)

@app.route("/add_aircraft")
def addaircraft():
	args = request.args
	aircraft_id=args.get('aircraft_id');
	buy_year = args.get('buy_year');
	fc_seats = args.get('fc_seats');
	bc_seats = args.get('bc_seats');
	ec_seats = args.get('ec_seats');
	aircraft_type = args.get('type');
	cur_location = args.get('cur_location');
	maintainence_status = args.get('maintainence_status');
	range_typical_payload = args.get('range_typical_payload');
	cmd = "SELECT * FROM Aircraft WHERE aircraft_id= :aircraft_id;"
	print "parse complete, send query."
	try:
		cur = g.conn.execute(text(cmd), aircraft_id=aircraft_id)
	except DBAPIError,e:
		print e
	data = cur.fetchall()
	if (len(data)!=0):
		data=[('Existed',)]
		return jsonify(coords = data)
	cmd = """
	INSERT INTO Aircraft
	(aircraft_id, type, buy_year, fc_seats, bc_seats, ec_seats, cur_location, maintainence_status, range_typical_payload)
	VALUES (:aircraft_id, :type, :buy_year, :fc_seats, :bc_seats, :ec_seats, :cur_location, :maintainence_status, :range_typical_payload);"""
	try:
		cur = g.conn.execute(text(cmd), aircraft_id=aircraft_id, type=aircraft_type, buy_year=buy_year, fc_seats=fc_seats, bc_seats=bc_seats, 
			ec_seats=ec_seats, cur_location=cur_location, maintainence_status=maintainence_status, range_typical_payload=range_typical_payload)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), aircraft_id=aircraft_id, type=aircraft_type, buy_year=buy_year, fc_seats=fc_seats, bc_seats=bc_seats, 
			ec_seats=ec_seats, cur_location=cur_location, maintainence_status=maintainence_status, range_typical_payload=range_typical_payload)
	data=[('Success',)]
	return jsonify(coords = data)

@app.route("/load_aircraft")
def loadaircraft():
	cmd = "SELECT aircraft_id, type, buy_year, fc_seats, bc_seats, ec_seats, cur_location, maintainence_status, range_typical_payload FROM Aircraft ORDER BY aircraft_id ASC;"
	try:
		cur = g.conn.execute(text(cmd))
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd))
	data = cur.fetchall()
	for i in range(0,len(data)):
		data[i]=list(data[i])
		for j in range(0,len(data[i])):
			data[i][j]=str(data[i][j])
			data[i][j]=str(data[i][j])
		data[i]=tuple(data[i])
	return jsonify(coords = data)

@app.route("/modify_aircraft")
def modifyaircraft():
	args = request.args
	#print args
	aircraft_id=args.get('aircraft_id');
	buy_year = args.get('buy_year');
	fc_seats = args.get('fc_seats');
	bc_seats = args.get('bc_seats');
	ec_seats = args.get('ec_seats');
	aircraft_type = args.get('type');
	cur_location = args.get('cur_location');
	maintainence_status = args.get('maintainence_status');
	range_typical_payload = args.get('range_typical_payload');
	cmd = """
	UPDATE Aircraft SET 
	aircraft_id= :aircraft_id, type= :type, buy_year= :buy_year, fc_seats= :fc_seats, bc_seats= :bc_seats, ec_seats= :ec_seats, 
	cur_location= :cur_location, maintainence_status= :maintainence_status, range_typical_payload= :range_typical_payload
	WHERE aircraft_id = :aircraft_id"""
	try:
		cur = g.conn.execute(text(cmd), aircraft_id=aircraft_id, type=aircraft_type, buy_year=buy_year, fc_seats=fc_seats, bc_seats=bc_seats, 
			ec_seats=ec_seats, cur_location=cur_location, maintainence_status=maintainence_status, range_typical_payload=range_typical_payload)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			try:
				cur = g.conn.execute(text(cmd), aircraft_id=aircraft_id, type=aircraft_type, buy_year=buy_year, fc_seats=fc_seats, bc_seats=bc_seats, ec_seats=ec_seats, cur_location=cur_location, maintainence_status=maintainence_status, range_typical_payload=range_typical_payload)
			except DBAPIError,e:
				print e
				data=[('Wrong',)]
				return jsonify(coords = data)
		else:
			data=[('Wrong',)]
			#print data
			return jsonify(coords = data)
	data=[('Success',)]
	#print data
	return jsonify(coords = data)
	return jsonify(coords = data)

#Add route function
#Input: none
#Output: iata_code, city from 'Airport' table
@app.route("/add_route")
def addroute():
	args = request.args
	source_airport = args.get('source_airport')
	dest_airport = args.get('dest_airport')
	height = args.get('height')
	duration = args.get('duration')
	distance = args.get('distance')
	description = args.get('description')
	cmd = "SELECT max(route_id) FROM Route;"
	try:
		cur = g.conn.execute(text(cmd))
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd))
	data = cur.fetchall()
	route_id=data[0][0]+1
	cmd = "INSERT INTO Route (route_id, dept_airport, arr_airport, scheduled_height, duraion, distance, description) VALUES (:route_id, :source_airport, :dest_airport, :height, :duration, :distance, :description);"
	try:
		cur = g.conn.execute(text(cmd), route_id=route_id, source_airport=source_airport, dest_airport=dest_airport, height=height, duration=duration, distance=distance, description=description)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), route_id=route_id, source_airport=source_airport, dest_airport=dest_airport, height=height, duration=duration, distance=distance, description=description)
	data=[('Success',)]
	return jsonify(coords = data)

#Add route function
#Input: none
#Output: iata_code, city from 'Airport' table
@app.route("/load_route")
def loadroute():
	cmd = "SELECT * FROM Route ORDER BY route_id;"
	try:
		cur = g.conn.execute(text(cmd))
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd))
	data = cur.fetchall()
	for i in range(0,len(data)):
		data[i]=list(data[i])
		for j in range(0,len(data[i])):
			data[i][j]=str(data[i][j])
		data[i]=tuple(data[i])
	return jsonify(coords = data)

#Add route function
#Input: none
#Output: iata_code, city from 'Airport' table
@app.route("/mod_route")
def modroute():
	args = request.args
	route_id = args.get('route_id')
	source_airport = args.get('source_airport')
	dest_airport = args.get('dest_airport')
	height = args.get('height')
	duration = args.get('duration')
	distance = args.get('distance')
	description = args.get('description')
	cmd = "UPDATE Route SET dept_airport= :source_airport, arr_airport= :dest_airport, scheduled_height= :height, duraion= :duration, distance= :distance, description= :description WHERE route_id= :route_id;"
	try:
		cur = g.conn.execute(text(cmd), route_id=route_id, source_airport=source_airport, dest_airport=dest_airport, height=height, duration=duration, distance=distance, description=description)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), route_id=route_id, source_airport=source_airport, dest_airport=dest_airport, height=height, duration=duration, distance=distance, description=description)
	data=[('Success',)]
	return jsonify(coords = data)

@app.route("/get_shared_flight_by_flight_number")
def getSharedFlightInfo():
	args = request.args
	flight_number = args.get('flight_num')
	cmd = "SELECT flight_id, dept_airport, arr_airport, sched_dept_t, sched_arr_t, host_airline, host_flight_no, aircraft_type FROM Flight F NATURAL JOIN Flight_share FS WHERE flight_no = :flight_num ORDER BY flight_id DESC LIMIT 1;"
	try:
		cur = g.conn.execute(text(cmd), flight_num=flight_number)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_num=flight_number)
	data = cur.fetchone()
	
	if data == None:
		result = ["NO"]
	else:
		result = list(data)
		result[0] = "YES"
		result[3] = str(result[3]).split(' ')[1]
		result[4] = str(result[4]).split(' ')[1]
		result[6] = str(result[6])
	return jsonify(coords = result)

@app.route("/set_share_flight")
def setSharedFlightInfo():
	args = request.args
	flight_number = args.get('flight_num')
	dept_airport = args.get('dept_airport')
	arri_airport = args.get('arri_airport')
	host_airline = args.get('host_airline')
	host_flight_num = args.get('host_flight_num')
	aircraft = args.get('aircraft')
	dept_date = args.get('dept_date')+' '+args.get('dept_time')
	arri_date = args.get('arri_date')+' '+args.get('arri_time')
	status = args.get('status')
	first_total = args.get('first_total')
	business_total = args.get('business_total')
	economic_total = args.get('economic_total')
	first_full_price = args.get('first_full_price')
	business_full_price = args.get('business_full_price')
	economic_full_price = args.get('economic_full_price')
	flight_id='S'+dept_date.split(' ')[0].split('-')[0]+dept_date.split(' ')[0].split('-')[1]+dept_date.split(' ')[0].split('-')[2]+flight_number+dept_airport+arri_airport
	#print flight_id
	flight_date=dept_date.split(' ')[0]
	#print flight_date
	cmd = "SELECT * FROM Flight WHERE flight_id= :flight_id;"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id)
	data=cur.fetchall()
	#print data
	if (data!=[]):
		result = ["Existed"]
		return jsonify(coords = result)
	else:
		cmd = "INSERT INTO Flight (flight_id, flight_no, flight_date, flight_status, sched_dept_t, sched_arr_t) VALUES (:flight_id, :flight_num, :flight_date, :status, :dept_date, :arri_date);"
		try:
			cur = g.conn.execute(text(cmd), flight_id=flight_id, flight_num=flight_number, flight_date=flight_date, status=status, dept_date=dept_date, arri_date=arri_date)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flight_id=flight_id, flight_num=flight_number, flight_date=flight_date, status=status, dept_date=dept_date, arri_date=arri_date)
		cmd = "INSERT INTO Flight_share (flight_id,host_airline,host_flight_no,	dept_airport,arr_airport,aircraft_type) VALUES (:flight_id, :host_airline, :host_flight_num, :dept_airport, :arri_airport, :aircraft);"
		try:
			cur = g.conn.execute(text(cmd), flight_id=flight_id, host_airline=host_airline, host_flight_num=host_flight_num, dept_airport=dept_airport, arri_airport=arri_airport, aircraft=aircraft)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flight_id=flight_id, host_airline=host_airline, host_flight_num=host_flight_num, dept_airport=dept_airport, arri_airport=arri_airport, aircraft=aircraft)
		cmd = "INSERT INTO Flight_ticket_manage_share (flight_id, class, price, remain_seats) VALUES (:flight_id, 'F', :first_full_price, :first_total), (:flight_id, 'B', :business_full_price, :business_total), (:flight_id, 'E', :economic_full_price, :economic_total);"
		try:
			cur = g.conn.execute(text(cmd), flight_id=flight_id, first_full_price=first_full_price, first_total=first_total, business_full_price=business_full_price, business_total=business_total, economic_full_price=economic_full_price, economic_total=economic_total)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flight_id=flight_id, first_full_price=first_full_price, first_total=first_total, business_full_price=business_full_price, business_total=business_total, economic_full_price=economic_full_price, economic_total=economic_total)
		result = ["Success"]
		return jsonify(coords = result)

@app.route("/get_cc_flight_by_flight_number")
def getCCFlightInfo():
	args = request.args
	flight_number = args.get('flight_num')
	cmd = "SELECT flight_id, dept_airport, arr_airport, sched_dept_t, sched_arr_t FROM Flight F NATURAL JOIN Flight_CC NATURAL JOIN Route WHERE flight_no = :flight_num ORDER BY flight_id DESC LIMIT 1;"
	try:
		cur = g.conn.execute(text(cmd), flight_num=flight_number)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_num=flight_number)
	data = cur.fetchone()
	#print data
	
	if data == None:
		result = ["NO"]
	else:
		result = list(data)
		result[0] = "YES"
		result[3] = str(result[3]).split(' ')[1]
		result[4] = str(result[4]).split(' ')[1]
	return jsonify(coords = result)

@app.route("/get_route")
def getRouteInfo():
	args = request.args
	dept_airport = args.get('dept_airport')
	arri_airport = args.get('arri_airport')
	cmd = "SELECT * FROM Route WHERE dept_airport= :dept_airport AND arr_airport= :arr_airport ORDER BY route_id;"
	try:
		cur = g.conn.execute(text(cmd), dept_airport=dept_airport, arr_airport=arri_airport)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), dept_airport=dept_airport, arr_airport=arri_airport)
	data = cur.fetchall()
	#print data
	
	for i in range(0,len(data)):
		data[i]=list(data[i])
		data[i][1]=str(data[i][1])
		data[i][2]=str(data[i][2])
		data[i][6]=str(data[i][6])
		data[i]=tuple(data[i])
	return jsonify(coords = data)

@app.route("/get_aircraft")
def getAircraftInfo():
	args = request.args
	route_id = int(args.get('route_id'))
	cmd = "SELECT * FROM Aircraft\
			WHERE maintainence_status='GFF'\
			AND range_typical_payload>=(SELECT distance\
										FROM Route\
										WHERE route_id= :route_id)\
			ORDER BY aircraft_id;"
	try:
		cur = g.conn.execute(text(cmd), route_id=route_id)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), route_id=route_id)
	data = cur.fetchall()
	#print data
	
	for i in range(0,len(data)):
		data[i]=list(data[i])
		data[i][0]=str(data[i][0])
		data[i][1]=str(data[i][1])
		data[i][5]=str(data[i][5])
		data[i][6]=str(data[i][6])
		data[i]=tuple(data[i])
	return jsonify(coords = data)

@app.route("/set_cc_flight")
def setCCFlightInfo():
	args = request.args
	flight_number = int(args.get('flight_num'))
	dept_airport = args.get('dept_airport')
	arri_airport = args.get('arri_airport')
	aircraft_id = args.get('aircraft_id')
	route_id = args.get('route_id')
	dept_date = args.get('dept_date')+' '+args.get('dept_time')
	arri_date = args.get('arri_date')+' '+args.get('arri_time')
	status = args.get('status')
	first_total = args.get('first_total')
	business_total = args.get('business_total')
	economic_total = args.get('economic_total')
	first_full_price = args.get('first_full_price')
	business_full_price = args.get('business_full_price')
	economic_full_price = args.get('economic_full_price')
	first_price_percent = args.get('first_price_percent')
	business_price_percent = args.get('business_price_percent')
	economic_price_percent = args.get('economic_price_percent')
	#print flight_number
	flight_num_str='0000'
	if (flight_number<10):
		flight_num_str='000'+str(flight_number)
	elif (flight_number<100):
		flight_num_str='00'+str(flight_number)
	elif (flight_number<1000):
		flight_num_str='0'+str(flight_number)
	flight_id='R'+dept_date.split(' ')[0].split('-')[0]+dept_date.split(' ')[0].split('-')[1]+dept_date.split(' ')[0].split('-')[2]+flight_num_str+dept_airport+arri_airport
	#print flight_id
	flight_date=dept_date.split(' ')[0]
	#print flight_date
	cmd = "SELECT * FROM Flight WHERE flight_id= :flight_id;"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id)
	data=cur.fetchall()
	#print data
	if (data!=[]):
		result = ["Existed"]
		return jsonify(coords = result)
	else:
		cmd = "INSERT INTO Flight (flight_id, flight_no, flight_date, flight_status, sched_dept_t, sched_arr_t) VALUES (:flight_id, :flight_num, :flight_date, :status, :dept_date, :arri_date);"
		try:
			cur = g.conn.execute(text(cmd), flight_id=flight_id, flight_num=flight_number, flight_date=flight_date, status=status, dept_date=dept_date, arri_date=arri_date)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flight_id=flight_id, flight_num=flight_number, flight_date=flight_date, status=status, dept_date=dept_date, arri_date=arri_date)
		cmd = "INSERT INTO Flight_CC (flight_id,route_id,aircraft_id) VALUES (:flight_id, :route_id, :aircraft_id);"
		try:
			cur = g.conn.execute(text(cmd), flight_id=flight_id, route_id=route_id, aircraft_id=aircraft_id)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flight_id=flight_id, route_id=route_id, aircraft_id=aircraft_id)
		cmd = "INSERT INTO Flight_ticket_manage_cc (flight_id,class,full_price,remain_seats,sold_seats,price_rate) VALUES (:flight_id, 'F', :first_full_price, :first_total, 0, :first_price_percent), (:flight_id, 'B', :business_full_price, :business_total, 0, :business_price_percent), (:flight_id, 'E', :economic_full_price, :economic_total, 0, :economic_price_percent);"
		try:
			cur = g.conn.execute(text(cmd), flight_id=flight_id, first_full_price=first_full_price, first_total=first_total, business_full_price=business_full_price, business_total=business_total, economic_full_price=economic_full_price, economic_total=economic_total, first_price_percent=first_price_percent, business_price_percent =business_price_percent, economic_price_percent =economic_price_percent)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flight_id=flight_id, first_full_price=first_full_price, first_total=first_total, business_full_price=business_full_price, business_total=business_total, economic_full_price=economic_full_price, economic_total=economic_total, first_price_percent=first_price_percent, business_price_percent =business_price_percent, economic_price_percent =economic_price_percent)
		result = ["Success"]
		return jsonify(coords = result)

@app.route("/load_flight_cc")
def loadFlightCC():
	args = request.args
	current = args.get('current')
	cmd = "SELECT flight_id, flight_no, flight_date, flight_status, sched_dept_t, act_dept_t, sched_arr_t, act_arr_t, route_id, dept_airport, arr_airport, aircraft_id, fc_seats, bc_seats, ec_seats\
			FROM Flight NATURAL JOIN Flight_CC NATURAL JOIN Route NATURAL JOIN Aircraft\
			WHERE flight_date>= :current\
			ORDER BY flight_date, flight_id;"
	try:
		cur = g.conn.execute(text(cmd), current=current)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), current=current)
	data=cur.fetchall()
	result=[]
	for row in data:
		temp=list(row)
		temp[0]=str(temp[0])
		#print temp[0]
		temp[2]=temp[2].isoformat()
		temp[3]=str(temp[3])
		temp[9]=str(temp[9])
		temp[10]=str(temp[10])
		temp[4]=temp[4].strftime("%Y-%m-%d %H:%M:%S")
		if (temp[5] is not None):
			temp[5]=temp[5].strftime("%Y-%m-%d %H:%M:%S")
		temp[6]=temp[6].strftime("%Y-%m-%d %H:%M:%S")
		if (temp[7] is not None):
			temp[7]=temp[7].strftime("%Y-%m-%d %H:%M:%S")
		temp[11]=str(temp[11])
		cmd = "SELECT full_price,sold_seats,price_rate\
				FROM Flight_ticket_manage_cc\
				WHERE flight_id= :flight_id\
				ORDER BY class;"
		try:
			cur = g.conn.execute(text(cmd), flight_id=temp[0])
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flight_id=temp[0])
		temp2=cur.fetchall()
		#print temp2
		temp.append(temp2[2][0])
		temp.append(temp2[2][1])
		temp.append(temp2[2][2])
		temp.append(temp2[0][0])
		temp.append(temp2[0][1])
		temp.append(temp2[0][2])
		temp.append(temp2[1][0])
		temp.append(temp2[1][1])
		temp.append(temp2[1][2])
		#print temp
		result.append(temp)
	return jsonify(coords = result)

@app.route("/get_aircraft_modify")
def getAircraftInfoModify():
	args = request.args
	route_id = int(args.get('route_id'))
	fc_sold_seat = int(args.get('fc_sold_seat'))
	bc_sold_seat = int(args.get('bc_sold_seat'))
	ec_sold_seat = int(args.get('ec_sold_seat'))
	cmd = "SELECT * FROM Aircraft\
			WHERE maintainence_status='GFF'\
			AND fc_seats>= :fc_sold_seat\
			AND bc_seats>= :bc_sold_seat\
			AND ec_seats>= :ec_sold_seat\
			AND range_typical_payload>=(SELECT distance\
										FROM Route\
										WHERE route_id= :route_id)\
			ORDER BY aircraft_id;"
	try:
		cur = g.conn.execute(text(cmd), route_id=route_id, fc_sold_seat=fc_sold_seat, bc_sold_seat=bc_sold_seat, ec_sold_seat=ec_sold_seat)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), route_id=route_id, fc_sold_seat=fc_sold_seat, bc_sold_seat=bc_sold_seat, ec_sold_seat=ec_sold_seat)
	data = cur.fetchall()
	#print data
	
	for i in range(0,len(data)):
		data[i]=list(data[i])
		data[i][0]=str(data[i][0])
		data[i][1]=str(data[i][1])
		data[i][5]=str(data[i][5])
		data[i][6]=str(data[i][6])
		data[i]=tuple(data[i])
	return jsonify(coords = data)

@app.route("/modify_flight_cc")
def modifyCCFlightInfo():
	args = request.args
	flight_id = args.get('flight_id')
	sched_dept_t = args.get('sched_dept_t')
	act_dept_t = args.get('act_dept_t')
	sched_arr_t = args.get('sched_arr_t')
	act_arr_t = args.get('act_arr_t')
	aircraft_id = args.get('aircraft_id')
	route_id = args.get('route_id')
	status = args.get('status')
	first_total = args.get('first_total')
	business_total = args.get('business_total')
	economic_total = args.get('economic_total')
	first_full_price = args.get('first_full_price')
	business_full_price = args.get('business_full_price')
	economic_full_price = args.get('economic_full_price')
	first_price_percent = args.get('first_price_percent')
	business_price_percent = args.get('business_price_percent')
	economic_price_percent = args.get('economic_price_percent')
	fc_sold_seats = args.get('fc_sold_seats')
	bc_sold_seats = args.get('bc_sold_seats')
	ec_sold_seats = args.get('ec_sold_seats')
	fc_remain_seats = int(first_total)-int(fc_sold_seats)
	bc_remain_seats = int(business_total)-int(bc_sold_seats)
	ec_remain_seats = int(economic_total)-int(ec_sold_seats)
	if len(act_dept_t)==0:
		act_dept_t=None
	if len(act_arr_t)==0:
		act_arr_t=None

	cmd = "UPDATE Flight SET sched_dept_t = :sched_dept_t, act_dept_t = :act_dept_t, sched_arr_t = :sched_arr_t, act_arr_t = :act_arr_t, flight_status= :flight_status WHERE flight_id= :flight_id;"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, sched_dept_t=sched_dept_t, act_dept_t=act_dept_t, sched_arr_t=sched_arr_t, act_arr_t=act_arr_t, flight_status=status)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, sched_dept_t=sched_dept_t, act_dept_t=act_dept_t, sched_arr_t=sched_arr_t, act_arr_t=act_arr_t, flight_status=status)

	cmd = "UPDATE Flight_CC SET route_id= :route_id ,aircraft_id= :aircraft_id WHERE  flight_id= :flight_id;"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, route_id=route_id ,aircraft_id=aircraft_id)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, route_id=route_id ,aircraft_id=aircraft_id)

	cmd = "UPDATE Flight_ticket_manage_cc SET full_price= :full_price,remain_seats= :remain_seats,sold_seats= :sold_seats,price_rate= :price_rate WHERE  flight_id= :flight_id AND class='F';"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, full_price=first_full_price, remain_seats=fc_remain_seats, sold_seats=fc_sold_seats, price_rate=first_price_percent)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, full_price=first_full_price, remain_seats=fc_remain_seats, sold_seats=fc_sold_seats, price_rate=first_price_percent)

	cmd = "UPDATE Flight_ticket_manage_cc SET full_price= :full_price,remain_seats= :remain_seats,sold_seats= :sold_seats,price_rate= :price_rate WHERE  flight_id= :flight_id AND class='B';"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, full_price=business_full_price, remain_seats=bc_remain_seats, sold_seats=bc_sold_seats, price_rate=business_price_percent)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, full_price=business_full_price, remain_seats=bc_remain_seats, sold_seats=bc_sold_seats, price_rate=business_price_percent)

	cmd = "UPDATE Flight_ticket_manage_cc SET full_price= :full_price,remain_seats= :remain_seats,sold_seats= :sold_seats,price_rate= :price_rate WHERE  flight_id= :flight_id AND class='E';"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, full_price=economic_full_price, remain_seats=ec_remain_seats, sold_seats=ec_sold_seats, price_rate=economic_price_percent)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, full_price=economic_full_price, remain_seats=ec_remain_seats, sold_seats=ec_sold_seats, price_rate=economic_price_percent)

	result=[('Success',)]
	return jsonify(coords = result)

@app.route("/load_flight_share")
def loadFlightShare():
	args = request.args
	current = args.get('current')
	cmd = "SELECT flight_id, flight_no, flight_date, flight_status, sched_dept_t, act_dept_t, sched_arr_t, act_arr_t, host_airline, host_flight_no, dept_airport, arr_airport, aircraft_type\
			FROM Flight NATURAL JOIN Flight_share\
			WHERE flight_date>= :current\
			ORDER BY flight_date, flight_id;"
	try:
		cur = g.conn.execute(text(cmd), current=current)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), current=current)
	data=cur.fetchall()
	result=[]
	for row in data:
		temp=list(row)
		temp[0]=str(temp[0])
		temp[2]=temp[2].isoformat()
		temp[3]=str(temp[3])
		temp[4]=temp[4].strftime("%Y-%m-%d %H:%M:%S")
		if (temp[5] is not None):
			temp[5]=temp[5].strftime("%Y-%m-%d %H:%M:%S")
		temp[6]=temp[6].strftime("%Y-%m-%d %H:%M:%S")
		if (temp[7] is not None):
			temp[7]=temp[7].strftime("%Y-%m-%d %H:%M:%S")
		temp[8]=str(temp[8])
		temp[10]=str(temp[10])
		temp[11]=str(temp[11])
		temp[12]=str(temp[12])
		cmd = "SELECT price, remain_seats\
				FROM Flight_ticket_manage_share\
				WHERE flight_id= :flight_id\
				ORDER BY class;"
		try:
			cur = g.conn.execute(text(cmd), flight_id=temp[0])
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flight_id=temp[0])
		temp2=cur.fetchall()
		#print temp2
		temp.append(temp2[2][0])
		temp.append(temp2[2][1])
		temp.append(temp2[0][0])
		temp.append(temp2[0][1])
		temp.append(temp2[1][0])
		temp.append(temp2[1][1])
		#print temp
		result.append(temp)
	return jsonify(coords = result)

@app.route("/modify_share_flight")
def modifyShareFlightInfo():
	args = request.args
	flight_id = args.get('flight_id')
	sched_dept_t = args.get('sched_dept_t')
	act_dept_t = args.get('act_dept_t')
	sched_arr_t = args.get('sched_arr_t')
	act_arr_t = args.get('act_arr_t')
	status = args.get('status')
	host_airline = args.get('host_airline')
	host_flight_num = args.get('host_flight_num')
	aircraft = args.get('aircraft')
	first_total = args.get('first_total')
	business_total = args.get('business_total')
	economic_total = args.get('economic_total')
	first_full_price = args.get('first_full_price')
	business_full_price = args.get('business_full_price')
	economic_full_price = args.get('economic_full_price')
	if len(act_dept_t)==0:
		act_dept_t=None
	if len(act_arr_t)==0:
		act_arr_t=None

	cmd = "UPDATE Flight SET sched_dept_t = :sched_dept_t, act_dept_t = :act_dept_t, sched_arr_t = :sched_arr_t, act_arr_t = :act_arr_t, flight_status= :flight_status WHERE flight_id= :flight_id;"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, sched_dept_t=sched_dept_t, act_dept_t=act_dept_t, sched_arr_t=sched_arr_t, act_arr_t=act_arr_t, flight_status=status)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, sched_dept_t=sched_dept_t, act_dept_t=act_dept_t, sched_arr_t=sched_arr_t, act_arr_t=act_arr_t, flight_status=status)
	
	cmd = "UPDATE Flight_share SET host_airline= :host_airline, host_flight_no= :host_flight_no, aircraft_type= :aircraft WHERE flight_id= :flight_id;"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, host_airline=host_airline, host_flight_no=host_flight_num, aircraft=aircraft)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, host_airline=host_airline, host_flight_no=host_flight_num, aircraft=aircraft)

	cmd = "UPDATE Flight_ticket_manage_share SET price= :price,remain_seats= :remain_seats WHERE  flight_id= :flight_id AND class='F';"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, price=first_full_price, remain_seats=first_total)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, price=first_full_price, remain_seats=first_total)

	cmd = "UPDATE Flight_ticket_manage_share SET price= :price,remain_seats= :remain_seats WHERE  flight_id= :flight_id AND class='B';"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, price=business_full_price, remain_seats=business_total)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, price=business_full_price, remain_seats=business_total)

	cmd = "UPDATE Flight_ticket_manage_share SET price= :price,remain_seats= :remain_seats WHERE  flight_id= :flight_id AND class='E';"
	try:
		cur = g.conn.execute(text(cmd), flight_id=flight_id, price=economic_full_price, remain_seats=economic_total)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), flight_id=flight_id, price=economic_full_price, remain_seats=economic_total)

	result=[('Success',)]
	return jsonify(coords = result)

if __name__ == "__main__":
	import click

	@click.command()
	@click.option('--debug', is_flag=True)
	@click.option('--threaded', is_flag=True)
	@click.argument('HOST', default='0.0.0.0')
	@click.argument('PORT', default=7000, type=int)
	def run(debug, threaded, host, port):
		HOST, PORT = host, port
		print "running on %s:%d" % (HOST, PORT)
		app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)

	run()
