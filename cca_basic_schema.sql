############################################
#
# SQL schema for W4111 Project 1 Part 1
# Project name: Cross Columbia Airlines Ticket and Flight System
# Group member: Ziyi Luo        (zl2471) & Youjia Zhang (yz2797)
# Update timestamp: 2:00 AM Oct 03 2016
#
############################################

# Entities and combined entities

CREATE TABLE Passenger (
	user_id int PRIMARY KEY,        	/* unique id for user */
	first_name varchar(100) NOT NULL,   /* first name of user */
	middle_name varchar(100),           /* middle name of user */
	last_name varchar(100) NOT NULL,    /* last name of user */
	email_addr varchar(50),         	/* email of user */
	cell_no varchar(20)             	/* cell phone number of user */
);

CREATE TABLE Airport (
	iata_code char(3) PRIMARY KEY, 	/* iata code of the airport */
	icao char(4)  NOT NULL,			/* icao code of the airport */
	city varchar(30) NOT NULL,		/* city name */
	state char(2),					/* state name */
	timezone varchar(4), 			/* timezone the airport belongs to, currently support E/C/M/P/AK/HA + S/D + T */
	name text NOT NULL,		/* airport name */
	airport_cat char(3) NOT NULL,	/* airport category defined by CCA */
	CHECK (
		airport_cat = 'HUB' OR		/* Hub airport including JFK & SFO */
		airport_cat = 'REG'			/* Otherwise */
	)
);

CREATE TABLE Aircraft (
	aircraft_id char(7) PRIMARY KEY,             /* unique id for aircraft */
	type varchar(10) NOT NULL,                   /* type of aircraft ie. A320 */
	buy_year smallint NOT NULL,                  /* year of buying aircraft */
	fc_seats smallint,                           /* number of first class seats in aircraft */
	bc_seats smallint,                           /* number of business class seats in aircraft */
	ec_seats smallint,                           /* number of economy class seats in aircraft */
	cur_location varchar(4),                     /* current location of aircraft */
	maintainence_status varchar(5) NOT NULL,     /* maintainence status of aircraft */
	range_typical_payload smallint               /* range of aircraft under typical payload */
	CHECK (
		fc_seats >= 0 AND
		bc_seats >= 0 AND
		ec_seats >= 0 AND
		buy_year >= 2010
	)
);

CREATE TABLE Route (
	route_id smallint PRIMARY KEY,                               /* unique id for route */
	dept_airport char(3) NOT NULL,                               /* departure airport of route */
	arr_airport char(3) NOT NULL,                                /* arrival airport of route */
	scheduled_height int,                                        /* scheduled height in route */
	duraion int,                                                 /* time duration in route in term of minute */
	distance int,                                                /* distance of route */
	description text,                                            /* route description */
	FOREIGN KEY (dept_airport) REFERENCES Airport (iata_code)    /* referenceing departure airport and arrival airport to airport */
		ON DELETE NO ACTION,                                     /* refuse delete airport first */
	FOREIGN KEY (arr_airport) REFERENCES Airport (iata_code)
		ON DELETE NO ACTION
);

CREATE TABLE Flight (
	flight_id char(19) PRIMARY KEY, /* unique id for flight */
	flight_no smallint NOT NULL,    /* number of flight */
	flight_date date NOT NULL,      /* date of flight */
	flight_status char(3) NOT NULL, /* status of flight */
	sched_dept_t TIMESTAMP,         /* scheduled departure time of flight */
	sched_arr_t TIMESTAMP,          /* scheduled arrival time of flight */
	act_dept_t TIMESTAMP,           /* actual departure time of flight */
	act_arr_t TIMESTAMP,            /* actual arrival time of flight */
	CHECK (
		flight_status = 'ARR' OR 	/* ARRIVED */
		flight_status = 'BOA' OR 	/* BOARDING */
		flight_status = 'CCL' OR 	/* CANCELLED */
		flight_status = 'DLY' OR 	/* DELAYED */
		flight_status = 'EME' OR 	/* EMERGENCY */
		flight_status = 'RDL' OR 	/* EN ROUTE DELAYED */
		flight_status = 'RER' OR 	/* EN ROUTE EARILER */
		flight_status = 'RES' OR 	/* RESCHEDULED */
		flight_status = 'ROU' OR 	/* EN ROUTE ON TIME */
		flight_status = 'SCH' OR 	/* SCHEDULED */
		flight_status = 'UNK' 		/* UNKOWN */
	)
);

CREATE TABLE Flight_CC (
	flight_id char(19) REFERENCES Flight,              /* reference to flight */
	route_id smallint NOT NULL REFERENCES Route,       /* reference to route */
	aircraft_id char(7) NOT NULL REFERENCES Aircraft,  /* reference to aircrate */
	PRIMARY KEY (flight_id)
);

CREATE TABLE Flight_share (
	flight_id char(19) REFERENCES Flight,  /* reference to flight */
	host_airline varchar(3) NOT NULL,      /* host airline */
	host_flight_no smallint,               /* host airline flight number */
	dept_airport char(3) NOT NULL,         /* departure airport */
	arr_airport char(3) NOT NULL,          /* arrival airport */
	aircraft_type varchar(10),             /* aircraft type */
	PRIMARY KEY (flight_id)
);

CREATE TABLE Books (
	user_id int REFERENCES Passenger,      /* reference to user */
	flight_id char(19) REFERENCES Flight,       /* reference to flight */
	confirm_no char(6) NOT NULL UNIQUE,    /* confirm number of ticket */
	class char(1) NOT NULL,                /* class of ticket */
	cur_price real NOT NULL,               /* current price of ticket */
	book_timestamp TIMESTAMP NOT NULL,     /* timestamp of booking, after a period of time without payment, the pending will be cleaned and user need to rebook a ticket */
	ticket_status char(4),
	PRIMARY KEY (user_id, flight_id),
	CHECK (                                /* limited class of ticet B=business class, E=economy class, F=first class */
		( class = 'B' OR
		class = 'E' OR
		class = 'F') AND
		( ticket_status = 'PEND' OR		   /* PENDING_PAYMENT */
		ticket_status = 'PAID' OR 		   /* TICKET_PAID */
		ticket_status = 'LOCK' OR 		   /* TICKET_LOCKED */
		ticket_status = 'CANC') AND 	   /* TICKET_CANCELLED */
		cur_price >= 0
	)
);

CREATE TABLE Flight_ticket_manage_share (
	flight_id char(19) REFERENCES Flight_share,  /* reference to flight_shart */
	class char(1),                               /* class of ticket */
	price real,                                  /* current price of the class on the flight */
	remain_seats int,                            /* remain seat of the class on the flight */
	PRIMARY KEY (flight_id, class),
	CHECK (
		price >= 0 AND
		remain_seats >= 0
	)
);

CREATE TABLE Flight_ticket_manage_cc (
	flight_id char(19) REFERENCES Flight_cc,     /* reference to flight_cc */
	class char(1),                               /* class of ticket   */
	full_price real,                             /* full price of the class on the flight */
	remain_seats int,                            /* remain seat of the class on the flight */
	sold_seats int,                              /* sold seat of the class on the flight, used if aircraft change */
	price_rate real,                              /* current price = full_price * price_rate on the class on the flight */
	PRIMARY KEY (flight_id, class),
	CHECK (
		full_price >= 0 AND
		remain_seats >= 0 AND
		sold_seats >= 0 AND
		price_rate >= 0
	)
);

INSERT INTO Passenger (user_id, first_name, middle_name, last_name, email_addr, cell_no)
VALUES (1, 'Ziyi', NULL, 'Luo', 'zl2471@columbia.edu', '+19174351234'),
       (2, 'Youjia', NULL, 'Zhang', 'yz2797@columbia.edu', '+13474055678'),
       (3, 'Barack', 'H', 'Obama', 'obama@mail.whitehouse.gov', '+112580'),
       (4, 'Eugene', NULL, 'Wu', 'ewu@cs.columbia.edu', '+19172332333'),
       (5, 'Jessica', NULL, 'He', 'jeh@google.com', '+12065723119'),
       (6, 'George', 'W', 'Bush', 'george.w.bush@gmail.com', '+11105723119'),
       (7, 'Hillary', NULL, 'Clinton', 'hillary.clinton@gmail.com', '+111051942293'),
       (8, 'Qi', NULL, 'Wang', 'qi.wang@yahoo.com', '+19172342932'),
       (9, 'Larry', NULL, 'Page', 'larry@google.com', '+1900918'),
       (10, 'Sheldon ', NULL, 'Cooper', 'sheldon@caltech.edu', '+18211928282');

INSERT INTO Airport (iata_code, icao, city, state, timezone, name, airport_cat)
VALUES ('JFK', 'KJFK', 'New York', 'NY', 'EDT', 'John F. Kennedy International Airport', 'HUB'),
       ('SFO', 'KSFO', 'San Francisco', 'CA', 'PDT', 'San Francisco International Airport', 'HUB'),
       ('ATL', 'KATL', 'Atlanta', 'GA','EDT', 'Hartsfield-Jackson Atlanta International Airport', 'HUB'),
       ('SEA', 'KSEA', 'Seattle', 'WA','PDT', 'Seattle-Tacoma International Airpor', 'REG'),
       ('DFW', 'KDFW', 'Dallas', 'TX', 'CDT','Dallas/Fort Worth International Airport', 'REG'),
       ('ORD', 'KORD', 'Chicago', 'IL', 'CDT', 'O''Hare International Airport', 'REG'),
       ('LAX', 'KLAX', 'Los Angeles', 'CA', 'PDT', 'Los Angeles International Airport', 'REG'),
       ('DEN', 'KDEN', 'Denver', 'CO', 'MDT', 'Denver International Airport', 'REG'),
       ('MIA', 'KMIA', 'Miami', 'FL', 'EDT', 'Miami International Airport', 'REG'),
       ('BOS', 'KBOS', 'Boston', 'MA', 'EDT', 'Logan International Airport', 'REG');

INSERT INTO Aircraft (
	aircraft_id,type, buy_year, fc_seats, bc_seats, ec_seats, cur_location,
	maintainence_status, range_typical_payload)
VALUES ('N201CC', 'A320-CEO', 2014, 0, 12, 138, 'JFK', 'GFF', 3812),
       ('N202CC', 'A320-CEO', 2014, 0, 12, 138, 'SFO', 'UM', 3812),
       ('N203CC', 'A320-CEO', 2015, 0, 12, 138, 'JFK', 'GFF', 3812),
       ('N204CC', 'A320-CEO', 2014, 0, 12, 138, 'SFO', 'GFF', 3812),
       ('N205CC', 'A320-NEO', 2016, 0, 12, 138, 'JFK', 'GFF', 4062),
       ('N206CC', 'A320-NEO', 2016, 0, 12, 138, 'SFO', 'GFF', 4062),
       ('N221CC', 'A319-CEO', 2014, 0, 8, 116, 'SFO', 'UM', 4343),
       ('N222CC', 'A319-CEO', 2015, 0, 8, 116, 'SFO', 'GFF', 4343),
       ('N223CC', 'A319-CEO', 2015, 0, 8, 116, 'ATL', 'GFF', 4343),
       ('N231CC', 'A321-NEO', 2016, 0, 16, 190, 'ATL', 'GFF', 4062),
       ('N232CC', 'A321-NEO', 2016, 0, 16, 190, 'JFK', 'GFF', 4062),
       ('N233CC', 'A321-NEO', 2016, 0, 16, 190, 'JFK', 'UM', 4062);

# References:
# [1] Aircraft registration - Wikipedia, the free encyclopedia
# https://en.wikipedia.org/wiki/Aircraft_registration
# [2] All About Airbus A 320 Family
# https://www.scribd.com/doc/30183955/All-About-Airbus-A-320-Family

INSERT INTO Route (route_id, dept_airport, arr_airport, scheduled_height, duraion, distance, description)
VALUES (1, 'JFK', 'SFO', 32500, 380, 2586, 'Regular height with direct flight; Redicrection at MI'),
       (2, 'SFO', 'JFK', 33500, 320, 2586, 'Regular height with direct flight; Redicrection at OH'),
       (3, 'JFK', 'ATL', 32500, 145, 761, 'Highest height restriction at DC; Redirction at VA'),
       (4, 'ATL', 'JFK', 31500, 135, 761, 'Highest height with redirction at VA'),
       (5, 'SFO', 'ATL', 33500, 275, 2139, 'Regular height with direct flight'),
       (6, 'ATL', 'SFO', 32500, 320, 2139, 'Lowest height with redirction at AR'),
       (7, 'JFK', 'MIA', 32500, 180, 1090, 'Height restriction at US Coast'),
       (8, 'MIA', 'JFK', 31500, 180, 1090, 'Height restriction at US Coast'),
       (9, 'SFO', 'SEA', 31500, 120, 679, 'Highest height restriction at WA and OR'),
       (10, 'SEA', 'SFO', 30500, 120, 679, 'Highest height restriction at WA and OR'),
       (11, 'SFO', 'LAX', 30500, 70, 337, 'Regular height with direct flight'),
       (12, 'LAX', 'SFO', 31500, 70, 337, 'Regular height with direct flight'),
       (13, 'JFK', 'DFW', 32500, 230, 1391, 'Redirction at PA, WV'),
       (14, 'DFW', 'JFK', 33500, 205, 1391, 'Redirction at PA');

# "Odd North East" Altitude Rules
# http://www.lapeeraviation.com/odd-north-east/

INSERT INTO Flight (flight_id, flight_no, flight_date, flight_status, sched_dept_t, act_dept_t, sched_arr_t, act_arr_t)
VALUES ('R201606240003JFKSFO',3,'2016-06-24','ARR','2016-06-24 10:00:00','2016-06-24 10:05:03','2016-06-24 13:20:00','2016-06-24 13:32:15'),
       ('R201606240004SFOJFK',4,'2016-06-24','ARR','2016-06-24 15:20:00','2016-06-24 15:20:00','2016-06-24 23:40:00','2016-06-24 23:30:30'),
       ('R201606270003JFKSFO',3,'2016-06-27','CCL','2016-06-27 10:00:00',null,'2016-06-27 13:20:00',null),
       ('R201606270004SFOJFK',4,'2016-06-27','CCL','2016-06-27 15:20:00',null,'2016-06-27 23:40:00',null),
       ('R201606270005JFKSFO',5,'2016-06-27','CCL','2016-06-27 16:30:00',null,'2016-06-27 19:50:00',null),
       ('R201606270006SFOJFK',6,'2016-06-27','CCL','2016-06-27 22:15:00',null,'2016-06-28 06:35:00',null),
       ('R201606270007JFKATL',7,'2016-06-27','CCL','2016-06-27 09:20:00',null,'2016-06-27 11:45:00',null),
       ('R201606270008ATLJFK',8,'2016-06-27','CCL','2016-06-27 14:15:00',null,'2016-06-27 16:30:00',null),
       ('R201606270009SFOATL',9,'2016-06-27','ARR','2016-06-27 08:55:00','2016-06-27 09:00:00','2016-06-27 16:30:00','2016-06-27 16:32:00'),
       ('R201606270010ATLSFO',10,'2016-06-27','ARR','2016-06-27 18:35:00','2016-06-27 18:52:00','2016-06-27 20:55:00','2016-06-27 21:03:00'),
       ('R201606270011JFKMIA',11,'2016-06-27','CCL','2016-06-27 09:35:00',null,'2016-06-27 12:35:00',null),
       ('R201606270012MIAJFK',12,'2016-06-27','CCL','2016-06-27 14:10:00',null,'2016-06-27 17:10:00',null),
       ('R201606270101SFOSEA',101,'2016-06-27','ARR','2016-06-27 07:15:00','2016-06-27 07:14:00','2016-06-27 09:15:00','2016-06-27 09:10:00'),
       ('R201606270102SEASFO',102,'2016-06-27','ARR','2016-06-27 10:25:00','2016-06-27 10:26:00','2016-06-27 12:25:00','2016-06-27 12:25:00'),
       ('R201606270103SFOSEA',103,'2016-06-27','ARR','2016-06-27 14:15:00','2016-06-27 14:19:00','2016-06-27 16:15:00','2016-06-27 16:12:00'),
       ('R201606270104SEASFO',104,'2016-06-27','ARR','2016-06-27 17:25:00','2016-06-27 17:25:00','2016-06-27 19:25:00','2016-06-27 17:27:00'),
       ('S201606271023SFOSEA',1023,'2016-06-27','ARR','2016-06-27 14:45:00','2016-06-27 14:47:00','2016-06-27 16:45:00','2016-06-27 16:45:00'),
       ('S201606271024SEASFO',1024,'2016-06-27','ARR','2016-06-27 07:00:00','2016-06-27 07:03:00','2016-06-27 09:05:00','2016-06-27 09:03:00'),
       ('S201606271027SEALAX',1027,'2016-06-27','ARR','2016-06-27 07:05:00','2016-06-27 07:07:00','2016-06-27 09:45:00','2016-06-27 09:46:00'),
       ('S201606271028LAXSEA',1028,'2016-06-27','ARR','2016-06-27 17:20:00','2016-06-27 17:20:00','2016-06-27 20:10:00','2016-06-27 20:07:00'),
       ('S201606271029SEALAX',1029,'2016-06-27','ARR','2016-06-27 12:00:00','2016-06-27 12:01:00','2016-06-27 14:39:00','2016-06-27 14:38:00'),
       ('S201606271030LAXSEA',1030,'2016-06-27','ARR','2016-06-27 14:45:00','2016-06-27 14:52:00','2016-06-27 17:25:00','2016-06-27 17:30:00'),
       ('S201606271009JFKBOS',1009,'2016-06-27','CCL','2016-06-27 11:00:00',null,'2016-06-27 12:09:00',null),
       ('S201606271010BOSJFK',1010,'2016-06-27','CCL','2016-06-27 17:04:00',null,'2016-06-27 18:40:00',null),
       ('S201606271011JFKBOS',1011,'2016-06-27','CCL','2016-06-27 19:25:00',null,'2016-06-27 20:50:00',null),
       ('S201606271012BOSJFK',1012,'2016-06-27','CCL','2016-06-27 19:35:00',null,'2016-06-27 21:06:00',null),
       ('R201611060003JFKSFO',3,'2016-11-06','SCH','2016-11-06 10:00:00',null,'2016-11-06 13:20:00',null), /* N201CC */
       ('R201611070003JFKSFO',3,'2016-11-07','SCH','2016-11-07 10:00:00',null,'2016-11-07 13:20:00',null),
       ('R201611060004SFOJFK',4,'2016-11-06','SCH','2016-11-06 15:20:00',null,'2016-11-06 23:40:00',null), /* N201CC */
       ('R201611070004SFOJFK',4,'2016-11-07','SCH','2016-11-07 15:20:00',null,'2016-11-07 23:40:00',null),
       ('R201611060005JFKSFO',5,'2016-11-06','SCH','2016-11-06 16:30:00',null,'2016-11-06 19:50:00',null), /* N206CC */
       ('R201611070005JFKSFO',5,'2016-11-07','SCH','2016-11-07 16:30:00',null,'2016-11-07 19:50:00',null),
       ('R201611060006SFOJFK',6,'2016-11-06','SCH','2016-11-06 22:15:00',null,'2016-11-07 06:35:00',null), /* N206CC */
       ('R201611070006SFOJFK',6,'2016-11-07','SCH','2016-11-07 22:15:00',null,'2016-11-08 06:35:00',null),
       ('R201611060007JFKATL',7,'2016-11-06','SCH','2016-11-06 09:20:00',null,'2016-11-06 11:45:00',null), /* N232CC */
       ('R201611070007JFKATL',7,'2016-11-07','SCH','2016-11-07 09:20:00',null,'2016-11-07 11:45:00',null),
       ('R201611060008ATLJFK',8,'2016-11-06','SCH','2016-11-06 14:15:00',null,'2016-11-06 16:30:00',null), /* N232CC */
       ('R201611070008ATLJFK',8,'2016-11-07','SCH','2016-11-07 14:15:00',null,'2016-11-07 16:30:00',null),
       ('R201611060009SFOATL',9,'2016-11-06','SCH','2016-11-06 08:55:00',null,'2016-11-06 16:30:00',null), /* N231CC */
       ('R201611070009SFOATL',9,'2016-11-07','SCH','2016-11-07 08:55:00',null,'2016-11-07 16:30:00',null),
       ('R201611060010ATLSFO',10,'2016-11-06','SCH','2016-11-06 18:35:00',null,'2016-11-06 20:55:00',null), /* N231CC */
       ('R201611070010ATLSFO',10,'2016-11-07','SCH','2016-11-07 18:35:00',null,'2016-11-07 20:55:00',null),
       ('R201611060011JFKMIA',11,'2016-11-06','SCH','2016-11-06 09:35:00',null,'2016-11-06 12:35:00',null), /* N205CC */
       ('R201611070011JFKMIA',11,'2016-11-07','SCH','2016-11-07 09:35:00',null,'2016-11-07 12:35:00',null),
       ('R201611060012MIAJFK',12,'2016-11-06','SCH','2016-11-06 14:10:00',null,'2016-11-06 17:10:00',null), /* N205CC */
       ('R201611070012MIAJFK',12,'2016-11-07','SCH','2016-11-07 14:10:00',null,'2016-11-07 17:10:00',null),
       ('R201611060101SFOSEA',101,'2016-11-06','SCH','2016-11-06 07:15:00',null,'2016-11-06 09:15:00',null), /* N222CC */
       ('R201611070101SFOSEA',101,'2016-11-07','SCH','2016-11-07 07:15:00',null,'2016-11-07 09:15:00',null),
       ('R201611060102SEASFO',102,'2016-11-06','SCH','2016-11-06 10:25:00',null,'2016-11-06 12:25:00',null), /* N222CC */
       ('R201611070102SEASFO',102,'2016-11-07','SCH','2016-11-07 10:25:00',null,'2016-11-07 12:25:00',null),
       ('R201611060103SFOSEA',103,'2016-11-06','SCH','2016-11-06 14:15:00',null,'2016-11-06 16:15:00',null), /* N222CC */
       ('R201611070103SFOSEA',103,'2016-11-07','SCH','2016-11-07 14:15:00',null,'2016-11-07 16:15:00',null),
       ('R201611060104SEASFO',104,'2016-11-06','SCH','2016-11-06 17:25:00',null,'2016-11-06 19:25:00',null), /* N222CC */
       ('R201611070104SEASFO',104,'2016-11-07','SCH','2016-11-07 17:25:00',null,'2016-11-07 19:25:00',null),
       ('S201611041023SFOSEA',1023,'2016-11-04','SCH','2016-11-04 14:45:00',null,'2016-11-04 16:45:00',null), /* Virgin 744 - A319 */
       ('S201611051023SFOSEA',1023,'2016-11-05','SCH','2016-11-05 14:45:00',null,'2016-11-05 16:45:00',null),
       ('S201611041024SEASFO',1024,'2016-11-04','SCH','2016-11-04 07:00:00',null,'2016-11-04 09:05:00',null), /* Virgin 751 - A319 */
       ('S201611051024SEASFO',1024,'2016-11-05','SCH','2016-11-05 07:00:00',null,'2016-11-05 09:05:00',null),
       ('S201611041027SEALAX',1027,'2016-11-04','SCH','2016-11-04 07:05:00',null,'2016-11-04 09:45:00',null), /* Alaska 466 - B737 */
       ('S201611051027SEALAX',1027,'2016-11-05','SCH','2016-11-05 07:05:00',null,'2016-11-05 09:45:00',null),
       ('S201611041028LAXSEA',1028,'2016-11-04','SCH','2016-11-04 17:20:00',null,'2016-11-04 20:10:00',null), /* Alaska 449 - B737 */
       ('S201611051028LAXSEA',1028,'2016-11-05','SCH','2016-11-05 17:20:00',null,'2016-11-05 20:10:00',null),
       ('S201611041029SEALAX',1029,'2016-11-04','SCH','2016-11-04 12:00:00',null,'2016-11-04 14:39:00',null), /* Alaska 460 - B737 */
       ('S201611051029SEALAX',1029,'2016-11-05','SCH','2016-11-05 12:00:00',null,'2016-11-05 14:39:00',null),
       ('S201611041030LAXSEA',1030,'2016-11-04','SCH','2016-11-04 14:45:00',null,'2016-11-04 17:25:00',null), /* Alaska 325 - B737 */
       ('S201611051030LAXSEA',1030,'2016-11-05','SCH','2016-11-05 14:45:00',null,'2016-11-05 17:25:00',null),
       ('S201611041009JFKBOS',1009,'2016-11-04','SCH','2016-11-04 11:00:00',null,'2016-11-04 12:09:00',null), /* Delta 2487 - B717 */
       ('S201611051009JFKBOS',1009,'2016-11-05','SCH','2016-11-05 11:00:00',null,'2016-11-05 12:09:00',null),
       ('S201611041010BOSJFK',1010,'2016-11-04','SCH','2016-11-04 17:04:00',null,'2016-11-04 18:40:00',null), /* Delta 476 - B717 */
       ('S201611051010BOSJFK',1010,'2016-11-05','SCH','2016-11-05 17:04:00',null,'2016-11-05 18:40:00',null),
       ('S201611041011JFKBOS',1011,'2016-11-04','SCH','2016-11-04 19:25:00',null,'2016-11-04 20:50:00',null), /* Delta 431 - A319 */
       ('S201611051011JFKBOS',1011,'2016-11-05','SCH','2016-11-05 19:25:00',null,'2016-11-05 20:50:00',null),
       ('S201611041012BOSJFK',1012,'2016-11-04','SCH','2016-11-04 19:35:00',null,'2016-11-04 21:06:00',null), /* Delta 793 - A319 */
       ('S201611051012BOSJFK',1012,'2016-11-05','SCH','2016-11-05 19:35:00',null,'2016-11-05 21:06:00',null);

INSERT INTO Flight_CC (flight_id,route_id,aircraft_id)
VALUES ('R201606240003JFKSFO',1,'N201CC'),
       ('R201606270003JFKSFO',1,'N201CC'),
       ('R201611060003JFKSFO',1,'N201CC'),
       ('R201611070003JFKSFO',1,'N201CC'),
       ('R201606240004SFOJFK',2,'N201CC'),
       ('R201606270004SFOJFK',2,'N201CC'),
       ('R201611060004SFOJFK',2,'N201CC'),
       ('R201611070004SFOJFK',2,'N201CC'),
       ('R201606270005JFKSFO',1,'N206CC'),
       ('R201611060005JFKSFO',1,'N206CC'),
       ('R201611070005JFKSFO',1,'N206CC'),
       ('R201606270006SFOJFK',2,'N206CC'),
       ('R201611060006SFOJFK',2,'N206CC'),
       ('R201611070006SFOJFK',2,'N206CC'),
       ('R201606270007JFKATL',3,'N232CC'),
       ('R201611060007JFKATL',3,'N232CC'),
       ('R201611070007JFKATL',3,'N232CC'),
       ('R201606270008ATLJFK',4,'N232CC'),
       ('R201611060008ATLJFK',4,'N232CC'),
       ('R201611070008ATLJFK',4,'N232CC'),
       ('R201606270009SFOATL',5,'N231CC'),
       ('R201611060009SFOATL',5,'N231CC'),
       ('R201611070009SFOATL',5,'N231CC'),
       ('R201606270010ATLSFO',6,'N231CC'),
       ('R201611060010ATLSFO',6,'N231CC'),
       ('R201611070010ATLSFO',6,'N231CC'),
       ('R201606270011JFKMIA',7,'N205CC'),
       ('R201611060011JFKMIA',7,'N205CC'),
       ('R201611070011JFKMIA',7,'N205CC'),
       ('R201606270012MIAJFK',8,'N205CC'),
       ('R201611060012MIAJFK',8,'N205CC'),
       ('R201611070012MIAJFK',8,'N205CC'),
       ('R201606270101SFOSEA',9,'N222CC'),
       ('R201611060101SFOSEA',9,'N222CC'),
       ('R201611070101SFOSEA',9,'N222CC'),
       ('R201606270102SEASFO',10,'N222CC'),
       ('R201611060102SEASFO',10,'N222CC'),
       ('R201611070102SEASFO',10,'N222CC'),
       ('R201606270103SFOSEA',9,'N222CC'),
       ('R201611060103SFOSEA',9,'N222CC'),
       ('R201611070103SFOSEA',9,'N222CC'),
       ('R201606270104SEASFO',10,'N222CC'),
       ('R201611060104SEASFO',10,'N222CC'),
       ('R201611070104SEASFO',10,'N222CC');

INSERT INTO Flight_share (flight_id,host_airline,host_flight_no,
	dept_airport,arr_airport,aircraft_type)
VALUES ('S201606271009JFKBOS','DL',2487,'JFK','BOS','B717'),
       ('S201611041009JFKBOS','DL',2487,'JFK','BOS','B717'),
       ('S201611051009JFKBOS','DL',2487,'JFK','BOS','B717'),
       ('S201606271010BOSJFK','DL',476,'BOS','JFK','B717'),
       ('S201611041010BOSJFK','DL',476,'BOS','JFK','B717'),
       ('S201611051010BOSJFK','DL',476,'BOS','JFK','B717'),
       ('S201606271011JFKBOS','DL',431,'JFK','BOS','A319'),
       ('S201611041011JFKBOS','DL',431,'JFK','BOS','A319'),
       ('S201611051011JFKBOS','DL',431,'JFK','BOS','A319'),
       ('S201606271012BOSJFK','DL',793,'BOS','JFK','A319'),
       ('S201611041012BOSJFK','DL',793,'BOS','JFK','A319'),
       ('S201611051012BOSJFK','DL',793,'BOS','JFK','A319'),
       ('S201606271023SFOSEA','VX',744,'SFO','SEA','A319'),
       ('S201611041023SFOSEA','VX',744,'SFO','SEA','A319'),
       ('S201611051023SFOSEA','VX',744,'SFO','SEA','A319'),
       ('S201606271024SEASFO','VX',751,'SEA','SFO','A319'),
       ('S201611041024SEASFO','VX',751,'SEA','SFO','A319'),
       ('S201611051024SEASFO','VX',751,'SEA','SFO','A319'),
       ('S201606271027SEALAX','AS',466,'SEA','LAX','B737'),
       ('S201611041027SEALAX','AS',466,'SEA','LAX','B737'),
       ('S201611051027SEALAX','AS',466,'SEA','LAX','B737'),
       ('S201606271028LAXSEA','AS',449,'LAX','SEA','B739'),
       ('S201611041028LAXSEA','AS',449,'LAX','SEA','B739'),
       ('S201611051028LAXSEA','AS',449,'LAX','SEA','B739'),
       ('S201606271029SEALAX','AS',460,'SEA','LAX','B738'),
       ('S201611041029SEALAX','AS',460,'SEA','LAX','B738'),
       ('S201611051029SEALAX','AS',460,'SEA','LAX','B738'),
       ('S201606271030LAXSEA','AS',325,'LAX','SEA','B737'),
       ('S201611041030LAXSEA','AS',325,'LAX','SEA','B737'),
       ('S201611051030LAXSEA','AS',325,'LAX','SEA','B737');

INSERT INTO Books (user_id, flight_id, confirm_no, class, cur_price, book_timestamp, ticket_status)
VALUES (1,'R201606240003JFKSFO','CAS04J','B',1245.70,'2016-06-20 17:05:03','PAID'),
       (2,'R201606240003JFKSFO','DAP045','E',724.60,'2016-05-20 20:05:03','PAID'),
       (1,'S201606271009JFKBOS','ZEU463','E',245.00,'2016-05-10 13:07:03','PAID'),
       (2,'S201606271009JFKBOS','ZER413','E',215.00,'2016-05-30 13:07:03','PAID'),
       (3,'S201611051011JFKBOS','AFE963','E',349.00,'2016-10-10 6:07:03','PAID'),
       (2,'R201611070104SEASFO','ADH903','E',876.30,'2016-10-07 16:07:03','PAID'),
       (4,'R201611060101SFOSEA','UED738','E',793.90,'2016-10-01 23:46:53','PAID'),
       (5,'S201611041024SEASFO','PETTQ8','E',403.90,'2016-10-11 20:37:53','PAID'),
       (6,'R201611060006SFOJFK','P873ER','E',633.90,'2016-09-07 21:37:53','PAID'),
       (7,'R201611060010ATLSFO','PUI3ER','E',476.00,'2016-09-17 22:33:53','PAID');

INSERT INTO Flight_ticket_manage_share (flight_id, class, price, remain_seats)
VALUES ('S201606271009JFKBOS','E',63,3),
       ('S201606271009JFKBOS','B',180,0),
       ('S201606271009JFKBOS','F',0,0),
       ('S201611041009JFKBOS','E',79,23),
       ('S201611041009JFKBOS','B',160,0),
       ('S201611041009JFKBOS','F',0,0),
       ('S201611051009JFKBOS','E',83,18),
       ('S201611051009JFKBOS','B',160,1),
       ('S201611051009JFKBOS','F',0,0),
       ('S201606271010BOSJFK','E',63,0),
       ('S201606271010BOSJFK','B',180,0),
       ('S201606271010BOSJFK','F',0,0),
       ('S201611041010BOSJFK','E',85,41),
       ('S201611041010BOSJFK','B',160,0),
       ('S201611041010BOSJFK','F',0,0),
       ('S201611051010BOSJFK','E',99,10),
       ('S201611051010BOSJFK','B',160,1),
       ('S201611051010BOSJFK','F',0,0),
       ('S201606271011JFKBOS','E',61,3),
       ('S201606271011JFKBOS','B',165,0),
       ('S201606271011JFKBOS','F',0,0),
       ('S201611041011JFKBOS','E',69,10),
       ('S201611041011JFKBOS','B',145,3),
       ('S201611041011JFKBOS','F',0,0),
       ('S201611051011JFKBOS','E',75,5),
       ('S201611051011JFKBOS','B',140,1),
       ('S201611051011JFKBOS','F',0,0),
       ('S201606271012BOSJFK','E',90,0),
       ('S201606271012BOSJFK','B',170,0),
       ('S201606271012BOSJFK','F',0,0),
       ('S201611041012BOSJFK','E',85,29),
       ('S201611041012BOSJFK','B',145,2),
       ('S201611041012BOSJFK','F',0,0),
       ('S201611051012BOSJFK','E',75,11),
       ('S201611051012BOSJFK','B',140,2),
       ('S201611051012BOSJFK','F',0,0),
       ('S201606271023SFOSEA','E',89,0),
       ('S201606271023SFOSEA','B',195,0),
       ('S201606271023SFOSEA','F',0,0),
       ('S201611041023SFOSEA','E',73,12),
       ('S201611041023SFOSEA','B',182,1),
       ('S201611041023SFOSEA','F',0,0),
       ('S201611051023SFOSEA','E',73,21),
       ('S201611051023SFOSEA','B',182,3),
       ('S201611051023SFOSEA','F',0,0),
       ('S201606271024SEASFO','E',89,0),
       ('S201606271024SEASFO','B',195,0),
       ('S201606271024SEASFO','F',0,0),
       ('S201611041024SEASFO','E',73,7),
       ('S201611041024SEASFO','B',182,4),
       ('S201611041024SEASFO','F',0,0),
       ('S201611051024SEASFO','E',73,17),
       ('S201611051024SEASFO','B',182,1),
       ('S201611051024SEASFO','F',0,0),
       ('S201606271027SEALAX','E',65,3),
       ('S201606271027SEALAX','B',210,0),
       ('S201606271027SEALAX','F',0,0),
       ('S201611041027SEALAX','E',63,25),
       ('S201611041027SEALAX','B',220,2),
       ('S201611041027SEALAX','F',0,0),
       ('S201611051027SEALAX','E',63,29),
       ('S201611051027SEALAX','B',220,2),
       ('S201611051027SEALAX','F',0,0),
       ('S201606271028LAXSEA','E',65,9),
       ('S201606271028LAXSEA','B',210,0),
       ('S201606271028LAXSEA','F',0,0),
       ('S201611041028LAXSEA','E',63,5),
       ('S201611041028LAXSEA','B',222,3),
       ('S201611041028LAXSEA','F',0,0),
       ('S201611051028LAXSEA','E',63,32),
       ('S201611051028LAXSEA','B',222,2),
       ('S201611051028LAXSEA','F',0,0),
       ('S201606271029SEALAX','E',65,0),
       ('S201606271029SEALAX','B',210,0),
       ('S201606271029SEALAX','F',0,0),
       ('S201611041029SEALAX','E',63,10),
       ('S201611041029SEALAX','B',218,0),
       ('S201611041029SEALAX','F',0,0),
       ('S201611051029SEALAX','E',63,25),
       ('S201611051029SEALAX','B',236,1),
       ('S201611051029SEALAX','F',0,0),
       ('S201606271030LAXSEA','E',65,1),
       ('S201606271030LAXSEA','B',210,0),
       ('S201606271030LAXSEA','F',0,0),
       ('S201611041030LAXSEA','E',63,20),
       ('S201611041030LAXSEA','B',228,2),
       ('S201611041030LAXSEA','F',0,0),
       ('S201611051030LAXSEA','E',63,19),
       ('S201611051030LAXSEA','B',228,1),
       ('S201611051030LAXSEA','F',0,0);

INSERT INTO Flight_ticket_manage_cc (flight_id,class,full_price,remain_seats,sold_seats,price_rate)
VALUES ('R201606240003JFKSFO','E',450,8,130,0.8),
       ('R201606240003JFKSFO','B',1080,0,12,0.8),
       ('R201606240003JFKSFO','F',0,0,0,0),
       ('R201606270003JFKSFO','E',450,49,89,0.82),
       ('R201606270003JFKSFO','B',1080,0,12,0.8),
       ('R201606270003JFKSFO','F',0,0,0,0),
       ('R201611060003JFKSFO','E',450,40,98,0.7),
       ('R201611060003JFKSFO','B',1080,4,8,0.75),
       ('R201611060003JFKSFO','F',0,0,0,0),
       ('R201611070003JFKSFO','E',450,28,110,0.7),
       ('R201611070003JFKSFO','B',1080,3,9,0.75),
       ('R201611070003JFKSFO','F',0,0,0,0),
       ('R201606240004SFOJFK','E',400,33,105,0.8),
       ('R201606240004SFOJFK','B',1000,0,12,0.8),
       ('R201606240004SFOJFK','F',0,0,0,0),
       ('R201606270004SFOJFK','E',400,18,120,0.82),
       ('R201606270004SFOJFK','B',1000,0,12,0.8),
       ('R201606270004SFOJFK','F',0,0,0,0),
       ('R201611060004SFOJFK','E',400,25,113,0.74),
       ('R201611060004SFOJFK','B',1000,3,9,0.75),
       ('R201611060004SFOJFK','F',0,0,0,0),
       ('R201611070004SFOJFK','E',400,33,105,0.74),
       ('R201611070004SFOJFK','B',1000,2,10,0.75),
       ('R201611070004SFOJFK','F',0,0,0,0),
       ('R201606270005JFKSFO','E',450,35,103,0.8),
       ('R201606270005JFKSFO','B',1080,0,12,0.8),
       ('R201606270005JFKSFO','F',0,0,0,0),
       ('R201611060005JFKSFO','E',450,53,85,0.7),
       ('R201611060005JFKSFO','B',1080,2,10,0.75),
       ('R201611060005JFKSFO','F',0,0,0,0),
       ('R201611070005JFKSFO','E',450,79,59,0.7),
       ('R201611070005JFKSFO','B',1080,1,11,0.75),
       ('R201611070005JFKSFO','F',0,0,0,0),
       ('R201606270006SFOJFK','E',400,46,92,0.7),
       ('R201606270006SFOJFK','B',1100,0,12,0.8),
       ('R201606270006SFOJFK','F',0,0,0,0),
       ('R201611060006SFOJFK','E',400,36,102,0.64),
       ('R201611060006SFOJFK','B',1100,0,12,0.75),
       ('R201611060006SFOJFK','F',0,0,0,0),
       ('R201611070006SFOJFK','E',400,51,87,0.64),
       ('R201611070006SFOJFK','B',1100,0,12,0.75),
       ('R201611070006SFOJFK','F',0,0,0,0),
       ('R201606270007JFKATL','E',320,54,136,0.8),
       ('R201606270007JFKATL','B',950,2,14,0.8),
       ('R201606270007JFKATL','F',0,0,0,0),
       ('R201611060007JFKATL','E',320,80,110,0.7),
       ('R201611060007JFKATL','B',950,7,9,0.75),
       ('R201611060007JFKATL','F',0,0,0,0),
       ('R201611070007JFKATL','E',320,71,119,0.7),
       ('R201611070007JFKATL','B',950,8,8,0.75),
       ('R201611070007JFKATL','F',0,0,0,0),
       ('R201606270008ATLJFK','E',320,64,126,0.8),
       ('R201606270008ATLJFK','B',950,0,16,0.8),
       ('R201606270008ATLJFK','F',0,0,0,0),
       ('R201611060008ATLJFK','E',320,50,140,0.7),
       ('R201611060008ATLJFK','B',950,4,12,0.75),
       ('R201611060008ATLJFK','F',0,0,0,0),
       ('R201611070008ATLJFK','E',320,61,129,0.7),
       ('R201611070008ATLJFK','B',950,4,12,0.75),
       ('R201611070008ATLJFK','F',0,0,0,0),
       ('R201606270009SFOATL','E',500,11,179,0.7),
       ('R201606270009SFOATL','B',950,0,16,0.8),
       ('R201606270009SFOATL','F',0,0,0,0),
       ('R201611060009SFOATL','E',500,65,125,0.55),
       ('R201611060009SFOATL','B',950,3,13,0.72),
       ('R201611060009SFOATL','F',0,0,0,0),
       ('R201611070009SFOATL','E',500,57,133,0.55),
       ('R201611070009SFOATL','B',950,4,12,0.72),
       ('R201611070009SFOATL','F',0,0,0,0),
       ('R201606270010ATLSFO','E',520,45,145,0.7),
       ('R201606270010ATLSFO','B',950,0,16,0.8),
       ('R201606270010ATLSFO','F',0,0,0,0),
       ('R201611060010ATLSFO','E',520,82,108,0.55),
       ('R201611060010ATLSFO','B',950,4,12,0.72),
       ('R201611060010ATLSFO','F',0,0,0,0),
       ('R201611070010ATLSFO','E',520,70,120,0.55),
       ('R201611070010ATLSFO','B',950,2,14,0.72),
       ('R201611070010ATLSFO','F',0,0,0,0),
       ('R201606270011JFKMIA','E',350,8,130,0.8),
       ('R201606270011JFKMIA','B',950,0,12,0.8),
       ('R201606270011JFKMIA','F',0,0,0,0),
       ('R201611060011JFKMIA','E',350,18,120,0.6),
       ('R201611060011JFKMIA','B',950,0,12,0.7),
       ('R201611060011JFKMIA','F',0,0,0,0),
       ('R201611070011JFKMIA','E',350,9,129,0.6),
       ('R201611070011JFKMIA','B',950,0,12,0.7),
       ('R201611070011JFKMIA','F',0,0,0,0),
       ('R201606270012MIAJFK','E',350,0,138,0.8),
       ('R201606270012MIAJFK','B',950,0,12,0.8),
       ('R201606270012MIAJFK','F',0,0,0,0),
       ('R201611060012MIAJFK','E',350,19,119,0.6),
       ('R201611060012MIAJFK','B',950,1,11,0.7),
       ('R201611060012MIAJFK','F',0,0,0,0),
       ('R201611070012MIAJFK','E',350,39,99,0.6),
       ('R201611070012MIAJFK','B',950,2,10,0.7),
       ('R201611070012MIAJFK','F',0,0,0,0),
       ('R201606270101SFOSEA','E',120,0,116,0.65),
       ('R201606270101SFOSEA','B',950,0,8,0.7),
       ('R201606270101SFOSEA','F',0,0,0,0),
       ('R201611060101SFOSEA','E',120,10,106,0.55),
       ('R201611060101SFOSEA','B',950,2,6,0.7),
       ('R201611060101SFOSEA','F',0,0,0,0),
       ('R201611070101SFOSEA','E',120,13,103,0.55),
       ('R201611070101SFOSEA','B',950,0,8,0.7),
       ('R201611070101SFOSEA','F',0,0,0,0),
       ('R201606270102SEASFO','E',120,0,116,0.7),
       ('R201606270102SEASFO','B',950,0,8,0.7),
       ('R201606270102SEASFO','F',0,0,0,0),
       ('R201611060102SEASFO','E',120,7,109,0.6),
       ('R201611060102SEASFO','B',950,1,7,0.7),
       ('R201611060102SEASFO','F',0,0,0,0),
       ('R201611070102SEASFO','E',120,4,112,0.6),
       ('R201611070102SEASFO','B',950,0,8,0.7),
       ('R201611070102SEASFO','F',0,0,0,0),
       ('R201606270103SFOSEA','E',120,0,116,0.7),
       ('R201606270103SFOSEA','B',950,0,8,0.7),
       ('R201606270103SFOSEA','F',0,0,0,0),
       ('R201611060103SFOSEA','E',120,7,109,0.6),
       ('R201611060103SFOSEA','B',950,1,7,0.7),
       ('R201611060103SFOSEA','F',0,0,0,0),
       ('R201611070103SFOSEA','E',120,5,111,0.6),
       ('R201611070103SFOSEA','B',950,1,7,0.7),
       ('R201611070103SFOSEA','F',0,0,0,0),
       ('R201606270104SEASFO','E',120,0,116,0.7),
       ('R201606270104SEASFO','B',950,0,8,0.7),
       ('R201606270104SEASFO','F',0,0,0,0),
       ('R201611060104SEASFO','E',120,14,102,0.6),
       ('R201611060104SEASFO','B',950,2,6,0.7),
       ('R201611060104SEASFO','F',0,0,0,0),
       ('R201611070104SEASFO','E',120,2,114,0.6),
       ('R201611070104SEASFO','B',950,1,7,0.7),
       ('R201611070104SEASFO','F',0,0,0,0);

-- Query for ticket booking history of Passenger "Youjia Zhang"
-- Return flight number, departure and arrival airport and city, flight date and scheduled departure time

SELECT Passenger.first_name,Passenger.last_name,Flight.flight_no,Route.dept_airport,Airport.city,Route.arr_airport,A.city,Flight.flight_date,Flight.sched_dept_t
FROM Flight_CC, Route, Passenger, Books, Flight, Airport,Airport A
WHERE Route.dept_airport=Airport.iata_code AND
Route.arr_airport=A.iata_code AND
Flight_CC.route_id=Route.route_id AND
Flight_CC.flight_id = Books.flight_id AND
Flight.flight_id = Books.flight_id AND
Passenger.user_id=Books.user_id AND
Passenger.first_name='Youjia' AND
Passenger.last_name='Zhang'
UNION
SELECT DISTINCT Passenger.first_name,Passenger.last_name,Flight.flight_no,Flight_share.dept_airport,Airport.city,Flight_share.arr_airport,A.city,Flight.flight_date,Flight.sched_dept_t
FROM Flight_share, Route, Passenger, Books, Flight, Airport,Airport A
WHERE Flight_share.dept_airport=Airport.iata_code AND
Flight_share.arr_airport=A.iata_code AND
Flight_share.flight_id = Books.flight_id AND
Flight.flight_id = Books.flight_id AND
Passenger.user_id=Books.user_id AND
Passenger.first_name='Youjia' AND
Passenger.last_name='Zhang';

-- Query for total attendence of all arrived flight with a given flight_no
-- Return the percentage of total attendence of all arrived flight with a given flight_no with class mixed
-- hold 2 digits after the decimal point

SELECT round((CAST (sum(sold_seats) AS numeric)/(sum(remain_seats)+sum(sold_seats))),2) AS AVERAGE_ON_BOARD_RATE
FROM Flight_ticket_manage_cc
WHERE flight_id IN (SELECT flight_id
FROM Flight
WHERE flight_no=3
AND flight_status = 'ARR');

-- Query for total delayed flight operated by our company (i.e., not included code-share flights)
-- Return total arrived flights, total delayed flights, on time rate (with 2 digits after the decimal point) and average delay minutes.

SELECT COUNT(arrived_flight) AS TOTAL_ARRIVED_FLIGHT,
       COUNT(delayed_flight) AS TOTAL_DELAYED_FLIGHT,
       ROUND((COUNT(arrived_flight)-COUNT(delayed_flight))::NUMERIC/COUNT(arrived_flight),2) AS ON_TIME_RATE,
       ROUND((SUM(EXTRACT(epoch from (delayed_flight.act_dept_t - delayed_flight.sched_dept_t)/60)) / COUNT(delayed_flight))::NUMERIC) AS AVG_DELAY_MIN
FROM
(SELECT Flight.* FROM Flight, Flight_CC WHERE Flight_CC.flight_id = Flight.flight_id AND flight_status = 'ARR') AS arrived_flight
LEFT OUTER JOIN
(SELECT * FROM FLIGHT WHERE Flight.sched_dept_t < Flight.act_dept_t) AS delayed_flight
ON arrived_flight.flight_id = delayed_flight.flight_id;

--Trigger for release ticket
CREATE FUNCTION delete_pend() RETURNS trigger AS
$$
BEGIN
	WITH needdelete(flight_id, class) AS 
	(SELECT flight_id, class
	FROM Books
	WHERE ticket_status='PEND'
	AND now()-interval '5 hours'-book_timestamp>interval '1 minutes')
	UPDATE Flight_ticket_manage_cc
	SET remain_seats=remain_seats+1, sold_seats=sold_seats-1
	FROM needdelete
	WHERE Flight_ticket_manage_cc.flight_id=needdelete.flight_id AND Flight_ticket_manage_cc.class=needdelete.class;
	WITH needdelete(flight_id, class) AS 
	(SELECT flight_id, class
	FROM Books
	WHERE ticket_status='PEND'
	AND now()-interval '5 hours'-book_timestamp>interval '1 minutes')
	UPDATE Flight_ticket_manage_share
	SET remain_seats=remain_seats+1
	FROM needdelete
	WHERE Flight_ticket_manage_share.flight_id=needdelete.flight_id AND Flight_ticket_manage_share.class=needdelete.class;
	DELETE FROM Books
	WHERE ticket_status='PEND'
	AND now()-interval '5 hours'-book_timestamp>interval '1 minutes';
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER releaseticket
BEFORE INSERT OR UPDATE ON Books
EXECUTE PROCEDURE delete_pend();

