#!/usr/bin/env python2.7

"""
Columbia W4111 Intro to databases
Example webserver

To run locally

    python server.py

Go to http://localhost:8111 in your browser


A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""

import os
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from sqlalchemy.exc import DBAPIError
from flask import Flask, render_template, request, url_for, jsonify, Response, redirect, flash, g
import datetime
import json
import random
import string
from json import dumps

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)


#
# The following uses the postgresql test.db -- you can use this for debugging purposes
# However for the project you will need to connect to your Part 2 database in order to use the
# data
#
# XXX: The URI should be in the format of: 
#
#     postgresql://USER:PASSWORD@<IP_OF_POSTGRE_SQL_SERVER>/postgres
#
# For example, if you had username ewu2493, password foobar, then the following line would be:
#
#     DATABASEURI = "postgresql://ewu2493:foobar@<IP_OF_POSTGRE_SQL_SERVER>/postgres"
#
# Swap out the URI below with the URI for the database created in part 2
DATABASEURI = "postgresql://zl2471:d462v@104.196.175.120/postgres"


#
# This line creates a database engine that knows how to connect to the URI above
#
engine = create_engine(DATABASEURI)

@app.before_request
def before_request():
  	"""
  	This function is run at the beginning of every web request 
  	(every time you enter an address in the web browser).
  	We use it to setup a database connection that can be used throughout the request

  	The variable g is globally accessible
  	"""
	try:
		g.conn = engine.connect()
	except:
		print "uh oh, problem connecting to database"
		import traceback; traceback.print_exc()
		g.conn = None

@app.after_request
def add_header(response):
	#set the max_age of webpage to 0 to force browser check page to server
	response.cache_control.max_age=0
	return response

@app.teardown_request
def teardown_request(exception):
	"""
	At the end of the web request, this makes sure to close the database connection.
	If you don't the database could run out of memory!
	"""
	try:
		g.conn.close()
	except Exception as e:
		pass

@app.route('/',methods=["GET","POST"])
def index():
  # DEBUG: this is debugging code to see what request looks like
	return render_template("index.html")

@app.route("/bookticket.html")
def bookticket():
	return render_template("bookticket.html")

@app.route("/usersearch.html")
def usersearch():
	return render_template("usersearch.html")

@app.route("/userinfo.html")
def userinfo():
	return render_template("userinfo.html")

@app.route("/payment.html")
def payment():
	return render_template("payment.html")

@app.route("/check.html")
def ticketcheck():
	return render_template("check.html")


#load airport and release expired 'PEND' ticket function
#Input: none
#Output: iata_code, city from 'Airport' table
@app.route("/load_airport")
def loadairport():
	cmd = "WITH needdelete(flight_id, class) AS\
			(SELECT flight_id, class\
			FROM Books WHERE ticket_status='PEND'\
			AND now()-interval '5 hours'-book_timestamp>interval '1 minutes')\
			UPDATE Flight_ticket_manage_cc\
			SET remain_seats=remain_seats+1, sold_seats=sold_seats-1\
			FROM needdelete\
			WHERE Flight_ticket_manage_cc.flight_id=needdelete.flight_id AND Flight_ticket_manage_cc.class=needdelete.class;"
	try:
		cur = g.conn.execute(text(cmd))
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd))
	cmd = "WITH needdelete(flight_id, class) AS\
			(SELECT flight_id, class\
			FROM Books\
			WHERE ticket_status='PEND'\
			AND now()-interval '5 hours'-book_timestamp>interval '1 minutes')\
			UPDATE Flight_ticket_manage_share\
			SET remain_seats=remain_seats+1\
			FROM needdelete\
			WHERE Flight_ticket_manage_share.flight_id=needdelete.flight_id AND Flight_ticket_manage_share.class=needdelete.class;"
	try:
		cur = g.conn.execute(text(cmd))
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd))
	cmd = "DELETE FROM Books\
			WHERE ticket_status='PEND'\
			AND now()-interval '5 hours'-book_timestamp>interval '1 minutes';"
	try:
		cur = g.conn.execute(text(cmd))
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd))
	cmd = "SELECT iata_code, city FROM Airport ORDER BY iata_code;"
	try:
		cur = g.conn.execute(text(cmd))
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd))
	data = cur.fetchall()
	for i in range(0,len(data)):
		data[i]=list(data[i])
		for j in range(0,len(data[i])):
			data[i][j]=str(data[i][j])
			data[i][j]=str(data[i][j])
		data[i]=tuple(data[i])
	return jsonify(coords = data)

#search own ticket based on airport and date
#Input: source_airport, destination_airport, departure date
#Output: available ticket
@app.route("/search_ticket_own")
def searchTicketOwn():
	#extract source_airport, destination_airport, departure date from HTTP request
	args = request.args
	source_airport = args.get('source_air')
	dest_airport = args.get('dest_air')
	dept_date = args.get('dept_date')
	cmd = "SELECT Flight.flight_no, Flight.sched_dept_t, Flight.sched_arr_t, Aircraft.type, Flight_ticket_manage_cc.class, Flight_ticket_manage_cc.full_price*Flight_ticket_manage_cc.price_rate, Flight_ticket_manage_cc.remain_seats, Flight.flight_id\
		FROM Flight, Flight_CC, Flight_ticket_manage_cc, Aircraft\
		WHERE Flight_CC.route_id IN (SELECT Route.route_id\
									FROM Route\
									WHERE Route.dept_airport= :s_air AND Route.arr_airport= :d_air)\
				AND Flight.flight_id=Flight_CC.flight_id\
				AND Flight.flight_date= :d_date\
				AND Flight_ticket_manage_cc.flight_id=Flight_CC.flight_id\
				AND Flight.flight_status='SCH'\
				AND Flight_ticket_manage_cc.remain_seats>0\
				AND Aircraft.aircraft_id=Flight_CC.aircraft_id\
		ORDER BY Flight.flight_no, Flight_ticket_manage_cc.class;"
	try:
	#get id, description, camera id, time period, tag
		cur = g.conn.execute(text(cmd), s_air=source_airport, d_air=dest_airport, d_date=dept_date)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), s_air=source_airport, d_air=dest_airport, d_date=dept_date)
	data = cur.fetchall()
	#print data
	for i in range(0,len(data)):
		data[i]=list(data[i])
		data[i][1]=data[i][1].isoformat(' ')
		data[i][2]=data[i][2].isoformat(' ')
		data[i]=tuple(data[i])
	#print data
	return jsonify(coords = data)

#search shared ticket based on airport and date
#Input: source_airport, destination_airport, departure date
#Output: available ticket
@app.route("/search_ticket_share")
def searchTicketShare():
	#extract source_airport, destination_airport, departure date from HTTP request
	args = request.args
	source_airport = args.get('source_air')
	dest_airport = args.get('dest_air')
	dept_date = args.get('dept_date')
	cmd = "WITH Flight_info(id, host_air, host_no, aircraft)\
			AS (SELECT Flight_share.flight_id, Flight_share.host_airline, Flight_share.host_flight_no, Flight_share.aircraft_type\
				FROM Flight_share\
				WHERE Flight_share.dept_airport= :s_air\
					AND Flight_share.arr_airport= :d_air)\
			SELECT Flight.flight_no, Flight.sched_dept_t, Flight.sched_arr_t, Flight_info.host_air, Flight_info.host_no, Flight_info.aircraft, Flight_ticket_manage_share.class, Flight_ticket_manage_share.price, Flight_ticket_manage_share.remain_seats, Flight.flight_id\
			FROM Flight, Flight_info, Flight_ticket_manage_share\
			WHERE Flight.flight_id=Flight_info.id\
					AND Flight_ticket_manage_share.flight_id=Flight_info.id\
					AND Flight.flight_date= :d_date\
					AND Flight.flight_status='SCH'\
					AND Flight_ticket_manage_share.remain_seats>0\
			ORDER BY Flight.flight_no, Flight_ticket_manage_share.class;"
	try:
		cur = g.conn.execute(text(cmd), s_air=source_airport, d_air=dest_airport, d_date=dept_date)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd), s_air=source_airport, d_air=dest_airport, d_date=dept_date)
	data = cur.fetchall()
	#print data
	for i in range(0,len(data)):
		data[i]=list(data[i])
		data[i][1]=data[i][1].isoformat(' ')
		data[i][2]=data[i][2].isoformat(' ')
		data[i]=tuple(data[i])
	#print data
	return jsonify(coords = data)

#calculate check sum
#Input: user id, flight id
#Output: user id
def calChecksum(user_id, flight_id_select):
	chars=string.ascii_uppercase+string.digits
	size=6
	return ''.join(random.choice(chars) for _ in range(size))

#search user id and pending book
#Input: name, email, phone, flight id, class, price
#Output: user id
@app.route("/search_user")
def searchUser():
	args = request.args
	firstname = args.get('firstname')
	middlename = args.get('middlename')
	lastname = args.get('lastname')
	email = args.get('email')
	phone = args.get('phone')
	flight_id_select = args.get('flight_id_select')
	class_select = args.get('class_select')
	price_select = args.get('price_select')
	flight_flag = args.get('flight_flag')
	if len(middlename)==0:
		middlename=None

	#find userid
	if middlename is None:
		cmd = "SELECT user_id FROM Passenger WHERE first_name= :fname AND middle_name IS NULL AND last_name= :lname AND email_addr= :email AND cell_no= :phone;"
		try:
			cur = g.conn.execute(text(cmd), fname=firstname, lname=lastname, email=email, phone=phone)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), fname=firstname, lname=lastname, email=email, phone=phone)
		data = cur.fetchall()
	else:
		cmd = "SELECT user_id FROM Passenger WHERE first_name= :fname AND middle_name= :mname AND last_name= :lname AND email_addr= :email AND cell_no= :phone;"
		try:
			cur = g.conn.execute(text(cmd), fname=firstname, mname=middlename, lname=lastname, email=email, phone=phone)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), fname=firstname, mname=middlename, lname=lastname, email=email, phone=phone)
		data = cur.fetchall()

	#insert new user
	if len(data)==0:
		cmd = "SELECT max(user_id)+1 FROM Passenger;"
		try:
			cur = g.conn.execute(text(cmd))
		except DBAPIError,e:
			print e
		data = cur.fetchall()
		#print data[0][0]
		cmd = "INSERT INTO Passenger (user_id, first_name, middle_name, last_name, email_addr, cell_no) VALUES (:userid,:fname,:mname,:lname,:email,:phone);"
		try:
			cur = g.conn.execute(text(cmd),userid=str(data[0][0]),fname=firstname, mname=middlename, lname=lastname, email=email, phone=phone)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd),userid=str(data[0][0]),fname=firstname, lname=lastname, email=email, phone=phone)
	user_id=data[0][0]

	#check if this user has booked this flight
	cmd = "SELECT * FROM Books WHERE user_id= :userid AND flight_id= :flightid AND class= :c;"
	try:
		cur = g.conn.execute(text(cmd),userid=user_id, flightid=flight_id_select, c=class_select)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd),userid=user_id, flightid=flight_id_select, c=class_select)
	bookeddata = cur.fetchall()
	if (len(bookeddata)!=0):
		data=[('Existed',)]
		return jsonify(coords = data)

	#insert new book information
	current=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	checksum=calChecksum(user_id, flight_id_select)
	unique_flag=False
	#generate unique check number
	while (not unique_flag):
		cmd = "SELECT * FROM Books WHERE confirm_no= :confirmno;"
		try:
			cur = g.conn.execute(text(cmd),confirmno=checksum)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd),confirmno=checksum)
		checkdata = cur.fetchall()
		if (len(checkdata)==0):
			unique_flag=True
		else:
			checksum=calChecksum(user_id, flight_id_select)
	#print user_id
	#print current

	#insert book information
	cmd = "INSERT INTO Books (user_id, flight_id, confirm_no, class, cur_price, book_timestamp, ticket_status) VALUES (:userid,:flightid,:confirmno,:c,:price,:time,'PEND');"
	try:
		cur = g.conn.execute(text(cmd),userid=user_id, flightid=flight_id_select, c=class_select, confirmno=checksum, price=price_select, time=current)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd),userid=user_id, flightid=flight_id_select, c=class_select, confirmno=checksum, price=price_select, time=current)

	#change remian ticket
	if (flight_flag=="true"):
		cmd = "UPDATE Flight_ticket_manage_cc SET remain_seats=remain_seats-1, sold_seats=sold_seats+1 WHERE flight_id= :flightid AND class= :c;"
		try:
			cur = g.conn.execute(text(cmd), flightid=flight_id_select, c=class_select)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flightid=flight_id_select, c=class_select)
	elif (flight_flag=="false"):
		cmd = "UPDATE Flight_ticket_manage_share SET remain_seats=remain_seats-1 WHERE flight_id= :flightid AND class= :c;"
		try:
			cur = g.conn.execute(text(cmd), flightid=flight_id_select, c=class_select)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flightid=flight_id_select, c=class_select)
	for i in range(0,len(data)):
		data[i]=list(data[i])
		for j in range(0,len(data[i])):
			data[i][j]=str(data[i][j])
			data[i][j]=str(data[i][j])
		data[i]=tuple(data[i])
	#print data
	return jsonify(coords = data)

#pay ticket
#Input: user id, flight id, class
#Output: success
@app.route("/pay")
def pay():
	#extract source_airport, destination_airport, departure date from HTTP request
	args = request.args
	userid = args.get('user_id')
	flight_id_select = args.get('flight_id_select')
	class_select = args.get('class_select')
	cmd = "UPDATE Books SET ticket_status='PAID' WHERE user_id= :userid AND flight_id= :flightid AND class= :c;"
	try:
		cur = g.conn.execute(text(cmd),userid=userid, flightid=flight_id_select, c=class_select)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd),userid=userid, flightid=flight_id_select, c=class_select)
	data=[('Success',)]
	return jsonify(coords = data)

#release ticket
#Input: user id, flight id, class
#Output: success
@app.route("/release_ticket")
def releaseTicket():
	#extract from HTTP request
	args = request.args
	userid = args.get('user_id')
	flight_id_select = args.get('flight_id_select')
	class_select = args.get('class_select')
	flight_flag = args.get('flight_flag')
	cmd = "DELETE FROM Books WHERE user_id= :userid AND flight_id= :flightid AND class= :c;"
	try:
		cur = g.conn.execute(text(cmd),userid=userid, flightid=flight_id_select, c=class_select)
	except DBAPIError,e:
		print e
		if e.connection_invalidated:
			g.conn = engine.connect()
			cur = g.conn.execute(text(cmd),userid=userid, flightid=flight_id_select, c=class_select)
	if (flight_flag=="true"):
		cmd = "UPDATE Flight_ticket_manage_cc SET remain_seats=remain_seats+1, sold_seats=sold_seats-1 WHERE flight_id= :flightid AND class= :c;"
		try:
			cur = g.conn.execute(text(cmd), flightid=flight_id_select, c=class_select)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flightid=flight_id_select, c=class_select)
	elif (flight_flag=="false"):
		cmd = "UPDATE Flight_ticket_manage_share SET remain_seats=remain_seats+1 WHERE flight_id= :flightid AND class= :c;"
		try:
			cur = g.conn.execute(text(cmd), flightid=flight_id_select, c=class_select)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), flightid=flight_id_select, c=class_select)
	data=[('Success',)]
	return jsonify(coords = data)

#search user id and pending book
#Input: user id flight id, class
#Output: user id
@app.route("/load_ticket")
def loadTicket():
	args = request.args
	userid = args.get('user_id')
	flight_id_select = args.get('flight_id_select')
	class_select = args.get('class_select')
	flight_flag = args.get('flight_flag')
	if (flight_flag=="true"):
		cmd = "WITH Flight_info(no, dept_t, arr_t) AS\
				(SELECT Flight.flight_no, Flight.sched_dept_t, Flight.sched_arr_t\
					FROM Flight\
					WHERE Flight.flight_id= :flightid)\
			SELECT Passenger.first_name, Passenger.middle_name, Passenger.last_name, Passenger.email_addr, Passenger.cell_no, Flight_info.no, Flight_info.dept_t, Route.dept_airport, Flight_info.arr_t, Route.arr_airport, Books.class, Books.confirm_no, Aircraft.type\
			FROM Passenger, Books, Flight_info, Flight_CC, Aircraft, Route\
			WHERE Passenger.user_id= :userid\
				AND Passenger.user_id=Books.user_id\
				AND Books.flight_id= :flightid\
				AND Flight_CC.flight_id= :flightid\
				AND Route.route_id=Flight_CC.route_id\
				AND Flight_CC.aircraft_id=Aircraft.aircraft_id\
				AND Books.class= :c;"
		try:
			cur = g.conn.execute(text(cmd),userid=userid, flightid=flight_id_select, c=class_select)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd),userid=userid, flightid=flight_id_select, c=class_select)
		data = cur.fetchall()
	elif (flight_flag=="false"):
		cmd = "SELECT Passenger.first_name, Passenger.middle_name, Passenger.last_name, Passenger.email_addr, Passenger.cell_no, Flight.flight_no,  Flight.sched_dept_t, Flight_share.dept_airport, Flight.sched_arr_t, Flight_share.arr_airport, Books.class, Books.confirm_no, Flight_share.aircraft_type, Flight_share.host_airline, Flight_share.host_flight_no\
				FROM Passenger, Books, Flight, Flight_share\
				WHERE Passenger.user_id= :userid\
					AND Passenger.user_id=Books.user_id\
					AND Flight.flight_id= :flightid\
					AND Flight.flight_id=Books.flight_id\
					AND Flight.flight_id=Flight_share.flight_id\
					AND Books.class= :c;"
		try:
			cur = g.conn.execute(text(cmd),userid=userid, flightid=flight_id_select, c=class_select)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd),userid=userid, flightid=flight_id_select, c=class_select)
		data = cur.fetchall()
	for i in range(0,len(data)):
		data[i]=list(data[i])
		data[i][6]=data[i][6].isoformat(' ')
		data[i][8]=data[i][8].isoformat(' ')
		data[i]=tuple(data[i])
	#print data
	return jsonify(coords = data)

#search user id and pending book
#Input: name, email, phone, flight id, class, price
#Output: user id
@app.route("/query_user")
def queryUser():
	args = request.args
	firstname = args.get('firstname')
	middlename = args.get('middlename')
	lastname = args.get('lastname')
	email = args.get('email')
	phone = args.get('phone')
	if len(middlename)==0:
		middlename=None

	#find userid
	if middlename is None:
		cmd = "SELECT user_id FROM Passenger WHERE first_name= :fname AND middle_name IS NULL AND last_name= :lname AND email_addr= :email AND cell_no= :phone;"
		try:
			cur = g.conn.execute(text(cmd), fname=firstname, lname=lastname, email=email, phone=phone)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), fname=firstname, lname=lastname, email=email, phone=phone)
		data = cur.fetchall()
	else:
		cmd = "SELECT user_id FROM Passenger WHERE first_name= :fname AND middle_name= :mname AND last_name= :lname AND email_addr= :email AND cell_no= :phone;"
		try:
			cur = g.conn.execute(text(cmd), fname=firstname, mname=middlename, lname=lastname, email=email, phone=phone)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), fname=firstname, mname=middlename, lname=lastname, email=email, phone=phone)
		data = cur.fetchall()

	#insert new user
	if len(data)==0:
		result=[('None',)]
		return jsonify(coords = result)
	else:
		user_id=data[0][0]
		cmd= "WITH book_his(flight_no,dept_airport,dept_city,arr_airport,arr_city,date,class,flight_id) AS \
				(SELECT Flight.flight_no,Route.dept_airport,Airport.city,Route.arr_airport,A.city,Flight.flight_date,Books.class,Flight.flight_id\
				FROM Flight_CC, Route, Books, Flight, Airport, Airport A\
				WHERE Route.dept_airport=Airport.iata_code AND\
				Route.arr_airport=A.iata_code AND\
				Flight_CC.route_id=Route.route_id AND\
				Flight_CC.flight_id = Books.flight_id AND\
				Flight.flight_id = Books.flight_id AND\
				Books.user_id= :user_id \
				UNION\
				SELECT DISTINCT Flight.flight_no,Flight_share.dept_airport,Airport.city,Flight_share.arr_airport,A.city,Flight.flight_date,Books.class,Flight.flight_id\
				FROM Flight_share, Route, Passenger, Books, Flight, Airport, Airport A\
				WHERE Flight_share.dept_airport=Airport.iata_code AND\
				Flight_share.arr_airport=A.iata_code AND\
				Flight_share.flight_id = Books.flight_id AND\
				Flight.flight_id = Books.flight_id AND\
				Books.user_id = :user_id)\
				SELECT *\
				FROM book_his\
				ORDER BY flight_no, date DESC;"
		try:
			cur = g.conn.execute(text(cmd), user_id=user_id)
		except DBAPIError,e:
			print e
			if e.connection_invalidated:
				g.conn = engine.connect()
				cur = g.conn.execute(text(cmd), user_id=user_id)
		result = cur.fetchall()
		#print result
		for i in range(0,len(result)):
			result[i]=list(result[i])
			result[i][5]=result[i][5].isoformat()
			result[i]=tuple(result[i])
		return jsonify(coords = result, users=user_id)

if __name__ == "__main__":
	import click

	@click.command()
	@click.option('--debug', is_flag=True)
	@click.option('--threaded', is_flag=True)
	@click.argument('HOST', default='0.0.0.0')
	@click.argument('PORT', default=8000, type=int)
	def run(debug, threaded, host, port):
		HOST, PORT = host, port
		print "running on %s:%d" % (HOST, PORT)
		app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)

	run()
