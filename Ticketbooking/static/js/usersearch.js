//main function

//airport set
var ticket_set=[];

//clear sessionStorage
sessionStorage.clear();

emailflag=false;
phoneflag=false;

user_id=null;

function validateEmail(){	
	emailField=document.getElementById("email").value;
	//console.log(emailField);
	var atpos=emailField.indexOf("@");
	var atnum=emailField.split("@").length-1;
	var dotpos=emailField.lastIndexOf(".");
	if (atnum!=1 || atpos<1 || dotpos<atpos+2 || dotpos+2>=emailField.length) {
		document.getElementById("emailcheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("emailbox").className="form-group has-feedback has-error";
		emailflag=false;
	}
	else {
		document.getElementById("emailcheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("emailbox").className="form-group has-feedback has-success";
		emailflag=true;
	}
}

function validatePhone(){
	PhoneField=document.getElementById("phone").value;
	var isnum = /^\d+$/.test(PhoneField);
	var phonelen=PhoneField.length;
	//console.log(isnum);
	if ((phonelen<5 || phonelen>13) || !isnum) {
		document.getElementById("phonecheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("phonebox").className="form-group has-feedback has-error";
		phoneflag=false;
	}
	else {
		document.getElementById("phonecheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("phonebox").className="form-group has-feedback has-success";
		phoneflag=true;
	}
}

function search_user() {
	firstname = document.getElementById("firstname").value;
	middlename = document.getElementById("middlename").value;
	lastname = document.getElementById("lastname").value;
	email = document.getElementById("email").value;
	phone = document.getElementById("phone").value;
	//console.log(firstname);
	//console.log(middlename);
	//console.log(lastname);
	//console.log(email);
	//console.log(phone);
	if (firstname.length!=0 && lastname.length!=0 && email.length!=0 && phone.length!=0 && emailflag && phoneflag) {
		phone = "+"+phone;
		$.get( "/query_user",{firstname:firstname, middlename:middlename, lastname:lastname, email:email, phone:phone}, function( data, status ) {
			//console.log(data);
			if (data.coords[0][0]=="None") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> User not found.</span>");
				document.getElementById("emailcheck").className="";	
				document.getElementById("emailbox").className="form-group has-feedback";
				document.getElementById("phonecheck").className="";
				document.getElementById("phonebox").className="form-group has-feedback";		
				$("#error").show();
				document.getElementById("emailcheck").className="";
				document.getElementById("firstname").value="";
				document.getElementById("middlename").value="";
				document.getElementById("lastname").value="";
				document.getElementById("email").value="";
				document.getElementById("phone").value="";
			}
			else {
				//console.log(user_id);
				$("#ticket_list tr").remove();
				ticket_set=[];
				//console.log(data);
				user_id=data.users;
				for (i=0; i<data.coords.length; i++){
    				ticket_set.push ({flgiht_no: data.coords[i][0], dept_city: data.coords[i][1], dept_airport: data.coords[i][2], arri_city: data.coords[i][3],  arri_airport: data.coords[i][4], date: data.coords[i][5], trav_class: data.coords[i][6], flight_id: data.coords[i][7]});
  			}
			//add flight to a table
  			for (var i = 0; i<ticket_set.length; ++i){
    			if (ticket_set[i].trav_class==="E") {
					newline="Economy"
				}
    			if (ticket_set[i].trav_class==="B") {
					newline="Business"
				}
    			if (ticket_set[i].trav_class==="F") {
					newline="First"
				}
    			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='getTicket("+i.toString()+", false)'><td>CC"+ticket_set[i].flgiht_no.toString()+"</td><td>"+ticket_set[i].dept_city+"("+ticket_set[i].dept_airport+")"+"</td><td>"+ticket_set[i].arri_city+"("+ticket_set[i].arri_airport+")"+"</td><td>"+ticket_set[i].date+"</td><td>"+newline+"</td>"+"</tr>"
    			$("#ticket_list").append(newline);
  			}
			}
		});
	}
	else if ((!emailflag && email.length!=0) && (!phoneflag && phone.length!=0)) {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> You have wrong information.</span>");
		$("#error").show();
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> You have missed some information.</span>");
		$("#error").show();
	}
}

function getTicket(index) {
	flight_id_select=ticket_set[index].flight_id;
	class_select=ticket_set[index].trav_class;
	//console.log(user_id);
	if (flight_id_select[0]==='R') {
		flight_flag=true;
	}
	if (flight_id_select[0]==='S') {
		flight_flag=false;
	}
	sessionStorage.setItem("user_id",user_id);
	sessionStorage.setItem("flight_id_select",flight_id_select);
	sessionStorage.setItem("class_select",class_select);
	sessionStorage.setItem("flight_flag",flight_flag);
	location.href="check.html";
}

