//main function

//airport set
var airport_set=[];

//source airport and destination airport
var source_airport=null;
var dest_airport=null;

//departure date in string 
var dept_date=null;

//share flight set
var flight_share=[];
//own flight set
var flight_own=[];

//ticket selected
var flight_id_select;
var class_select;
var price_select;

//clear sessionStorage
sessionStorage.clear();

//load airport
$.get("/load_airport",{}, function( data, status ){
  //console.log(data.coords[0]);
  //store airport
  for (i=0; i<data.coords.length; i++){
    airport_set.push ({iata_code: data.coords[i][0], city: data.coords[i][1]});
  }
  //console.log(airport_set);
  //add every airport to a dropdown
  for (var i = 0; i<airport_set.length; ++i){   
	newline = "<li><a tabindex=\"-1\" onclick=\"set_iata("+i.toString()+", true)\" href=\"#\"> "+airport_set[i].city+" ("+airport_set[i].iata_code+")</a></li>"
	//console.log(newline);
    $("#source_air").append(newline);
	newline = "<li><a tabindex=\"-1\" onclick=\"set_iata("+i.toString()+", false)\" href=\"#\"> "+airport_set[i].city+" ("+airport_set[i].iata_code+")</a></li>"
    //console.log(newline);
    $("#dest_air").append(newline);
  }
});

function set_iata(index, flag) {
	if (flag) {
		source_airport=airport_set[index].iata_code;
		line=airport_set[index].city+" ("+airport_set[index].iata_code+")";
		$('#dropdown_title_source').html(line+"<span class='caret'></span>");
	}
	else {
		dest_airport=airport_set[index].iata_code;
		line=airport_set[index].city+" ("+airport_set[index].iata_code+")";
		$('#dropdown_title_dest').html(line+"<span class='caret'></span>");
	}
	//console.log(source_airport);
	//console.log(dest_airport);
}

//the datetime picker
$(function () {
    $('#datetimepicker1').datetimepicker({
        useCurrent: false, //Important! See issue #1075
        //defaultDate: mainStartDT,
        format: "MM/DD/YYYY",
		minDate: moment()
    });
});

//set departure date
$('#datetimepicker1').on("dp.change",function(e){
	//console.log("change in 1");
	desire = e.date._d;
	dept_date=desire.getFullYear().toString()+"-"+(desire.getMonth()+1).toString()+"-"+desire.getDate().toString();
	//console.log(dept_date);
});

function searchTicket() {
	if (source_airport!=null && dest_airport!=null && dept_date!=null) {
		$.get( "/search_ticket_own",{source_air:source_airport, dest_air:dest_airport, dept_date:dept_date}, function( data, status ) {
			$("#own_flight_list tr").remove();
			flight_own=[];
			//$("#share_flight_list").remove();
			//console.log(data);
			for (i=0; i<data.coords.length; i++){
    			flight_own.push ({flgiht_no: data.coords[i][0], dept_time: data.coords[i][1], arri_time: data.coords[i][2], aircraft: data.coords[i][3], trav_class: data.coords[i][4], price: data.coords[i][5], remain_seat: data.coords[i][6], flight_id: data.coords[i][7]});
  			}
			//add flight to a table
  			for (var i = 0; i<flight_own.length; ++i){
    			if (flight_own[i].trav_class==="E") {
					newline="Economy"
				}
    			if (flight_own[i].trav_class==="B") {
					newline="Business"
				}
    			if (flight_own[i].trav_class==="F") {
					newline="First"
				}
    			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='bookTicket("+i.toString()+", true)'><td>CC"+flight_own[i].flgiht_no.toString()+"</td><td>"+flight_own[i].dept_time+"</td><td>"+source_airport+"</td><td>"+flight_own[i].arri_time+"</td><td>"+dest_airport+"</td><td>"+flight_own[i].aircraft+"</td><td>"+newline+"</td><td>"+flight_own[i].price+"</td><td>"+flight_own[i].remain_seat.toString()+"</tr>"
    			$("#own_flight_list").append(newline);
  			}
		});
		$.get( "/search_ticket_share",{source_air:source_airport, dest_air:dest_airport, dept_date:dept_date}, function( data, status ) {
			//$("#own_flight_list").remove();
			$("#share_flight_list tr").remove();
			flight_share=[];
			//console.log(data);
			for (i=0; i<data.coords.length; i++){
    			flight_share.push ({flgiht_no: data.coords[i][0], dept_time: data.coords[i][1], arri_time: data.coords[i][2], host_airline: data.coords[i][3], host_flightno: data.coords[i][4], aircraft: data.coords[i][5], trav_class: data.coords[i][6], price: data.coords[i][7], remain_seat: data.coords[i][8], flight_id: data.coords[i][9]});
  			}
			//add flight to a table
  			for (var i = 0; i<flight_share.length; ++i){
    			if (flight_share[i].trav_class==="E") {
					newline="Economy"
				}
    			if (flight_share[i].trav_class==="B") {
					newline="Business"
				}
    			if (flight_share[i].trav_class==="F") {
					newline="First"
				}
    			newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='bookTicket("+i.toString()+", false)'><td>CC"+flight_share[i].flgiht_no.toString()+"("+flight_share[i].host_airline+flight_share[i].host_flightno.toString()+")"+"</td><td>"+flight_share[i].dept_time+"</td><td>"+source_airport+"</td><td>"+flight_share[i].arri_time+"</td><td>"+dest_airport+"</td><td>"+flight_share[i].aircraft+"</td><td>"+newline+"</td><td>"+flight_share[i].price+"</td><td>"+flight_share[i].remain_seat.toString()+"</tr>"
    			$("#share_flight_list").append(newline);
  			}
		});
	}
}

function bookTicket(index,flag) {	
	flight_flag=flag;
	if (flag) {
		flight_id_select=flight_own[index].flight_id;
		class_select=flight_own[index].trav_class;
		price_select=flight_own[index].price;
	}
	else {
		flight_id_select=flight_share[index].flight_id;
		class_select=flight_share[index].trav_class;
		price_select=flight_share[index].price;
	}
	sessionStorage.setItem("flight_flag",flight_flag);
	sessionStorage.setItem("flight_id_select",flight_id_select);
	sessionStorage.setItem("class_select",class_select);
	sessionStorage.setItem("price_select",price_select);
	location.href="userinfo.html";
}
