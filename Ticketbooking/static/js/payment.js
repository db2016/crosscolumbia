flight_flag=sessionStorage.getItem("flight_flag");
flight_id_select=sessionStorage.getItem("flight_id_select");
class_select=sessionStorage.getItem("class_select");
price_select=sessionStorage.getItem("price_select");
user_id=sessionStorage.getItem("user_id");

cardtypeflag=false;
cardnumflag=false;
monthflag=false;
yearflag=false;
secflag=false;

document.getElementById("pricing").innerHTML="You need to pay: "+price_select.toString();

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    id = setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            clearInterval(id);
			$.get( "/release_ticket",{user_id:user_id, flight_id_select:flight_id_select, class_select:class_select, flight_flag:flight_flag}, function( data, status ) {
				console.log(data);
				if (data.coords[0][0]=='Success'){
					location.href="/";
				}
			});			
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = 60,
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
};

function set_card(type){
	cardtypeflag=true;
	$('#dropdown_title_card').html(type+"<span class='caret'></span>");
}

function validateCardnum() {
	cardnumfield=document.getElementById("cardnum").value;
	var isnum = /^\d+$/.test(cardnumfield);
	var cnlen=cardnumfield.length;
	//console.log(isnum);
	if (cnlen!=16 || !isnum) {
		document.getElementById("cardnumcheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("cardnumbox").className="form-group has-feedback has-error";
		cardnumflag=false;
	}
	else {
		document.getElementById("cardnumcheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("cardnumbox").className="form-group has-feedback has-success";
		cardnumflag=true;
	}
}

function validateMonth() {
	monthfield=document.getElementById("month").value;
	var isnum = /^\d+$/.test(monthfield);
	var monthlen=monthfield.length;
	//console.log(isnum);
	if (monthlen!=2 || !isnum) {
		document.getElementById("monthcheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("datebox").className="form-group has-feedback has-error";
		monthflag=false;
	}
	else if (yearflag) {
		document.getElementById("monthcheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("datebox").className="form-group has-feedback has-success";
		monthflag=true;
	}
	else {
		document.getElementById("monthcheck").className="glyphicon glyphicon-ok form-control-feedback";
		monthflag=true;
	}
}

function validateYear() {
	yearfield=document.getElementById("year").value;
	var isnum = /^\d+$/.test(yearfield);
	var yearlen=yearfield.length;
	//console.log(isnum);
	if (yearlen!=4 || !isnum) {
		document.getElementById("yearcheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("datebox").className="form-group has-feedback has-error";
		yearflag=false;
	}
	else if (monthflag) {
		document.getElementById("yearcheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("datebox").className="form-group has-feedback has-success";
		yearflag=true;
	}
	else {
		document.getElementById("yearcheck").className="glyphicon glyphicon-ok form-control-feedback";
		yearflag=true;
	}
}

function validateSCode() {
	scfield=document.getElementById("scode").value;
	var isnum = /^\d+$/.test(scfield);
	var sclen=scfield.length;
	//console.log(isnum);
	if (sclen!=3 || !isnum) {
		document.getElementById("scodecheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("scbox").className="form-group has-feedback has-error";
		secflag=false;
	}
	else {
		document.getElementById("scodecheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("scbox").className="form-group has-feedback has-success";
		secflag=true;
	}
}

function pay() {
	var today=new Date();
	var month=document.getElementById("month").value;
	var year=document.getElementById("year").value;
	var expireday=new Date(year,month,1,0,0,0,0); 
	if (cardtypeflag && cardnumflag && monthflag && yearflag && secflag && today<expireday) {
		$.get( "/pay",{user_id:user_id, flight_id_select:flight_id_select, class_select:class_select}, function( data, status ) {
			//console.log(data);
			if (data.coords[0][0]=='Success'){
				location.href="check.html";
			}
		});
	}
	else {
		$("#error").show();
	}
}
