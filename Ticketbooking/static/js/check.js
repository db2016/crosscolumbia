flight_flag=sessionStorage.getItem("flight_flag");
flight_id_select=sessionStorage.getItem("flight_id_select");
class_select=sessionStorage.getItem("class_select");
price_select=sessionStorage.getItem("price_select");
user_id=sessionStorage.getItem("user_id");

$.get("/load_ticket",{user_id:user_id, flight_id_select:flight_id_select, class_select:class_select, flight_flag:flight_flag}, function( data, status ){
	//console.log(data);
	firstname = data.coords[0][0];
	middlename = data.coords[0][1];
	if (middlename===null) {
		middlename=" ";
	}
	lastname = data.coords[0][2];
	email = data.coords[0][3];
	phone = data.coords[0][4];
	if (flight_flag=='true') {
		flightnum="CC"+data.coords[0][5];
	}
	else {
		flightnum="CC"+data.coords[0][5]+"("+data.coords[0][13]+data.coords[0][14]+")";
		//console.log(flightnum);
	}
	dept_time=data.coords[0][6];
	dept_airport=data.coords[0][7];
	arri_time=data.coords[0][8];
	arri_airport=data.coords[0][9];
	class_now=data.coords[0][10];
	aircraft=data.coords[0][12];
	checknum=data.coords[0][11];
	document.getElementById("checknum").innerHTML="<strong>Check Number: </strong>"+checknum;
	document.getElementById("firstname").innerHTML="<strong>First Name: </strong>"+firstname;
	document.getElementById("middlename").innerHTML="<strong>Middle Name: </strong>"+middlename;
	document.getElementById("lastname").innerHTML="<strong>Last Name: </strong>"+lastname;
	document.getElementById("email").innerHTML="<strong>Email: </strong>"+email;
	document.getElementById("phone").innerHTML="<strong>Phone: </strong>"+phone;
	document.getElementById("flightnum").innerHTML="<strong>Flight Number: </strong>"+flightnum;
	document.getElementById("depttime").innerHTML="<strong>Departure Time: </strong>"+dept_time;
	document.getElementById("deptairport").innerHTML="<strong>Departure Airport: </strong>"+dept_airport;
	document.getElementById("arritime").innerHTML="<strong>Arrival Time: </strong>"+arri_time;
	document.getElementById("arriairport").innerHTML="<strong>Arrival Airport: </strong>"+arri_airport;
	document.getElementById("class").innerHTML="<strong>Class: </strong>"+class_now;
	document.getElementById("aircraft").innerHTML="<strong>Aircraft Type: </strong>"+aircraft;
});

function returnIndex() {
	location.href="/";
}
