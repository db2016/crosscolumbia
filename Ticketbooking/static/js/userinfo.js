flight_flag=sessionStorage.getItem("flight_flag");
flight_id_select=sessionStorage.getItem("flight_id_select");
class_select=sessionStorage.getItem("class_select");
price_select=sessionStorage.getItem("price_select");
emailflag=false;
phoneflag=false;

function validateEmail(){	
	emailField=document.getElementById("email").value;
	//console.log(emailField);
	var atpos=emailField.indexOf("@");
	var atnum=emailField.split("@").length-1;
	var dotpos=emailField.lastIndexOf(".");
	if (atnum!=1 || atpos<1 || dotpos<atpos+2 || dotpos+2>=emailField.length) {
		document.getElementById("emailcheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("emailbox").className="form-group has-feedback has-error";
		emailflag=false;
	}
	else {
		document.getElementById("emailcheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("emailbox").className="form-group has-feedback has-success";
		emailflag=true;
	}
}

function validatePhone(){
	PhoneField=document.getElementById("phone").value;
	var isnum = /^\d+$/.test(PhoneField);
	var phonelen=PhoneField.length;
	//console.log(isnum);
	if ((phonelen<5 || phonelen>13) || !isnum) {
		document.getElementById("phonecheck").className="glyphicon glyphicon-remove form-control-feedback";
		document.getElementById("phonebox").className="form-group has-feedback has-error";
		phoneflag=false;
	}
	else {
		document.getElementById("phonecheck").className="glyphicon glyphicon-ok form-control-feedback";
		document.getElementById("phonebox").className="form-group has-feedback has-success";
		phoneflag=true;
	}
}

function set_userinfo() {
	firstname = document.getElementById("firstname").value;
	middlename = document.getElementById("middlename").value;
	lastname = document.getElementById("lastname").value;
	email = document.getElementById("email").value;
	phone = document.getElementById("phone").value;
	//console.log(firstname);
	//console.log(middlename);
	//console.log(lastname);
	//console.log(email);
	//console.log(phone);
	if (firstname.length!=0 && lastname.length!=0 && email.length!=0 && phone.length!=0 && emailflag && phoneflag) {
		phone = "+"+phone;
		$.get( "/search_user",{firstname:firstname, middlename:middlename, lastname:lastname, email:email, phone:phone, flight_id_select:flight_id_select, class_select:class_select, price_select:price_select, flight_flag:flight_flag}, function( data, status ) {
			//console.log(data);
			if (data.coords[0][0]=="Existed") {
				$("#errormessage").remove();
				$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> You have booked this flight.</span>");
				document.getElementById("emailcheck").className="";	
				document.getElementById("emailbox").className="form-group has-feedback";
				document.getElementById("phonecheck").className="";
				document.getElementById("phonebox").className="form-group has-feedback";		
				$("#error").show();
				document.getElementById("emailcheck").className="";
				document.getElementById("firstname").value="";
				document.getElementById("middlename").value="";
				document.getElementById("lastname").value="";
				document.getElementById("email").value="";
				document.getElementById("phone").value="";
			}
			else {	
				user_id=data.coords[0][0];
				//console.log(user_id);
				sessionStorage.setItem("user_id",user_id);
				location.href="payment.html";
			}
		});
	}
	else if ((!emailflag && email.length!=0) && (!phoneflag && phone.length!=0)) {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> You have wrong information.</span>");
		$("#error").show();
	}
	else {
		$("#errormessage").remove();
		$("#error").append("<span  id=\"errormessage\"> <strong>Warning!</strong> You have missed some information.</span>");
		$("#error").show();
	}
}
